# Yeet

**YEet Every Term!**

[Urban Dictionary](https://www.urbandictionary.com/define.php?term=Yeet) "to yeet"

- "To discard an item at a high velocity"
- "A word one may scream while propelling an object through the air at alarming speeds"

A project to reimplement and improve upon kimwitu++ using modern C++.

## Prerequisites

- C++20 compatible compiler
- `cmake`
- `gengetopt`
- `flex`
- `bison`

## Build from source

Simply invoking
```
$ pwd
~/yeet
$ cmake -Bbuild [-S.]
...
$ cmake --build build [--jobs=#]
...
$ cmake --install build [--prefix=/some/path]
```
should suffice to configure and build `yeet` in the folder `build` over `#` jobs in parallel and install it in `/some/path/bin` (and additional config files for cmake in `/some/path/lib/cmake/yeet`). Default value for `#` is 1 and for `/some/path` depending on your operating system.

## Usage

```
$ yeet -h
yeet 0.3.0
Usage: [OPTIONS] [FILES]

About yeet:

  -h, --help                Print help and exit
      --detailed-help       Print help, including all details and hidden
                              options, and exit
  -V, --version             Print version and exit

Configuration:
  -l, --loglevel=INT        Set the log level  (default=`3')
  -p, --parsetrace          Trace parsing  (default=off)
  -s, --scantrace           Trace scanning  (default=off)
  -f, --file-prefix=PREFIX  Set output file prefix  (default=`')
  -n, --namespace=NAME      Set namespace name for generated files
                              (default=`yeet')
      --yystype             Generate header with yystype definition for
                              bison/flex  (default=off)
      --c-headers           Generate c-style headers (.h) instead of c++-style
                              (.hh)  (default=off)
```

## Documentation

- For the code: see folder `doxygen`
  - to build, run `doxygen` in the `doxygen` folder
- For the user manual: see folder `docs`
- For the developer manual: see `docs`

## Developing

### Contributing to remote
- The `.gitignore` per default ignores any file in the root of the repository to allow arbitrary build folders easily. Exceptions are the folders `cmake`, `doc`, `gen`, `include` and `src`.
- Usage of `pre-commit` is highly recommended.

### IDE Configuration
- CMake's configuration always generates a `compile_commands.json`, which can be included in your IDE configuration
- `cmake/vscode.cmake` generates a configuration for VSCode's `c_cpp_properties.json` using the latest configuration run. This can be disabled through the cmake option `-DYEET_VSCODE_SUPPORT=OFF`. The configuration can be edited afterwards, only the following settings will be overridden
    - `compilerPath`
    - `cppStandard`
    - `cStandard`
    - `defines`
    - `compile_commands`

### Folder structure

```
yeet/
├─ cmake/               scripts for cmake
├─ doc/                 files for doxygen
├─ gen/                 files and definitions for pre-generated source files
├─ include/             external includes
├─ src/                 main sources
│  ├─ generator/        sources for file generation
│  ├─ tree/             sources for intermediate representation after parsing, before file generation
├─ CMakeLists.txt       top level CMakeLists
```
