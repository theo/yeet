# ########################################################################################
# Term processor yeet. CMake functions
#
# Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
# University of Rostock, lead by Karsten Wolf, as well as yeet contributors
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# -- NOTE: Third party dependency used by this software --
# This software depends on the fmt lib (MIT License),
# and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
#
# ########################################################################################

# Some functions useful in other scripts

# convert json array in variable input_list to cmake list and write to out_var
function(json_array_to_list input_list out_var)
    string(JSON len LENGTH ${${input_list}})

    if(len GREATER 0)
        math(EXPR max_elem ${len}-1)

        foreach(i RANGE ${max_elem})
            string(JSON elem GET ${${input_list}} ${i})
            list(APPEND tmp ${elem})
        endforeach()

        set(${out_var} ${tmp} PARENT_SCOPE)
    else()
        set(${out_var} "" PARENT_SCOPE)
    endif()
endfunction(json_array_to_list)

# convert list given by var input_list to json array and write to out_var
function(list_to_json_array input_list out_var)
    list(LENGTH ${input_list} len)

    if(len GREATER 0)
        list(JOIN ${input_list} "\", \"" tmp)
        string(PREPEND tmp "[ \"")
        string(APPEND tmp "\" ]")
        set(${out_var} ${tmp} PARENT_SCOPE)
    else()
        set(${out_var} "[]" PARENT_SCOPE)
    endif()
endfunction(list_to_json_array)

# merge json array given in arr with cmake list given in list by appending list to arr,
# removing duplicates and writing to out_var
function(merge_json_array_list arr list out_var)
    json_array_to_list(${arr} arrlist)
    list(APPEND arrlist ${${list}})
    list(REMOVE_DUPLICATES arrlist)
    list_to_json_array(arrlist result)
    set(${out_var} ${result} PARENT_SCOPE)
endfunction(merge_json_array_list)

# merge json arrays given in arr1 and arr2 by appending arr2 to arr1,
# removing duplicates and writing to out_var
function(merge_json_arrays arr1 arr2 out_var)
    json_array_to_list(${arr2} list2)
    merge_json_array_list(${arr1} list2 tmp)
    set(${out_var} ${tmp} PARENT_SCOPE)
endfunction(merge_json_arrays)
