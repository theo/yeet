/****************************************************************************************
  Term processor yeet. Lexic scanner definitions

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file scanner.ll
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Lexic scanner definitions
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

%{

#include <cerrno>
#include <climits>
#include <cstdlib>
#include <cstring> // strerror
#include <string>

#include "parser.hh"

#include <io.hh>
#include <runtime.hh>

#define YY_USER_ACTION loc->columns(yyleng);

using namespace parsing;

int cinit_paren_nesting = 0;
int cinit_array_nesting = 0;
parsing::location* loc;
void reset_nestcount();
int eat_comment(parsing::location& local);
void eat_line_comment(parsing::location& local);

%}

%option noyywrap nounput batch debug
%start NORMAL CEXPR CEXPRDQ CEXPRSQ C INCLSTART INCL
%x OPTION

special             [\_]
identifier          ([_a-zA-Z][_a-zA-Z0-9]*)
nonidentifier       [^a-zA-Z0-9\_]
int                 [0-9]+
exp                 [Ee][-+]?[0-9]+
float               [-+]?[0-9]+\.?[0-9]*
whitespace_nl       [\ \t\v\f\r]
whitespace          [\ \t\r]


%%
%{
    loc->step();
%}


<C,CEXPR>\/\*                                   {loc->step(); int lines=eat_comment(*loc); return Parser::make_CNEWLINES(lines, *loc);}
<NORMAL,INCLSTART>\/\*                          {loc->step(); eat_comment(*loc);}
<C,CEXPR>\/\/                                   {loc->step(); eat_line_comment(*loc); return Parser::make_CNEWLINES(1, *loc);}
<NORMAL,INCLSTART>\/\/                          {loc->step(); eat_line_comment(*loc);}

<NORMAL>{whitespace}+                           {loc->step();}
<NORMAL>{whitespace_nl}+                        {loc->step();}
<NORMAL,CEXPR>\n+                               {loc->lines(yyleng); loc->step();}

<NORMAL>^%uview                                 {return Parser::make_PERCENTUVIEW(*loc);}
<NORMAL>^%rview                                 {return Parser::make_PERCENTRVIEW(*loc);}

<NORMAL>^\%\{{whitespace}*		                {BEGIN INCLSTART; return Parser::make_PERCENTLBRACKET(*loc);}
<INCLSTART>\n                                   {BEGIN INCL; loc->lines(1); return Parser::make_ENDOFINCLUDEFILES(*loc);}
<INCLSTART>{identifier}                         {return Parser::make_IDENTIFIER(yytext, *loc);}
<INCLSTART>{whitespace}+                        {loc->step();}
<INCL>.                                         {return Parser::make_CCHAR(yytext, *loc);}
<INCL>\n                                        {loc->lines(1); return Parser::make_CCHAR(yytext, *loc);}
<INCL>^\%\}{whitespace}*\n                      {BEGIN NORMAL; loc->lines(1); return Parser::make_PERCENTRBRACKET(*loc);}

<NORMAL>"."                                     {return Parser::make_DOT(*loc);}
<NORMAL,CEXPR>","                               {return Parser::make_COMMA(*loc);}
<NORMAL>":"                                     {return Parser::make_COLON(*loc);}
<NORMAL>";"                                     {return Parser::make_SCOLON(*loc);}
<NORMAL>"("                                     {return Parser::make_LPAREN(*loc);}
<NORMAL>")"                                     {return Parser::make_RPAREN(*loc);}
<NORMAL,C>"{"                                   {return Parser::make_LCURLYPAREN(*loc);}
<NORMAL,C>"}"                                   {return Parser::make_RCURLYPAREN(*loc);}
<NORMAL>"|"                                     {return Parser::make_BAR(*loc);}
<NORMAL>\-\>                                    {return Parser::make_ARROW(*loc);}
<NORMAL>\[                                      {return Parser::make_LBRACKET(*loc);}
<NORMAL>\]                                      {return Parser::make_RBRACKET(*loc);}
<NORMAL>\-?{int}                                {return Parser::make_INT(std::stoi(yytext),*loc);}
<NORMAL>"*"                                     {return Parser::make_STAR(*loc);}
<NORMAL>"="                                     {return Parser::make_EQ(*loc);}
<NORMAL>">"                                     {return Parser::make_RANGLE(*loc);}
<NORMAL>"<"                                     {return Parser::make_LANGLE(*loc);}

<NORMAL>"list"                                  {return Parser::make_LIST(*loc);}
<NORMAL>"provided"                              {return Parser::make_PROVIDED(*loc);}

<NORMAL>{identifier}                            {return Parser::make_IDENTIFIER (yytext, *loc);}

<CEXPR>[^\n\\"';,\(\)\[\]$]+                    {return Parser::make_CEXPR(yytext, *loc);}
<NORMAL,CEXPR,C>\${int}                           {return Parser::make_DOLLARVAR(std::stoi(yytext+1),*loc);}
<CEXPR,CEXPRDQ,CEXPRSQ,C>\n                     {loc->lines(1); return Parser::make_NEWLINE(*loc);}
<CEXPR,CEXPRDQ,CEXPRSQ>\\|\\\\                  {return Parser::make_CEXPR(yytext, *loc);}
<NORMAL,C,CEXPR>\"                              {BEGIN CEXPRDQ; return Parser::make_DQUOTE(*loc);}
<NORMAL,C,CEXPR>\'                              {BEGIN CEXPRSQ; return Parser::make_SQUOTE(*loc);}
<CEXPR>\$                                       {return Parser::make_CEXPR(yytext, *loc);}
<CEXPR>;                                        {BEGIN NORMAL; reset_nestcount(); return Parser::make_SCOLON(*loc);}
<CEXPR>\(                                       {cinit_paren_nesting++; return Parser::make_LPAREN(*loc);}
<CEXPR>\)                                       {if(cinit_paren_nesting == 0){
                                                    BEGIN NORMAL; reset_nestcount(); return Parser::make_RPAREN(*loc);
                                                } else {
                                                    cinit_paren_nesting--; return Parser::make_RPAREN(*loc);
                                                }}
<CEXPR>\[                                       {cinit_array_nesting++; return Parser::make_LBRACKET(*loc);}
<CEXPR>\]                                       {if(cinit_array_nesting == 0){
                                                    BEGIN NORMAL; reset_nestcount(); return Parser::make_RPAREN(*loc);
                                                } else {
                                                    cinit_array_nesting--; return Parser::make_RPAREN(*loc);
                                                }}

<CEXPRDQ>[^\n\\"]+			                    {return Parser::make_CEXPR(yytext, *loc);}
<CEXPRDQ>\"				                        {BEGIN CEXPR; return Parser::make_DQUOTE(*loc);}
<CEXPRDQ>\\\"				                    {return Parser::make_CEXPR(yytext, *loc);}
<CEXPRSQ>[^\n\\']+			                    {return Parser::make_CEXPR(yytext, *loc);}
<CEXPRSQ>\'			                            {BEGIN CEXPR; return Parser::make_SQUOTE(*loc);}
<CEXPRSQ>\\\'				                    {return Parser::make_CEXPR(yytext, *loc);}

<C>\$\$                                         {return Parser::make_CLINE("$", *loc);}
<C>\\\\                                         {return Parser::make_CLINE(yytext, *loc);}
<C>\\                                           {return Parser::make_CLINE(yytext, *loc);}
<C>\$[0-9]*                                     {if (strcmp(yytext, "$") == 0) return Parser::make_CCHAR(yytext, *loc);
                                                return Parser::make_DOLLARVAR(std::stoi(yytext+1),*loc);}
<C>{identifier}                                 {return Parser::make_CLINE(yytext, *loc);}
<C>[^\n\{\}"'a-zA-Z_\$\\]+                      {return Parser::make_CLINE(yytext, *loc);}
<C>\/					                        {return Parser::make_CLINE(yytext, *loc);}
<C>\/\*[ \t]*EMPTY[ \t]*\*\/		            {return Parser::make_CLINE(yytext, *loc);}
<C>\/\*[ \t]*NOTREACHED[ \t]*\*\/	            {return Parser::make_CLINE(yytext, *loc);}
<C>\/\*[ \t]*SUPPRESS[ \t]*[0-9]+[ \t]*\*\/	    {return Parser::make_CLINE(yytext, *loc);}

<OPTION>{whitespace}*                           {loc->step();}
<OPTION>\n                                      {loc->lines(yyleng); loc->step(); BEGIN NORMAL;}
<OPTION>.                                       {loc->step();}

<<EOF>>                                         {return Parser::make_END (*loc);}
%%

void do_NORMAL()        {BEGIN NORMAL;}
void do_CEXPR()         {BEGIN CEXPR;}
void do_CEXPRDQ()       {BEGIN CEXPRDQ;}
void do_CEXPRSQ()       {BEGIN CEXPRSQ;}
void do_C()             {BEGIN C;}

int parsing::Parse(const std::string_view file, Declarations& decl)
{
    runtime::log("Parsing {}...", file);
    yy_flex_debug = runtime::args.scantrace_flag;
    runtime::Input current_input{file};
    loc = &current_input.get_location();
    yyin = current_input;
    Parser parse(decl);
    parse.set_debug_level(runtime::args.parsetrace_flag);
    return parse();
}

void reset_nestcount(){
    if(cinit_array_nesting != 0){
        //warning
    }
    if(cinit_paren_nesting != 0){
        //warning
    }
    cinit_array_nesting = 0;
    cinit_paren_nesting = 0;
}

int eat_comment(parsing::location& local){
    char c1, c2;
    int nest;
    int lines = 0;
    for(nest = 1, c2 = ' ';;){
        c1 = c2;
        c2 = yyinput();
        if(c2 == '\n'){
            local.lines();
            lines++;
        } else if(c2 == 0){
            //EOF error
        }
        if(c1 == '/' && c2 == '*'){
            nest++;
        } else if(c1 == '*' && c2 == '/'){
            nest--;
        }
        if(nest <= 0){
            break;
        }
    }
    local.step();
    return lines;
}

void eat_line_comment(parsing::location& local){
    char c = yyinput();
    while(c != '\n') c = yyinput();
    /*{
        if (c == EOF)
        {
            unput(c);
            break;
        }
        else
        {
            c = yyinput();
        }
    }*/
    local.lines(); local.step();
}
