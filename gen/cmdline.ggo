# ########################################################################################
# Term processor yeet. Definitions for commandline parameters
#
# Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
# University of Rostock, lead by Karsten Wolf, as well as yeet contributors
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# -- NOTE: Third party dependency used by this software --
# This software depends on the fmt lib (MIT License),
# and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
# ########################################################################################

usage "[OPTIONS] [FILES]"

args "--include-getopt --show-required --unamed-opts=FILE --no-handle-error"

description "About yeet:"

#versiontext "Yeet:
#Report bugs to <lukas.zech@uni-rostock.de>."



#############################################################################

section "Configuration"
option "loglevel" l
  "Set the log level"
  details="
Specify the log level
    0 - everything
    1 - errors, warnings and infos
    2 - errors and warnings
    3 - errors

"
  int
  default="3"
  optional

option "parsetrace" p
  "Trace parsing"
  details="
If set, yeet prints the parsing trace for debugging
"
  flag off

option "scantrace" s
  "Trace scanning"
  details="
If set, yeet prints the scanner trace for debugging
"
  flag off

option "file-prefix" f
  "Set output file prefix"
   details = "
Allows the use of a custom file prefix for generated files

"
  string
  typestr="PREFIX"
  default=""
  optional

option "namespace" n
  "Set namespace name for generated files"
   details = "
Allows the use of a custom name for the namespace of generated files. Default 'yeet'

"
  string
  typestr="NAME"
  default="yeet"
  optional

option "yystype" -
  "Generate header with yystype definition for bison/flex"
  details="
When given yeet will generate ${file-prefix}yystype header. This defines YYSTYPE
for bison and flex to parse into yeet structures.

"
  flag off

option "c-headers" -
  "Generate c-style headers (.h) instead of c++-style (.hh)"
  details="
When given yeet will generate header files as .h instead of .hh.
This is simply for compatibility with kimwitu++.

"
  flag off
