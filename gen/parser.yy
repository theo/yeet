/****************************************************************************************
  Term processor yeet. Parser rules

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file parser.yy
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Parser rules
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

/* C++ parser interface */
%skeleton "lalr1.cc"
/* required bison version*/
%require "3.3.2"
/* Standard yylex is not typesafe, this directive creates
make_TOKEN functions for use in the lexer */
%define api.token.constructor
/* use c++ variant types, needed to use c++ objects as semantic values, since default union doesn't work*/
%define api.value.type variant
/* assert correct cleanup of semantic values */
%define parse.assert
/* use the namespace "parsing" instead of "yy" */
%define api.namespace { parsing }
/* rename the parser class */
%define api.parser.class { Parser }
/* for passing input location */
%locations
/* but not creating locations.hh, because we only use it here */
%define api.location.file none
/* produce trace output */
%define parse.trace
/* additional error information */
%define parse.error verbose
/* inserted at top of parser.hh and parser.cc */
%code requires
{
    /* TODO : remove includes? */
    /*
    * Yeet
    * Parser.hh/.cc
    * last edited: 2020-06-05
    * authors: Nick Kotsakidis, Justin Kreikemeyer
    * Bison parser for .k-sepcifications.
    */
    #include <string>
    #include "tree/PhylumStatus.hh"
    #include "tree/Phylum.hh"
    #include "tree/PatternStatus.hh"
    #include "tree/Operator.hh"
    #include "tree/OutPatternStatus.hh"
    #include "tree/View.hh"
    #include "tree/RView.hh"
    #include "tree/UView.hh"
    #include "tree/OutmostPattern.hh"
    #include "tree/Pattern.hh"
    #include "tree/Rule.hh"
    #include "tree/RewriteClause.hh"
    #include "tree/RewriteTerm.hh"
    #include "tree/RewriteRule.hh"
    #include "tree/UnparseClause.hh"
    #include "tree/UnparseItem.hh"
    #include "tree/UnparseRule.hh"
    #include "tree/CodeRedirection.hh"
    #include "tree/RedirectionSymbol.hh"
    #include "runtime.hh"

    // Give Flex the prototype of yylex we want ...
    #define YY_DECL parsing::Parser::symbol_type yylex()
}
/* we give the parser the input file for locations
   and declarations to parse into */
%parse-param { parsing::Declarations& decl }
/* insert at top of parser.cc */
%code
{
    /* for AST-classes */
    #include "tree/PhylumStatus.hh"
    #include "tree/Phylum.hh"
    #include "tree/PatternStatus.hh"
    #include "tree/Operator.hh"
    #include "tree/OutPatternStatus.hh"
    #include "tree/View.hh"
    #include "tree/RView.hh"
    #include "tree/UView.hh"
    #include "tree/OutmostPattern.hh"
    #include "tree/Pattern.hh"
    #include "tree/Rule.hh"
    #include "tree/RewriteClause.hh"
    #include "tree/RewriteTerm.hh"
    #include "tree/RewriteRule.hh"
    #include "tree/UnparseClause.hh"
    #include "tree/UnparseItem.hh"
    #include "tree/UnparseRule.hh"
    #include "tree/CodeRedirection.hh"
    #include "tree/RedirectionSymbol.hh"
    #include "tree/Attribute.hh"
    #include "tree/CodeOption.hh"
    #include <string_view>
    extern void do_NORMAL();
    extern void do_CEXPR();
    extern void do_CEXPRDQ();
    extern void do_CEXPRSQ();
    extern void do_C();
    std::string currentPhylumDefinition = "";
    std::string currentPhylumListElementName = "";
    bool currentPhylumDefinitionIsList = false;
    std::string_view predefined_phyla[] = {"integer", "real", "casestring"};

     // ... and declare it for the parser's sake.
    YY_DECL;
}
/* prefix all tokens to avoid confusion */
%define api.token.prefix {TOK_}
/* define terminals */
%token
    END 0       "end of file"
    COMMA       ","
    COLON       ":"
    SCOLON      ";"
    LPAREN      "("
    RPAREN      ")"
    LCURLYPAREN "{"
    RCURLYPAREN "}"
    BAR         "|"
    LBRACKET    "["
    RBRACKET    "]"
    SQUOTE      "\'"
    DQUOTE      "\""
    EQ          "="
    STAR        "*"
    LANGLE      "<"
    RANGLE      ">"
    DOT 	    "."
    NEWLINE     "\n"
;
%token ARROW
/* define typed tokens */
%token <std::string> IDENTIFIER "identifier"
%token <std::string> CEXPR      "c expression"
%token <std::string> CCHAR      "c character, only matched in code includes"
%token <std::string> CLINE      "line of c/c++ code"
%token <int> CNEWLINES          "new lines because of comments in arbitrary c code"
%token <int> INT                "integer"
%token <int> DOLLARVAR          "dollar variable"
/* define non typed tokens */
%token LIST           "list modifier for phylum declaration"
%token PROVIDED       "provided modifier in pattern"
%token PERCENTUVIEW   "%uview"
%token PERCENTRVIEW   "%rview"
%token PERCENTLBRACKET "%{"
%token PERCENTRBRACKET "%}"
%token ENDOFINCLUDEFILES "placeholder token to identify end of includesfiles listing"
/* define types of non-terminals */
%type <std::vector<tree::Operator>> operatordeclarations
%type <tree::Operator> operatordeclaration
%type <std::vector<std::string>> operatorarguments
%type <std::vector<tree::OutmostPattern>> outmostpatterns
%type <tree::OutmostPattern> outmostpattern
%type <std::vector<tree::Pattern>> patterns
%type <tree::Pattern> pattern
%type <std::string> c_string
%type <std::vector<tree::UnparseClause>> unparseclauses
%type <tree::UnparseClause> unparseclause
%type <std::vector<tree::UnparseItem>> unparseitems
%type <tree::UnparseItem> unparseitem
%type <std::vector<tree::UView>> uviewnames
%type <std::vector<tree::RewriteClause>> rewriteclauses
%type <tree::RewriteClause> rewriteclause
%type <tree::RewriteTerm> term
%type <std::vector<tree::RewriteTerm>> termsoption
%type <std::vector<tree::RewriteTerm>> terms
%type <std::vector<tree::RView>> rviewnames
%type <tree::CodeRedirection> coderedirection
%type <std::vector<std::string>> redirectionsymbols;
%type <std::vector<std::string>> redirectionsymbols_option;
%type <tree::CodeOption> code_option;
%type <std::vector<tree::Attribute>> attributes_option;
%type <tree::Attribute> attribute;
%type <std::string> attribute_init;
%type <std::string> provided;

%type <std::string> c_expression
%type <std::string> c_expression_elem
%type <std::string> c_expression_inner
%type <std::string> c_expression_inner_elem
%type <std::string> c_expression_dq
%type <std::string> c_expression_dq_elem
%type <std::string> c_expression_sq
%type <std::string> c_expression_sq_elem

%type <std::string> c_body
%type <std::string> c_text
%type <std::string> c_text_elem

%start specification;

%%

/********/
/* Main */
/********/

specification:
    {   /* initizialize scanner state */
        do_NORMAL();
    }
    declarations
;

declarations:
    %empty
|   declarations declaration
;

declaration:
    phylumdeclaration
    { }
|   rewritedeclaration
    { }
|   unparsedeclaration
    { }
|   uviewdeclaration
    { }
|   rviewdeclaration
    { }
|   coderedirection
    { decl.code_redirections.push_back($1); }
;

/**********************/
/* Phylum Definitions */
/**********************/

// Defining the parts of an AST through Phyla and their Operators

phylumdeclaration:
/*If the phylum already exists, set its status to declared, doesnt matter if thats already the case, then add newly found operators
    If the phylum doesnt exist yet, create new one and add that to the overall vector, signifying it being created as a correct declaration*/
    IDENTIFIER {currentPhylumDefinition = $1; currentPhylumDefinitionIsList = false; currentPhylumListElementName = ""; } storageoption ":" operatordeclarations code_option ";"
    {   bool exists = false;
        for(auto & phy : decl.phylum_declarations){
            if(phy.GetName() == $1){
                exists = true;
                phy.SetCode($6);
                phy.SetStatus(tree::PhylumStatus::phylum_declared);
                phy.SetFileName(*@1.begin.filename);
                phy.SetLineNr(@1.begin.line);
                phy.SetList(currentPhylumDefinitionIsList);
                phy.SetElement(currentPhylumListElementName);
                for(auto & op : $5){
                    phy.AddOperator(op);
                }
            }
        }
        if(!exists){
            tree::Phylum phy($1, tree::PhylumStatus::phylum_declared, *@1.begin.filename, @1.begin.line, $6);
            phy.SetList(currentPhylumDefinitionIsList);
            phy.SetElement(currentPhylumListElementName);
            for(auto & op : $5){
                phy.AddOperator(op);
            }
            decl.phylum_declarations.push_back(phy);
        }
        currentPhylumDefinition = "";
    }
;

storageoption:
    %empty
    { }
;

operatordeclarations:
/*No operators? Pass an empty vector signifying that*/
    %empty
    {   $$ = std::vector<tree::Operator>(); }
/*One operator? Pass a vector containing that*/
|   operatordeclaration
    {   std::vector<tree::Operator> ops; ops.push_back($1); $$ = ops;
        if(decl.tree_statistics.GetMaxOperators() < $1.GetChildren().size()){
            decl.tree_statistics.SetMaxOperators($1.GetChildren().size());
        }
    }
/*Multiple operators? Add last to vector of already existing ones and pass the result*/
|   operatordeclarations "|" operatordeclaration
    {   $1.push_back($3); $$ = $1;
        if(decl.tree_statistics.GetMaxOperators() < $3.GetChildren().size()){
            decl.tree_statistics.SetMaxOperators($3.GetChildren().size());
        }
    }
/* List phylum? TODO */
|   LIST IDENTIFIER
    { $$ = std::vector<tree::Operator>();
      currentPhylumDefinitionIsList = true;
      currentPhylumListElementName = $2;
      bool exists = false;
      for(auto & predefined : predefined_phyla){
        if($2 == predefined){
            exists = true;
        }
      }
      for(auto & phy : decl.phylum_declarations){
          if(phy.GetName() == $2){
          exists = true;
          }
      }
      if(!exists){
          tree::Phylum phy($2, tree::PhylumStatus::phylum_referenced, *@2.begin.filename, @2.begin.line, tree::CodeOption());
          decl.phylum_declarations.push_back(phy);
      }
      tree::Operator op1("Cons" + currentPhylumDefinition, *@2.begin.filename, @2.begin.line);
      op1.AddChild($2);
      op1.AddChild(currentPhylumDefinition);
      tree::Operator op2("Nil" + currentPhylumDefinition, *@2.begin.filename, @2.begin.line);
      $$.push_back(op1);
      $$.push_back(op2);
      if(decl.tree_statistics.GetMaxOperators() < 2){
            decl.tree_statistics.SetMaxOperators(2);
      }
    }
;

operatordeclaration:
/*Create new operator with the parsed arguments and pass the result*/
    IDENTIFIER "(" operatorarguments ")"
    {   tree::Operator op($1, *@1.begin.filename, @1.begin.line);
        for(auto & phy : $3){
            op.AddChild(phy);
        }
        $$ = op;
    }
;

operatorarguments:
/*No arguments? Pass an empty vector signifying that*/
    %empty
    {   $$ = std::vector<std::string>();    }
/*Multiple arguments? Add last to vector of already existing ones and pass the result*/
|   operatorarguments IDENTIFIER
    {   bool exists = false;
        for(auto & predefined : predefined_phyla){
            if($2 == predefined){
                exists = true;
            }
        }
        for(auto & phy : decl.phylum_declarations){
            if(phy.GetName() == $2){
            exists = true;
            }
        }
        if(!exists){
            tree::Phylum phy($2, tree::PhylumStatus::phylum_referenced, *@2.begin.filename, @2.begin.line, tree::CodeOption());
            decl.phylum_declarations.push_back(phy);
        }
        $1.push_back($2); $$ = $1;
    }
;

code_option:
    %empty
    { $$ = tree::CodeOption(); }
|   LCURLYPAREN attributes_option init_option RCURLYPAREN
    { $$ = tree::CodeOption($2); }
;

attributes_option:
    %empty
    { $$ = std::vector<tree::Attribute>();}
|   attributes_option attribute
    { $1.push_back($2); $$ = $1; }
;

attribute:
    IDENTIFIER IDENTIFIER attribute_init SCOLON
    { $$ = tree::Attribute($1, $2, $3); }
;

attribute_init:
    %empty
    { $$ = "";}
|   EQ { do_CEXPR(); } c_expression
    { do_NORMAL();
      $$ = $3;
    }
;

init_option:
    %empty
    { }
;

/*******************************************/
/* Everything to do with C/C++ expressions */
/* So this is a lot of unneccessary stuff, */
/* since Prof. Wolf doesn't use DollarVar, */
/* this will just parse into a string,     */
/* which can simply be pasted in the       */
/* generated code.                         */
/* If more additions are made, converting  */
/* this to specific objects for code       */
/* generation will probably be needed, so  */
/* I will just leave this here             */
/*******************************************/

c_expression:
    %empty
    { $$ = ""; }
|   c_expression c_expression_elem
    { $$ = $1 + $2; }
;

c_expression_elem:
    CEXPR
    { $$ = $1; }
|   DOLLARVAR
    { $$ = std::to_string($1); }
|   NEWLINE
    { $$ = '\n'; }
|   DQUOTE c_expression_dq DQUOTE               /* differentiating between strings in double quote vs single quotes may be unneccessary beyond this point, adding the appropiate characters around the content */
    { $$ = '\"' + $2 + '\"'; }
|   SQUOTE c_expression_sq SQUOTE
    { $$ = '\'' + $2 + '\''; }
|   LBRACKET c_expression_inner RBRACKET
    { $$ = '['+ $2 + ']'; }
|   LPAREN c_expression_inner RPAREN
    { $$ = '(' + $2 + ')'; }
;

c_expression_inner:
    %empty
    { $$ = ""; }
|   c_expression_inner c_expression_inner_elem
    { $$ = $1 + $2; }
;

c_expression_inner_elem:
    c_expression_elem
    { $$ = $1; }
|   COMMA
    { $$ = ", "; }
;

c_expression_dq:
    %empty
    { $$ = ""; }
|   c_expression_dq c_expression_dq_elem
    { $$ = $1 + $2; }
;

c_expression_dq_elem:
    CEXPR
    { $$ = $1; }
|   NEWLINE
    { $$ = '\n'; }
;

c_expression_sq:
    %empty
    { $$ = ""; }
|   c_expression_sq c_expression_sq_elem
    { $$ = $1 + $2; }
;

c_expression_sq_elem:
    CEXPR
    { $$ = $1; }
|   NEWLINE
    { $$ = '\n'; }
;

c_string:
    %empty
    { $$ = ""; }
|   c_string CCHAR
    { $$ = $1 + $2; }
;

c_body:
    LCURLYPAREN { do_C(); } c_text { do_NORMAL(); } RCURLYPAREN
    { $$ = $3; }
;

c_text:
    %empty
    { $$ = ""; }
|   c_text c_text_elem
    { $$ = $1 + $2; }
;

c_text_elem:
    CLINE
    { $$ = $1; }
|   DOLLARVAR
    { if($1 == 0)
        {$$ = "__result__";
      } else {
        //error
      }
    }
|   NEWLINE
    { $$ = '\n'; }
|   CNEWLINES
    { $$ = '\n'; }
|   DQUOTE { do_CEXPRDQ(); } c_expression_dq DQUOTE { do_C();
     $$ = '\"' + $3 + '\"'; }
|   SQUOTE { do_CEXPRSQ(); } c_expression_sq SQUOTE { do_C();
     $$ = '\'' + $3 + '\''; }
|   c_body
    { do_C();
     $$ = '{' + $1 + '}'; }
;

/************/
/* Patterns */
/************/

// Operator-Tree Patterns

outmostpatterns:
    outmostpattern
    { $$ = std::vector<tree::OutmostPattern>(); $$.push_back($1); }
|   outmostpatterns "," outmostpattern
    { $1.push_back($3); $$ = $1; }
;

outmostpattern:
    IDENTIFIER provided
    {   $$ = tree::OutmostPattern($1, $2);
        for(auto phy : decl.phylum_declarations){
            for(auto op : phy.GetOperators()){
                if(op.GetName() == $1){
                    $$.SetWildcardLength(op.GetChildren().size());
                }
            }
        }
    }
|   IDENTIFIER "(" patterns ")" provided
    { $$ = tree::OutmostPattern($1, $3, $5);
        for(auto phy : decl.phylum_declarations){
            for(auto op : phy.GetOperators()){
                if(op.GetName() == $1){
                    $$.SetWildcardLength(op.GetChildren().size());
                }
            }
        }
    }
|   IDENTIFIER "=" outmostpattern
    { $$ = tree::OutmostPattern($1, $3); }
;

provided:
    %empty
    { $$ = ""; }
|   PROVIDED LPAREN { do_CEXPR(); } c_expression { do_NORMAL(); } RPAREN
    { $$ = $4; }
;

patterns:
    %empty
	{ $$ = std::vector<tree::Pattern>(); }
|   pattern
    { $$ = std::vector<tree::Pattern>(); $$.push_back($1); }
|   patterns "," pattern
    { $1.push_back($3); $$ = $1; }
;

pattern:
    IDENTIFIER
    { $$ = tree::Pattern($1, tree::PatternStatus::pattern_variable_name);}
|   IDENTIFIER "(" patterns ")"
    { $$ = tree::Pattern($1, $3); }
|   IDENTIFIER "=" pattern
    { $$ = tree::Pattern($1, $3); }
|   "*"
    { $$ = tree::Pattern(); }
|   DQUOTE {do_CEXPRDQ();} c_expression_dq DQUOTE
    { do_NORMAL();
    $$ = tree::Pattern($3, tree::PatternStatus::pattern_string_literal);
    }
|   INT
    { $$ = tree::Pattern($1);}
;

/*************/
/* Rewriting */
/*************/

// Rewriting rules of the form Pattern -> <view_names: new_tree :>;

rewritedeclaration:
    outmostpatterns ARROW rewriteclauses ";"
    {
    for(auto& p : $1){
        for(auto& rc : $3){
            for(auto& rr : decl.rewrite_rules){
                if(p == rr.GetPattern() && rc == rr.GetRewriteClause()){
                    for(auto& v1 : rc.GetViews()){
                        for(auto& v2 : rr.GetRewriteClause().GetViews()){
                            if(v1.GetName() == v2.GetName()){
                                runtime::log<runtime::level::warn>("Rule at line {} won't be matched", @1.begin.line);
                                break;
                            }
                        }
                    }
                    break;
                }
            }
            decl.rewrite_rules.push_back(tree::RewriteRule(p, rc, *@1.begin.filename, @1.begin.line));
        }
    }
    }
;

rewriteclauses:
    rewriteclause
    { $$ = std::vector<tree::RewriteClause>(); $$.push_back($1); }
|   rewriteclauses rewriteclause
    { $$ = $1;
    bool exists = false;
    for(auto& c : $$){
        if(c == $2){
            exists = true;
            for(auto& v1 : $2.GetViews()){
                bool view_exists = false;
                for(auto& v2 : c.GetViews()){
                    if(v1.GetName() == v2.GetName()){
                        view_exists = true;
                        break;
                    }
                }
                if(!view_exists){
                    c.GetViews().push_back(v1);
                }
            }
        }
    }
    if(!exists){
        $$.push_back($2);
    }
    }
;

rewriteclause:
    "<" rviewnames ":" term ">"
    { $$ = tree::RewriteClause($2, $4); }
|   "<" rviewnames ":" IDENTIFIER EQ term ">" c_body
    { $$ = tree::RewriteClause($2, $6, $4, $8); }
|   "<" rviewnames ":" term ">" c_body
    { $$ = tree::RewriteClause($2, $4, "__result__", $6); }
;

rviewnames:
    %empty
    { $$ = std::vector<tree::RView>(); }
|   rviewnames IDENTIFIER
    { $$ = $1;
    for(auto& v : decl.rewrite_views){
        if(v.GetName() == $2){
            $$.push_back(v);
        }
    }
    }
;

term:
    IDENTIFIER                                                              // variable
    { $$ = tree::RewriteTerm($1, tree::RewriteTermStatus::rterm_variable); }
|   IDENTIFIER "(" termsoption ")"                                                // operator with subterms
    { $$ = tree::RewriteTerm($1, $3); }
|   term ARROW IDENTIFIER "(" termsoption ")"                                     // method invocation, not implemented yet
    {   std::vector<tree::RewriteTerm> subTerm = std::vector<tree::RewriteTerm>(); subTerm.push_back($1);
        $$ = tree::RewriteTerm(subTerm, $3, $5, tree::RewriteTermStatus::rterm_method_arrow); }
|   term DOT IDENTIFIER "(" termsoption ")"                                       // method invocation, not implemented yet
    {   std::vector<tree::RewriteTerm> subTerm = std::vector<tree::RewriteTerm>(); subTerm.push_back($1);
        $$ = tree::RewriteTerm(subTerm, $3, $5, tree::RewriteTermStatus::rterm_method_dot); }
|   term ARROW IDENTIFIER                                                   // member variable, not implemented yet
    {   std::vector<tree::RewriteTerm> subTerms = std::vector<tree::RewriteTerm>(); subTerms.push_back($1);
        $$ = tree::RewriteTerm(subTerms, tree::RewriteTermStatus::rterm_member_arrow, $3); }
|   term DOT IDENTIFIER                                                     // member variable, not implemented yet
    {   std::vector<tree::RewriteTerm> subTerms = std::vector<tree::RewriteTerm>(); subTerms.push_back($1);
        $$ = tree::RewriteTerm(subTerms, tree::RewriteTermStatus::rterm_member_dot, $3); }
|   DQUOTE {do_CEXPRDQ();} c_expression_dq DQUOTE                                   // string literal
    { do_NORMAL();
      $$ = tree::RewriteTerm($3, tree::RewriteTermStatus::rterm_string_literal);
    }
|   INT                                                                     // integer literal
    { $$ = tree::RewriteTerm($1); }
;

termsoption:
    %empty
    { $$ = std::vector<tree::RewriteTerm>(); }
|   terms
    { $$ = $1; }
;

terms:
    term
    { $$ = std::vector<tree::RewriteTerm>(); $$.push_back($1); }
|   terms "," term
    { $1.push_back($3); $$ = $1; }
;

rviewdeclaration:
    PERCENTRVIEW defined_rviews ";"
    { }
;

defined_rviews:
    IDENTIFIER
    { bool exists = false;
    for(auto& v: decl.rewrite_views){
        if(v.GetName() == $1){
            exists = true;
        }
    }
    if(!exists){
        tree::RView rv($1, *@1.begin.filename, @1.begin.line);
        decl.rewrite_views.push_back(rv);
    }
    }
    | defined_rviews IDENTIFIER
    { bool exists = false;
    for(auto& v: decl.rewrite_views){
        if(v.GetName() == $2){
            exists = true;
        }
    }
    if(!exists){
        tree::RView rv($2, *@2.begin.filename, @2.begin.line);
        decl.rewrite_views.push_back(rv);
    }
    }
    | defined_rviews "," IDENTIFIER
    { bool exists = false;
    for(auto& v: decl.rewrite_views){
        if(v.GetName() == $3){
            exists = true;
        }
    }
    if(!exists){
        tree::RView rv($3, *@3.begin.filename, @3.begin.line);
        decl.rewrite_views.push_back(rv);
    }
    }
;

/*************/
/* Unparsing */
/*************/

// Unparsing through rules of form Pattern -> [viewnames: strings/attributes :];

unparsedeclaration:
    outmostpatterns ARROW unparseclauses ";"
    {
    for(auto& p : $1){
        for(auto& uc : $3){
            for(auto& ur : decl.unparse_rules){
                if(p == ur.GetPattern() && uc == ur.GetUnparseClause()){
                    for(auto& v1 : uc.GetViews()){
                        for(auto& v2 : ur.GetUnparseClause().GetViews()){
                            if(v1.GetName() == v2.GetName()){
                                runtime::log<runtime::level::warn>("Rule at line {} won't be matched", @1.begin.line);
                                break;
                            }
                        }
                    }
                    break;
                }
            }
            decl.unparse_rules.push_back(tree::UnparseRule(p, uc, *@1.begin.filename, @1.begin.line));
        }
    }
    }
;

unparseclauses:
    unparseclause
    { $$ = std::vector<tree::UnparseClause>(); $$.push_back($1); }
|   unparseclauses unparseclause
    { $$ = $1;
    bool exists = false;
    for(auto& c : $$){
        if(c == $2){
            exists = true;
            for(auto& v1 : $2.GetViews()){
                bool view_exists = false;
                for(auto& v2 : c.GetViews()){
                    if(v1.GetName() == v2.GetName()){
                        view_exists = true;
                        break;
                    }
                }
                if(!view_exists){
                    c.GetViews().push_back(v1);
                }
            }
        }
    }
    if(!exists){
        $$.push_back($2);
    }
    }
;

unparseclause:
    "[" uviewnames ":" unparseitems "]"
    { $$ = tree::UnparseClause($2, $4); }
;

uviewnames:
    %empty
    { $$ = std::vector<tree::UView>();}
|   uviewnames IDENTIFIER
    { $$ = $1;
    for(auto& v : decl.unparse_views){
        if(v.GetName() == $2){
            $$.push_back(v);
        }
    }
    }
;

unparseitems:
    %empty
	{ $$ = std::vector<tree::UnparseItem>(); }
|   unparseitems unparseitem
	{ $1.push_back($2), $$ = $1; }
;

unparseitem:
    DQUOTE {do_CEXPRDQ();} c_expression_dq DQUOTE
    { do_NORMAL();
    $$ = tree::UnparseItem($3, tree::UnparseItemStatus::unparse_string_literal);
    }
|   IDENTIFIER
    { $$ = tree::UnparseItem($1, tree::UnparseItemStatus::unparse_variable); }
|   c_body
    { $$ = tree::UnparseItem($1, tree::UnparseItemStatus::unparse_cbody); }
;

uviewdeclaration:
    PERCENTUVIEW defined_uviews ";"
    { }
;

defined_uviews:
    IDENTIFIER
    { bool exists = false;
    for(auto& v: decl.unparse_views){
        if(v.GetName() == $1){
            exists = true;
        }
    }
    if(!exists){
        tree::UView uv($1, *@1.begin.filename, @1.begin.line);
        decl.unparse_views.push_back(uv);
    }
    }
|   defined_uviews IDENTIFIER
    { bool exists = false;
    for(auto& v: decl.unparse_views){
        if(v.GetName() == $2){
            exists = true;
        }
    }
    if(!exists){
        tree::UView uv($2, *@2.begin.filename, @2.begin.line);
        decl.unparse_views.push_back(uv);
    }
    }
|   defined_uviews "," IDENTIFIER
    { bool exists = false;
    for(auto& v: decl.unparse_views){
        if(v.GetName() == $3){
            exists = true;
        }
    }
    if(!exists){
        tree::UView uv($3, *@3.begin.filename, @3.begin.line);
        decl.unparse_views.push_back(uv);
    }
    }
;

/********************/
/* Code Redirection */
/********************/

// C++ Code redirection enclosed by %{ and %}

coderedirection:
    PERCENTLBRACKET redirectionsymbols_option ENDOFINCLUDEFILES c_string PERCENTRBRACKET
    { $$ = tree::CodeRedirection($4, $2, *@1.begin.filename, @1.begin.line); }
;

redirectionsymbols_option:
    %empty
    { $$ = std::vector<std::string>(); $$.push_back("CODE"); }
|   redirectionsymbols
    { $$ = $1; }
;

redirectionsymbols:
    IDENTIFIER
    { $$ = std::vector<std::string>(); $$.push_back($1); }
    |
    redirectionsymbols IDENTIFIER
    { $1.push_back($2); $$ = $1; }
;

%%

namespace parsing {

void parsing::Parser::error (const location_type& l, const std::string& m){
    std::cerr << l << ": " << m << '\n';
}

}
