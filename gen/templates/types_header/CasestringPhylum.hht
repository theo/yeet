/** Insert a casestring phylum as new root of the tree.
 * \param[in] value The string value this phylum represents.
 * \returns A new casestring phylum.
 */
casestring mkcasestring(std::string value);

/** A phylum representing a string. */
class casestring_impl : public AbstractPhylum_impl{
    public:
        /** The string value of this phylum. */
        std::string name;
        /** Construct a new casestring phylum. Do not use. Use mkcasestring(...) instead. */
        casestring_impl(std::string value)
        {
            this->name = value;
#ifdef EnableDebuggingMode
    std::cout << "casestring created!" << std::endl;
#endif
        }
        /** Default destructor of the casestring phylum. */
        virtual ~casestring_impl() { }
        /** Return the type of this phylum. */
        PhylumType const phyType() const { return PhylumType::phy_casestring; }
        /** Return the operator type of this phylum. */
        OperatorType const opType() const { return OperatorType::op_casestring; }
        /** Unparse the tree starting at this operator.
         * \param[in] printer A function used to print the string supplied in the unparse rules.
         * \param[in] uview The view under which to unparse.
         */
        const void unparse(void (*printer)(const std::string&, const UView&), UView uview)
        {
            printer(this->name, uview);
        }
        /** Rewrite starting at this operator.
         * \param[in] rview The view under which to perform the rewriting.
         * \returns A new, rewritten, tree.
         */
        casestring rewrite(RView rview)
        {
            return std::static_pointer_cast<casestring_impl>(shared_from_this());
        }
    private:
        /** Phylum specific implementation of rewriting. */
        AbstractPhylum doRewrite(RView rview)
        {
            return shared_from_this();
        }
};
