/**
 * Automatic translation of {{ Filenames }}
 * using yeet version: {{ Version }}
 */

#pragma once
#include <string>
#include <memory>
#include <iostream>

#ifndef REALTYPE
#define REALTYPE double
#endif

#ifndef INTTYPE
#define INTTYPE int
#endif

namespace {{ Namespace }} {

enum class UView;
enum class RView;

// Section: PhylumTypeEnum
{{ PhylumTypeEnum }}

// Section: OperatorTypeEnum
{{ OperatorTypeEnum }}

// Section: ForwardDeclarations
/* We forward declare all phyla and operators here, so we can use them later. */
class AbstractPhylum_impl;
class AbstractPhylum_list_impl;
class casestring_impl;
class integer_impl;
class real_impl;
{{ ForwardDeclarations }}

// Section: General phylum_cast definition
// taking the original phylum (since its a shared ptr) by const reference doesn't
// increase refcount of the managed object
template <typename P, typename T>
P phylum_cast(const std::shared_ptr<T>& t)
{
    return std::static_pointer_cast<typename P::element_type>(t);
}

// Section: Convenient cast definitions
#define AbstractPhylum_cast(x) std::static_pointer_cast<AbstractPhylum_impl>(x);
#define AbstractPhylum_list_cast(x) std::static_pointer_cast<AbstractPhylum_list_impl>(x);
#define casestring_cast(x) std::static_pointer_cast<casestring_impl>(x);
#define integer_cast(x) std::static_pointer_cast<integer_impl>(x);
#define real_cast(x) std::static_pointer_cast<real_impl>(x);
{{ ConvenientCasts }}

// Section: Typedefs for convenience
/* These typedefs are for convenience, so you do not have to always write the full name. */
typedef std::shared_ptr<AbstractPhylum_impl> AbstractPhylum;
typedef std::shared_ptr<const AbstractPhylum_impl> c_AbstractPhylum;
typedef std::shared_ptr<AbstractPhylum_list_impl> AbstractPhylum_list;
typedef std::shared_ptr<const AbstractPhylum_list_impl> c_AbstractPhylum_list;
typedef std::shared_ptr<casestring_impl> casestring;
typedef std::shared_ptr<const casestring_impl> c_casestring;
typedef std::shared_ptr<integer_impl> integer;
typedef std::shared_ptr<const integer_impl> c_integer;
typedef std::shared_ptr<real_impl> real;
typedef std::shared_ptr<const real_impl> c_real;
{{ Typedefs }}

// Section: Global info fields
struct {{ Namespace }}_operator_info {
    std::string_view name;
    int num_sons;
};

constexpr {{ Namespace }}_operator_info operator_info[] = {
    {"__dummy__", 0},
    {"casestring", 0},
    {"integer", 0},
    {"real", 0},
{{ InfoStructs }}
};

}

using namespace {{ Namespace }};

{{ RedirectedCode }}

namespace {{ Namespace }} {

// Section: create functions to create phylum from optype and arguments
AbstractPhylum {{ Namespace }}_create(OperatorType optype{{ CreateChildList }});

// Section: AbstractPhylumClass
{{ AbstractPhylumClass }}

// Section: AbstractPhylumListClass
{{ AbstractPhylumListClass }}

// Section: IntegerPhylum
{{ IntegerPhylum }}

// Section: RealPhylum
{{ RealPhylum }}

// Section: CasestringPhylum
{{ CasestringPhylum }}

// Section: PhylumClasses
{{ PhylumClasses }}

// Section: OperatorClasses
{{ OperatorClasses }}

// Section: Creators
{{ Creators }}
}
