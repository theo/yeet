/** Insert an integer phylum as new root of the tree.
 * \param[in] value The integer value this phylum represents.
 * \returns A new integer phylum.
 */
integer mkinteger(INTTYPE value);

/** A phylum representing an integer. */
class integer_impl : public AbstractPhylum_impl {
    public:
        /** The integer value of this phylum. */
        INTTYPE value;
        /** Construct a new integer phylum. Do not use. Use mkinteger(...) instead. */
        integer_impl(INTTYPE value)
        {
            this->value = value;
#ifdef EnableDebuggingMode
    std::cout << "integer created!" << std::endl;
#endif
        }
        /** Default destructor of the integer phylum. */
        virtual ~integer_impl() { }
        /** Return the type of this phylum. */
        PhylumType const phyType() const { return PhylumType::phy_integer; }
        /** Return the operator type of this phylum. */
        OperatorType const opType() const { return OperatorType::op_integer; }
        /** Unparse the tree starting at this operator.
         * \param[in] printer A function used to print the string supplied in the unparse rules.
         * \param[in] uview The view under which to unparse.
         */
        const void unparse(void (*printer)(const std::string&, const UView&), UView uview)
        {
            printer(std::to_string(this->value), uview);
        }
        /** Rewrite starting at this operator.
         * \param[in] rview The view under which to perform the rewriting.
         * \returns A new, rewritten, tree.
         */
        integer rewrite(RView rview)
        {
            return std::static_pointer_cast<integer_impl>(shared_from_this());
        }
    private:
        /** Phylum specific implementation of rewriting. */
        AbstractPhylum doRewrite(RView rview)
        {
            return shared_from_this();
        }
};
