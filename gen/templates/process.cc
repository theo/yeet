/****************************************************************************************
  Term processor yeet. Generation of processed templates from simple text files

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file process.cc
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Process templates from simple files to header
 * @version 0.1
 * @date 2024-04-26
 *
 * @copyright Copyright (c) 2023 - present
 *
 */

#include <unistd.h>

#include <algorithm>
#include <cstring>
#include <fstream>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

#include <fmt/color.h>
#include <fmt/format.h>
#include <fmt/ostream.h>

// to create string_views from literals the easy way
using namespace std::string_view_literals;

// opening marker of a variable in a template
constexpr std::string_view lMarker = "{{";
// closing marker of a variable in a template
constexpr std::string_view rMarker = "}}";

// prefix to fill in the generated file first
// includes license notice and namespace openings
constexpr std::string_view content_prefix =
    R"xXx(/****************************************************************************************
  Term processor yeet. Template specifications generated before yeet compilation

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once

#include "generator/Template.hh"

namespace generator
{
namespace templates
{

)xXx";

// suffix to put at the end of the generated file
// namespace closings
constexpr std::string_view content_suffix = R"xXx(} // namespace templates
} // namespace generator
)xXx";

constexpr std::string_view help_string = R"xXx(Usage: [-h|-o FILENAME] [FILES]

About templates:
  -h,            Print help and exit

Configuration:
  -o FILENAME    Set output filename (default=`Templates.inc')
)xXx";

// abort with formatted error message
template <typename... Args>
void abort [[noreturn]] (fmt::format_string<Args...> fmt_str, Args&&... args)
{
    fmt::println(
        stderr, "[{}]: {}", fmt::styled("ERROR", fmt::fg(fmt::color::red)),
        fmt::format(fmt_str, std::forward<Args>(args)...)
    );
    exit(EXIT_FAILURE);
}

int main(int argc, char** argv)
{
    const char* output_filename = "Templates.inc";
    // handle cmdline options
    signed char c;
    while ((c = getopt(argc, argv, "ho:")) != -1)
    {
        if (c == 'h')
        {
            fmt::print("{}\n{}", VERSION_STRING, help_string);
            exit(EXIT_SUCCESS);
        }
        if (c == 'o')
        {
            output_filename = optarg;
            continue;
        }
        if (c == '?')
        {
            exit(EXIT_FAILURE);
        }
    }
    // accumulate templates objects as strings
    std::ostringstream templates;
    for (int arg = optind; arg < argc; ++arg)
    {
        // read input
        std::ifstream current_input(argv[arg], std::ios::in | std::ios::ate);
        if (!current_input.is_open())
        {
            abort("Could not open file \"{}\"", argv[arg]);
        }
        if (!current_input.good())
        {
            abort("Opening file \"{}\" failed", argv[arg]);
        }
        // determine size of input file to copy to string in memory in full for
        // performance
        size_t size = current_input.tellg();
        std::string file_content(size, '\0');
        current_input.seekg(0);
        // copy data into string
        if (!current_input.read(file_content.data(), size))
        {
            abort("Reading file \"{}\" failed", argv[arg]);
        }
        // reset size to accumulate size of substrings without variables
        size = 0;
        // string_view operations don't copy the content
        const std::string_view content = file_content;
        // lPos: location of last found lMarker
        // rPos: location of last found rMarker
        size_t lPos = 0, rPos = 0;
        std::vector<std::pair<std::string_view, std::string_view>> substrings;
        std::vector<std::pair<std::string_view, size_t>> variables;
        // a substring goes from the previous rMarker to the next lMarker
        while ((lPos = content.find(lMarker, rPos)) != std::string_view::npos)
        {
            // take the closest lMarker to the next rMarker
            // in theory we could also advance to the next rMarker and find the closest
            // lMarker, doesn't really matter "historisch gewachsen"
            size_t temp = content.find(rMarker, lPos);
            lPos = content.rfind(lMarker, temp);
            auto substring = content.substr(rPos, lPos - rPos);
            size += substring.size();
            rPos = temp;
            // advance into the variable ...
            size_t var_begin = lPos + lMarker.length();
            // ... and past whitespace
            while (" \t\n\f\r\v"sv.find_first_of(content.at(var_begin)) != std::string_view::npos)
            {
                var_begin++;
            }
            // move rPos into variable name from the right
            size_t var_end = rPos - 1;
            while (" \t\n\f\r\v"sv.find_first_of(content.at(var_end)) != std::string_view::npos)
            {
                var_end--;
            }
            // right index for substr needs to be exclusive
            var_end++;
            auto var_name = content.substr(var_begin, var_end - var_begin);
            // advance rPos to be the first char of the next substring
            rPos += rMarker.length();
            substrings.emplace_back(std::make_pair(substring, var_name));
            // do we already know the variable?
            // yes: increase occurence count
            // no: save
            auto it = variables.begin();
            while (it != variables.end())
            {
                if (it->first == var_name)
                {
                    it->second++;
                    break;
                }
                it++;
            }
            if (it == variables.end())
            {
                variables.emplace_back(std::make_pair(var_name, 1));
            }
        }
        // there is still content behind the last variable
        substrings.emplace_back(std::make_pair(content.substr(rPos), "\0"));
        size += content.size() - rPos;
        // replace Input.hht with Input_hht as variable names
        std::replace(argv[arg], argv[arg] + strlen(argv[arg]), '.', '_');
        auto current_filename = std::string_view(argv[arg]);
        // only basename of the input file
        current_filename = current_filename.substr(current_filename.find_last_of("/") + 1);
        // constexpr Template<#var, #sub> #name {#size, {#subs}, {#vars}};
        // print beginning
        fmt::print(
            templates, "constexpr Template<{0}, {1}> {2}{{\"{2}\", {3}, {{{{", variables.size(),
            substrings.size(), current_filename, size
        );
        // print variables + occurence numbers
        std::for_each(
            variables.begin(), variables.end(), [&templates](const auto& v)
            { fmt::print(templates, "{{\"{}\", {}}}, ", v.first, v.second); }
        );
        fmt::print(templates, "}}}},{{{{");
        // print substrings, use raw strings for safety
        std::for_each(
            substrings.begin(), substrings.end(), [&templates](const auto& v)
            { fmt::print(templates, "{{R\"xXx({})xXx\", \"{}\"}}, ", v.first, v.second); }
        );
        fmt::print(templates, "}}}}}};\n\n");
    }
    // we first create a temporary file and compare with existing files
    std::string temp_output_filename = fmt::format("{}.tmp", output_filename);
    std::ofstream tmp_output_file(temp_output_filename, std::ios::out);
    fmt::print(tmp_output_file, "{}{}{}", content_prefix, templates.str(), content_suffix);
    tmp_output_file.flush();
    tmp_output_file.close();
    std::ifstream out_file(output_filename, std::ios::ate | std::ios::binary);
    if (out_file.fail())
    {
        // Output file does not exist, so we can simply rename
        std::rename(temp_output_filename.c_str(), output_filename);
    }
    else
    {
        // Output file exists already, so we need to check for difference
        std::ifstream tmp_file(temp_output_filename, std::ios::ate | std::ios::binary);
        if (tmp_file.fail())
        {
            abort("Could not open temporary file \"{}\" for comparison", temp_output_filename);
        }
        else
        {
            bool equal = out_file.tellg() == tmp_file.tellg();
            if (equal)
            {
                // Files are the same length, need to compare contents
                out_file.seekg(0, std::ios::beg);
                tmp_file.seekg(0, std::ios::beg);
                equal = std::equal(
                    std::istreambuf_iterator<char>(out_file.rdbuf()),
                    std::istreambuf_iterator<char>(),
                    std::istreambuf_iterator<char>(tmp_file.rdbuf())
                );
            }
            if (equal)
            {
                // can remove tmp file
                std::remove(temp_output_filename.c_str());
            }
            else
            {
                // Different -> use new file
                std::remove(output_filename);
                std::rename(temp_output_filename.c_str(), output_filename);
            }
        }
    }
    exit(EXIT_SUCCESS);
}
