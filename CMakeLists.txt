# ########################################################################################
# Term processor yeet. Top Level CMakeLists
#
# Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
# University of Rostock, lead by Karsten Wolf, as well as yeet contributors
#
# The MIT License (MIT)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# -- NOTE: Third party dependency used by this software --
# This software depends on the fmt lib (MIT License),
# and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
# ########################################################################################

cmake_minimum_required(VERSION 3.10)

# set the project name
project(yeet VERSION 0.4.1 LANGUAGES C;CXX)

# export compile commands to json
# why do this?
# can be used in vscode c_cpp_properties.json to allow intellisense to find correct headers
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# disallow in-source builds
if(PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
    message(FATAL_ERROR "In source builds are not allowed. You should invoke "
        "CMake from a different directory or with a different folder for -B.")
endif()

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif()

# only allow CMake default build types
set(ALLOWED_BUILD_TYPES "Release;Debug;RelWithDebInfo;MinSizeRel")

if(NOT ${CMAKE_BUILD_TYPE} IN_LIST ALLOWED_BUILD_TYPES)
    message(FATAL_ERROR "Only ${ALLOWED_BUILD_TYPES} are allowed build types.\n"
        "Choose with -DCMAKE_BUILD_TYPE=$TYPE (default `Release`).")
endif()

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR})

# point to the sources
add_subdirectory(gen)
add_subdirectory(src)

# Prepare files for vscode usage
option(YEET_VSCODE_SUPPORT "Whether to patch a template .code-workspace file for the configuration" ON)

if(YEET_VSCODE_SUPPORT)
    include(cmake/vscode)
    patch_config_file(yeet ${CMAKE_CURRENT_LIST_DIR}/.vscode/c_cpp_properties.json)
endif()
