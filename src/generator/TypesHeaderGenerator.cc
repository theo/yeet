/****************************************************************************************
  Term processor yeet. Definitions for code generation of type declarations

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file TypesHeaderGenerator.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Implementation of code generation for type declarations
 * @version 0.1
 * @date 2024-04-26
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>

#include "runtime.hh"
#include "Template.hh"
#include "tree/Phylum.hh"
#include "TypesHeaderGenerator.hh"

using namespace std;
using namespace tree;

namespace generator
{
/* Construction */

TypesHeaderGenerator::TypesHeaderGenerator(
    const std::string& filePrefix, parsing::Declarations& decl
)
    : Generator(filePrefix, decl)
{
    if (runtime::args.c_headers_given)
    {
        fileSuffix = "k.h";
    }
    else
    {
        fileSuffix = "k.hh";
    }
    this->filename = filePrefix + fileSuffix;
}

TypesHeaderGenerator::~TypesHeaderGenerator() {}

/* Functions */

string TypesHeaderGenerator::Generate()
{
    const std::string_view integer = templates::IntegerPhylum_hht.render();
    const std::string_view real = templates::RealPhylum_hht.render();
    const std::string_view casestring = templates::CasestringPhylum_hht.render();
    const std::string_view abstractPhylumListClass = templates::AbstractPhylumList_hht.render();
    /* Fill the main template */
    vector<string> filenames;
    templates::Data data;
    data.emplace("Version", PACKAGE_STRING);
    data.emplace("Namespace", namespaceName);
    data.emplace("PhylumTypeEnum", this->GeneratePhylumTypeEnum());
    data.emplace("OperatorTypeEnum", this->GenerateOperatorTypeEnum());
    data.emplace("ForwardDeclarations", this->GenerateForwardDeclarations());
    data.emplace("ConvenientCasts", this->GenerateConvenientCasts());
    data.emplace("Typedefs", this->GenerateTypedefs());
    data.emplace("AbstractPhylumClass", templates::AbstractPhylum_hht.render());
    data.emplace("AbstractPhylumListClass", abstractPhylumListClass);
    data.emplace("IntegerPhylum", integer);
    data.emplace("RealPhylum", real);
    data.emplace("CasestringPhylum", casestring);
    data.emplace("PhylumClasses", this->GeneratePhylumClasses());
    data.emplace("OperatorClasses", this->GenerateOperatorClasses());
    data.emplace("Creators", this->GenerateCreators());
    data.emplace("RedirectedCode", this->GetRedirectionsFor("KC_TYPES_HEADER", filenames));
    data.emplace("Filenames", this->GetFileNamesFromList(filenames));
    data.emplace("InfoStructs", this->GenerateOperatorInfo());
    data.emplace("CreateChildList", this->GenerateOperatorChildren());
    return templates::TypesMain_hht.render(data);
}

const string TypesHeaderGenerator::GeneratePhylumTypeEnum()
{
    // start value, since dummy, casestring, int and real occupy values 0,1,2,3
    int iterator = 4;
    templates::Data data;
    data.emplace("PhylumTypes", "");
    for (auto phy = this->phylumDeclarations.begin(); phy != this->phylumDeclarations.end(); ++phy)
    {
        data.append_to("PhylumTypes", "\tphy_" + phy->GetName() + " = " + to_string(iterator));
        iterator++;
        if (next(phy) == this->phylumDeclarations.end())
        {
            data.append_to("PhylumTypes", "\n");
        }
        else
        {
            data.append_to("PhylumTypes", ",\n");
        }
    }
    return templates::PhylumTypeEnum_hht.render(data);
}

const string TypesHeaderGenerator::GenerateOperatorTypeEnum()
{
    // start value, since dummy, casestring, int and real occupy values 0,1,2,3
    int iterator = 4;
    templates::Data data;
    data.emplace("OperatorTypes", "");
    for (auto phy = this->phylumDeclarations.begin(); phy != this->phylumDeclarations.end(); ++phy)
    {
        auto ops = phy->GetOperators();
        for (auto op = ops.begin(); op != ops.end(); ++op)
        {
            data.append_to("OperatorTypes", "\top_" + op->GetName() + " = " + to_string(iterator));
            iterator++;
            if (next(phy) == this->phylumDeclarations.end() && next(op) == ops.end())
            {
                data.append_to("OperatorTypes", "\n");
            }
            else
            {
                data.append_to("OperatorTypes", ",\n");
            }
        }
    }
    return templates::OperatorTypeEnum_hht.render(data);
}

const std::string TypesHeaderGenerator::GeneratePhylumClasses()
{
    std::string templ;
    for (auto phy = this->phylumDeclarations.begin(); phy != this->phylumDeclarations.end(); ++phy)
    {
        if (phy->GetName() == "integer" || phy->GetName() == "casestring"
            || phy->GetName() == "real")
        {
            continue;
        }
        templates::Data data;
        data.emplace("Name", phy->GetName());
        data.emplace("LineNumber", to_string(phy->GetLineNr()));
        data.emplace("FileName", phy->GetFileName());
        string atts;
        auto customAtt = phy->GetCode().GetAttributes();
        for (auto att = customAtt.begin(); att != customAtt.end(); ++att)
        {
            string value = att->GetVal();
            if (value == "")
            {
                atts += "\t\t" + att->GetType() + " " + att->GetName() + ";\n";
            }
            else
            {
                atts += "\t\t" + att->GetType() + " " + att->GetName() + " = " + att->GetVal()
                    + ";\n";
            }
        }
        data.emplace("Attributes", atts);
        if (phy->IsList())
        {
            data.emplace("Element", phy->GetElementName());
            templ += templates::ListPhylum_hht.render(data);
        }
        else
        {
            templ += templates::Phylum_hht.render(data);
        }
    }
    return templ;
}

const std::string TypesHeaderGenerator::GenerateForwardDeclarations()
{
    string res;
    auto phyla = this->phylumDeclarations;
    for (auto phylum = phyla.begin(); phylum != phyla.end(); ++phylum)
    {
        templates::Data data;
        data.emplace("Name", phylum->GetName());
        res += templates::ForwardDeclaration_hht.render(data);
        auto operators = phylum->GetOperators();
        for (auto op = operators.begin(); op != operators.end(); ++op)
        {
            templates::Data opData;
            opData.emplace("Name", op->GetName());
            res += templates::ForwardDeclaration_hht.render(opData);
        }
    }
    return res;
}

const std::string TypesHeaderGenerator::GenerateConvenientCasts()
{
    string res;
    auto phyla = this->phylumDeclarations;
    for (auto phylum = phyla.begin(); phylum != phyla.end(); ++phylum)
    {
        templates::Data phyData;
        phyData.emplace("Name", phylum->GetName());
        res += templates::ConvenientCasts_hht.render(phyData);
        auto operators = phylum->GetOperators();
        for (auto op = operators.begin(); op != operators.end(); ++op)
        {
            templates::Data opData;
            opData.emplace("Name", op->GetName());
            res += templates::ConvenientCasts_hht.render(opData);
        }
    }
    return res;
}

const std::string TypesHeaderGenerator::GenerateTypedefs()
{
    string res = "";
    auto phyla = this->phylumDeclarations;
    for (auto phylum = phyla.begin(); phylum != phyla.end(); ++phylum)
    {
        templates::Data data;
        data.emplace("Name", phylum->GetName());
        res += templates::SharedPtrTypedef_hht.render(data);
        // auto operators = phylum->GetOperators();
        // for (auto op = operators.begin(); op != operators.end(); ++op)
        // {
        //                 templ.SetVariable("Child", op->GetName());
        // }
    }
    return res;
}

const std::string TypesHeaderGenerator::GenerateOperatorClasses()
{
    std::string templ;
    for (auto phy = this->phylumDeclarations.begin(); phy != this->phylumDeclarations.end(); ++phy)
    {
        auto operators = phy->GetOperators();
        for (auto op = operators.begin(); op != operators.end(); ++op)
        {
            templates::Data data;
            data.emplace("Name", op->GetName());
            data.emplace("Parent", phy->GetName());
            data.emplace("LineNumber", std::to_string(phy->GetLineNr()));
            data.emplace("FileName", phy->GetFileName());
            auto children = op->GetChildren();
            string childTemplStr;
            string argTemplStr;
            int argc = 0;
            for (auto child = children.begin(); child != children.end(); ++child)
            {
                string childname = opMemberPrefix + to_string(argc);
                childTemplStr += "\t" + *child + " " + childname + ";\n";
                argTemplStr += "const " + *child + "& " + childname;
                if (next(child) != children.end())
                {
                    argTemplStr += ", ";
                }
                argc++;
            }
            data.emplace("Children", childTemplStr);
            data.emplace("ConstructorArgs", argTemplStr);
            templ += templates::Operator_hht.render(data);
        }
    }

    return templ;
}

const std::string TypesHeaderGenerator::GenerateCreators()
{
    string creators = "";
    auto phyla = this->phylumDeclarations;
    for (auto phylum = phyla.begin(); phylum != phyla.end(); ++phylum)
    {
        auto operators = phylum->GetOperators();
        for (auto op = operators.begin(); op != operators.end(); ++op)
        {
            templates::Data data;
            data.emplace("Name", op->GetName());
            data.emplace("Type", op->GetName());
            auto children = op->GetChildren();
            string cargs = "";
            int argc = 1;
            for (auto child = children.begin(); child != children.end(); ++child)
            {
                cargs += "const " + *child + "& a" + to_string(argc);
                if (next(child) != children.end())
                {
                    cargs += ", ";
                }
                argc++;
            }
            data.emplace("Arguments", cargs);
            creators += templates::Creator_hht.render(data);
        }
    }
    return creators;
}

const std::string TypesHeaderGenerator::GenerateOperatorInfo()
{
    string info = "";
    auto phyla = this->phylumDeclarations;
    for (auto phylum = phyla.begin(); phylum != phyla.end(); ++phylum)
    {
        auto operators = phylum->GetOperators();
        for (auto op = operators.begin(); op != operators.end(); ++op)
        {
            info += "\t{\"" + op->GetName() + "\", " + to_string(op->GetChildren().size()) + "},";
            if (!(next(phylum) == phyla.end() && next(op) == operators.end()))
            {
                info += "\n";
            }
        }
    }
    return info;
}

const std::string TypesHeaderGenerator::GenerateOperatorChildren()
{
    string info = "";
    for (int i = 0; i < this->statistics.GetMaxOperators(); i++)
    {
        info += ", const AbstractPhylum& __phy" + to_string(i) + " = nullptr";
    }
    return info;
}

}  // namespace generator
