/****************************************************************************************
  Term processor yeet. Definitions for transformation to file generation friendly rules

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file FriendlyTransformer.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definitions for transformation to friendly rules
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "FriendlyTransformer.hh"

#include "../tree/RewriteRule.hh"
#include "../tree/UnparseRule.hh"
#include "FriendlyRewriteClause.hh"
#include "FriendlyRewriteRule.hh"
#include "FriendlyUnparseClause.hh"
#include "FriendlyUnparseRule.hh"

using namespace tree;

namespace generator
{
std::vector<FriendlyUnparseRule> FriendlyTransformer::Transform(std::vector<UnparseRule> rules)
{
    std::vector<FriendlyUnparseRule> ret = std::vector<FriendlyUnparseRule>();
    for (auto rule = rules.begin(); rule != rules.end(); ++rule)
    {
        auto lineNr = rule->GetLineNr();
        auto fileName = rule->GetFileName();

        auto opattern = rule->GetPattern();
        auto uclause = rule->GetUnparseClause();
        auto views = uclause.GetViews();
        for (auto view = views.begin(); view != views.end(); ++view)
        {
            FriendlyUnparseClause frUnpCl(uclause.GetUnparseItems());
            FriendlyUnparseRule fr(*view, opattern, frUnpCl, lineNr, fileName);
            ret.push_back(fr);
        }
    }
    return ret;
}

std::vector<FriendlyRewriteRule> FriendlyTransformer::Transform(std::vector<RewriteRule> rules)
{
    /* Analogous to the function for unparse rules */
    std::vector<FriendlyRewriteRule> ret = std::vector<FriendlyRewriteRule>();
    for (auto rule = rules.begin(); rule != rules.end(); ++rule)
    {
        auto lineNr = rule->GetLineNr();
        auto fileName = rule->GetFileName();

        auto opattern = rule->GetPattern();
        auto rclause = rule->GetRewriteClause();
        auto views = rclause.GetViews();
        for (auto view = views.begin(); view != views.end(); ++view)
        {
            FriendlyRewriteClause frRewCl(
                rclause.GetRewriteTerms(), rclause.GetResultName(), rclause.GetCode()
            );
            FriendlyRewriteRule fr(*view, opattern, frRewCl, lineNr, fileName);
            ret.push_back(fr);
        }
    }
    return ret;
}

}  // namespace generator
