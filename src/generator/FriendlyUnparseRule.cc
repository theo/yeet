/****************************************************************************************
  Term processor yeet. Definitions for file generation friendly unparse rules

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file FriendlyUnparseRule.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definitions for friendly unparse rules
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "FriendlyUnparseRule.hh"

#include "../tree/OutmostPattern.hh"
#include "../tree/UView.hh"

using namespace tree;

namespace generator
{
tree::OutmostPattern& FriendlyUnparseRule::GetPattern() { return this->pattern; }

FriendlyUnparseRule::FriendlyUnparseRule(
    tree::UView view, tree::OutmostPattern pattern, FriendlyUnparseClause clause, int lineNr,
    std::string fileName
)
{
    this->view = view;
    this->pattern = pattern;
    this->clause = clause;
    this->lineNr = lineNr;
    this->fileName = fileName;
}

FriendlyUnparseClause& FriendlyUnparseRule::GetFriendlyUnparseClause() { return this->clause; }

bool FriendlyUnparseRule::ForView(UView view) { return (this->view.GetName() == view.GetName()); }

bool FriendlyUnparseRule::ForView(const std::string& name)
{
    return (this->view.GetName() == name);
}

bool FriendlyUnparseRule::MatchesOperator(Operator op)
{
    if (this->pattern.GetStatus() == tree::outpattern_wildcardOperator
        || this->pattern.GetStatus() == tree::outpattern_subtermOperator)
    {
        return op.GetName() == this->pattern.GetID();
    }
    else if (this->pattern.GetStatus() == tree::outpattern_variable_assignment)
    {
        return op.GetName() == this->pattern.GetSubPattern().GetID();
    }
    return false;
}

std::ostream& operator<<(std::ostream& os, const FriendlyUnparseRule& upr)
{
    os << upr.pattern << " -> " << "[" << upr.view.GetName() << ": " << upr.clause;
    return os;
}

int FriendlyUnparseRule::GetLineNr() { return this->lineNr; }
std::string FriendlyUnparseRule::GetFileName() { return this->fileName; }
void FriendlyUnparseRule::SetLineNr(int lineNr) { this->lineNr = lineNr; }
void FriendlyUnparseRule::SetFileName(const std::string& fileName) { this->fileName = fileName; }

}  // namespace generator
