/****************************************************************************************
  Term processor yeet. Definitions for code generation of rewriting declarations

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file RewriteHeaderGenerator.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definitions for code generation of rewriting declarations
 * @version 0.1
 * @date 2024-04-26
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <algorithm>

#include <fmt/format.h>

#include "RewriteHeaderGenerator.hh"
#include "runtime.hh"
#include "Template.hh"

using namespace std;
using namespace tree;

namespace generator
{
RewriteHeaderGenerator::RewriteHeaderGenerator(
    const std::string& filePrefix, parsing::Declarations& decl
)
    : Generator(filePrefix, decl)
{
    if (runtime::args.c_headers_given)
    {
        fileSuffix = "rk.h";
    }
    else
    {
        fileSuffix = "rk.hh";
    }
    this->filename = filePrefix + fileSuffix;
}

RewriteHeaderGenerator::~RewriteHeaderGenerator() {}

string RewriteHeaderGenerator::Generate()
{
    vector<string> filenames;
    /* Fill the main template */
    templates::Data data;
    data.emplace("Version", PACKAGE_STRING);
    data.emplace("Namespace", namespaceName);
    data.emplace("RViewEnum", this->GenerateRViewEnum());
    data.emplace("RedirectedCode", this->GetRedirectionsFor("KC_REWRITE_HEADER", filenames));
    data.emplace("Filenames", this->GetFileNamesFromList(filenames));
    data.emplace("FilePrefix", this->filePrefix);
    data.emplace("MainFileSuffix", runtime::args.c_headers_given ? "k.h" : "k.hh");
    return templates::RewriteMain_hht.render(data);
}

const string RewriteHeaderGenerator::GenerateRViewEnum()
{
    templates::Data data;
    if (this->rViews.size() > 0)
    {
        data.emplace("RViews", ",\n");
    }
    for (auto v = this->rViews.begin(); v != this->rViews.end(); ++v)
    {
        data.append_to("RViews", "\t" + v->GetName());
        if (next(v) == this->rViews.end())
        {
            data.append_to("RViews", "\n");
        }
        else
        {
            data.append_to("RViews", ",\n");
        }
    }
    return templates::RViewEnum_hht.render(data);
}
}  // namespace generator
