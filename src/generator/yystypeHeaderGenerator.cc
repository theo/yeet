/****************************************************************************************
  Term processor yeet. Definitions for code generation of bison/flex types definitions

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file yystypeHeaderGenerator.cc
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Implementation of code generation for yystype declarations
 * @version 0.1
 * @date 2024-04-26
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>

#include "Template.hh"
#include "tree/Phylum.hh"
#include "runtime.hh"
#include "yystypeHeaderGenerator.hh"

using namespace std;
using namespace tree;

namespace generator
{
/* Construction */

yystypeHeaderGenerator::yystypeHeaderGenerator(
    const std::string& filePrefix, parsing::Declarations& decl
)
    : Generator(filePrefix, decl)
{
    if (runtime::args.c_headers_given)
    {
        fileSuffix = "yystype.h";
    }
    else
    {
        fileSuffix = "yystype.hh";
    }
    this->filename = filePrefix + fileSuffix;
}

yystypeHeaderGenerator::~yystypeHeaderGenerator() {}

/* Functions */

string yystypeHeaderGenerator::Generate()
{
    /* Fill the main template */
    templates::Data data;
    data.emplace("Version", PACKAGE_STRING);
    data.emplace("Namespace", namespaceName);
    std::string namespaceNameCaps = namespaceName;
    std::transform(
        namespaceName.begin(), namespaceName.end(), namespaceNameCaps.begin(), ::toupper
    );
    data.emplace("NamespaceCaps", namespaceNameCaps);
    data.emplace("yystypeDeclarations", this->GenerateyystypeDeclarations());
    return templates::yystypeMain_hht.render(data);
}

const string yystypeHeaderGenerator::GenerateyystypeDeclarations()
{
    string info = "";
    for (auto phy = this->phylumDeclarations.begin(); phy != this->phylumDeclarations.end(); ++phy)
    {
        info += "\t" + namespaceName + "::" + phy->GetName() + " yt_" + phy->GetName() + ";";
        if (next(phy) != this->phylumDeclarations.end())
        {
            info += "\n";
        }
    }
    return info;
}

}  // namespace generator
