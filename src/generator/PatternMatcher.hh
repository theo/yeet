/****************************************************************************************
  Term processor yeet. Declarations for code generation to match patterns

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file PatternMatcher.hh
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Declarations to match patterns in generated code
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <map>

#include "../tree/OutmostPattern.hh"

namespace generator
{
/** Class with static methods to retrieve usable information by recursively traversing
 * patterns. */
class PatternMatcher
{
public:
    /** Recursive method over an OutmostPattern to generate code for detecting a pattern
     * and retrieving the defined variables in the pattern. If on the top layer, call with
     * carry = "" and argc = 0.
     *
     * \param[in] pattern The pattern to convert to c++ code.
     * \param[in] opMemberPrefix Prefix to use for operator arguments. This should be the
     * supplied by a generator. \param[in] opRewritePrefix Prefix used when getting
     * pattern information for rewriting as the condition has to be slightly different
     * (uses l_arg_x, the rewritten children, in the top level). \param[in] carry
     * Carry-over argument access path. Carries prefixes containing the right chaining of
     * castings and argument accesses for the current depth for the condition through the
     * recursion. \param[in] argc Carries a counter that defines which member of the
     * current node (operator) in the AST we inspect. \param[out] condition This will be
     * filled with a string containing a C++-style boolean condition that checks for a
     * match on the pattern on the ast. Note this condition has to be augmented with
     * conditions for provided and equals, which can be generated from the variables.
     * \param[out] variables This will be filled with a map containing variable names as
     * keys and expressions to retrieve the patterns variables from the ast as values.
     */
    static void GetPatternInformation(
        tree::OutmostPattern& pattern, const std::string& opMemberPrefix,
        const std::string& opRewritePrefix, int argc, std::ostringstream& condition,
        std::map<std::string, std::string>& variables
    );

private:
    /** Recursive method over a pattern to generate code for detecting a pattern and
     * retrieving the defined variables in the pattern.
     *
     * \param[in] pattern The pattern to convert to c++ code.
     * \param[in] opMemberPrefix Prefix to use for operator arguments. This should be the
     * supplied by a generator. \param[in] opRewritePrefix Prefix used when getting
     * pattern information for rewriting as the condition has to be slightly different
     * (uses l_arg_x, the rewritten children, in the top level). \param[in] carry
     * Carry-over argument access path. Carries prefixes containing the right chaining of
     * castings and argument accesses for the current depth for the condition through the
     * recursion. \param[in] argc Carries a counter that defines which member of the
     * current node (operator) in the AST we inspect. \param[out] condition This will be
     * filled with a string containing a C++-style boolean condition that checks for a
     * match on the pattern on the ast. Note this condition has to be augmented with
     * conditions for provided and equals, which can be generated from the variables.
     * \param[out] variables This will be filled with a map containing variable names as
     * keys and expressions to retrieve the patterns variables from the ast as values.
     */
    static void GetPatternInformation(
        tree::Pattern& pattern, const std::string& opMemberPrefix,
        const std::string& opRewritePrefix, std::string carry, int argc,
        std::ostringstream& condition, std::map<std::string, std::string>& variables
    );

    /** Retrieve information about the provided statement that can be present after a
     * pattern.
     *
     * This function uses std::regex_find to replace all variable names in the pattern
     * with their access path in the tree while ensuring that only things that are meant
     * to be variables are replaced.
     *
     * \param[in] pattern The pattern to get the information for.
     * \param[out] condition The generated condition that checks the provided statement.
     * \param[in] variables The map from variable name to access path retrieved from
     * GetPatternInformation.
     */
    static void GetProvidedInformation(
        tree::OutmostPattern& pattern, std::ostringstream& condition,
        std::map<std::string, std::string>& variables
    );
};
}  // namespace generator
