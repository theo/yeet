/****************************************************************************************
  Term processor yeet. Definitions for code generation of rewriting definitions

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file RewriteClassGenerator.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definitions for code generation of rewriting definitions
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>

#include "FriendlyRewriteRule.hh"
#include "FriendlyTransformer.hh"
#include "RewriteClassGenerator.hh"
#include "runtime.hh"
#include "Template.hh"

using namespace std;
using namespace tree;

namespace generator
{
/****************
 * Construction *
 ****************/

RewriteClassGenerator::RewriteClassGenerator(
    const std::string& filePrefix, parsing::Declarations& decl
)
    : Generator(filePrefix, decl)
{
    this->filename = filePrefix + fileSuffix;
}

RewriteClassGenerator::~RewriteClassGenerator() {}
RewriteClassGenerator::RewriteClassGenerator() {}

/**************
 * Generation *
 **************/

string RewriteClassGenerator::Generate()
{
    vector<string> filenames;
    templates::Data data;
    data.emplace("Version", PACKAGE_STRING);
    data.emplace("Namespace", namespaceName);
    data.emplace("RedirectedCode", this->GetRedirectionsFor("KC_REWRITE", filenames));
    data.emplace("RewriteFunction", this->GenerateRewriteImpl());
    data.emplace("Filenames", this->GetFileNamesFromList(filenames));
    data.emplace("FilePrefix", this->filePrefix);
    data.emplace("HeaderSuffix", runtime::args.c_headers_given ? "h" : "hh");
    return templates::RewriteMain_cct.render(data);
}

const string RewriteClassGenerator::GenerateHeaderName()
{
    int differentFilePath = filePrefix.find_last_of("/");
    differentFilePath = differentFilePath != -1 ? differentFilePath + 1 : 0;
    return filePrefix.substr(differentFilePath, filePrefix.find_last_of(".") - differentFilePath)
        + string(runtime::args.c_headers_given ? ".h" : ".hh");
}

/*************
 * Rewriting *
 *************/

const std::string RewriteClassGenerator::GenerateRewriteImpl()
{
    std::string templ;
    /* Iterate over all (phyla and) operators*/
    for (auto phy = this->phylumDeclarations.begin(); phy != this->phylumDeclarations.end(); ++phy)
    {
        auto operators = phy->GetOperators();
        for (auto op = operators.begin(); op != operators.end(); ++op)
        {
            templates::Data data;
            data.emplace("Namespace", namespaceName);
            data.emplace("Name", op->GetName());
            data.emplace("Parent", phy->GetName());
            /* Generate default rewriting string (calling rewrite on all children) */
            auto children = op->GetChildren();
            string rewChildren;
            string childlist;
            string defRewMethodStr;
            string childCompareList;
            int argc = 0;
            if (children.size() != 0)
            {
                for (auto child = children.begin(); child != children.end(); ++child)
                {
                    string argnstr = to_string(argc);
                    rewChildren += "\tauto " + opRewritePrefix + argnstr + " = this->"
                        + opMemberPrefix + argnstr + "->rewrite(" + namespaceName
                        + "_current_view);\n";
                    childlist += opRewritePrefix + argnstr;
                    childCompareList += opRewritePrefix + argnstr + " != " + opMemberPrefix
                        + argnstr;
                    if (next(child) != children.end())
                    {
                        childlist += ", ";
                        childCompareList += " || ";
                    }
                    argc++;
                }
                defRewMethodStr += "\tif (" + childCompareList
                    + ")\n\t{\n\t\ttopmost_node = " + op->GetName() + "(" + childlist + ");\n\t}";
            }

            data.emplace("RewriteDefault", defRewMethodStr);
            data.emplace("RewriteChildren", rewChildren);

            /* For each view generate a case in the switch for unparsing */
            string caseStr;
            bool needSwitch = false;
            for (auto view = this->rViews.begin(); view != this->rViews.end(); ++view)
            {
                std::vector<FriendlyRewriteRule> matchingRules;
                /* Find the patterns that match the current operator and view */
                for (auto rule = this->rewriteRules.begin(); rule != this->rewriteRules.end();
                     ++rule)
                {
                    if (rule->ForView(*view) && rule->MatchesOperator(*op))
                    {
                        matchingRules.push_back(*rule);
                    }
                }

                /* If no patterns match, handle through default unparse. */
                if (matchingRules.size() != 0)
                {
                    /** Check if switch is already generated */
                    if (!needSwitch)
                    {
                        caseStr = "\tswitch (" + this->namespaceName + "_current_view)\n\t{\n";
                        needSwitch = true;
                    }
                    /* Add this view to the list of cases */
                    caseStr += "\tcase RView::" + view->GetName() + ":\n\t{\n";
                    for (auto match = matchingRules.begin(); match != matchingRules.end(); match++)
                    {
                        ostringstream condition;
                        std::map<std::string, std::string> variables;
                        ostringstream clause;
                        /* generate condition */
                        PatternMatcher::GetPatternInformation(
                            match->GetPattern(), opMemberPrefix, opRewritePrefix, 0, condition,
                            variables
                        );
                        string conditionStr = condition.str();
                        if (conditionStr == "")
                        {
                            conditionStr = "true";
                        }

                        /* generate body */
                        GenerateRewriteBody(*match, clause, variables, "\t\t\t");
                        /** Generate return statement */
                        clause << lineReset << "\n\t\t\tif (phylum_cast<" << phy->GetName()
                               << ">(__result__) == phylum_cast<" << phy->GetName()
                               << ">(topmost_node))\n\t\t\t{"
                               << "\n\t\t\t\treturn "
                                  "topmost_node;\n\t\t\t}\n\t\t\telse\n\t\t\t{"
                                  "\n\t\t\t\treturn __result__->rewrite("
                               << this->namespaceName << "_current_view);\n\t\t\t}\n";

                        /* Add pattern as comment before the if statement */
                        ostringstream mm;
                        mm << match->GetPattern();
                        caseStr += "\t\t/*" + mm.str() + "*/\n";
                        caseStr += "#line " + to_string(match->GetLineNr()) + " \""
                            + match->GetFileName() + "\"\n";

                        /* If this is the first pattern in the list, start with nothing */
                        if (match == matchingRules.begin())
                        {
                            caseStr += "\t\t";
                        }
                        /* If this is not the first pattern, add an else*/
                        else
                        {
                            caseStr += "\t\telse ";
                        }
                        /* Add the if statement and body to the current case */
                        caseStr += "if (" + conditionStr + ")\n\t\t{\n" + clause.str() + "\t\t}\n";
                    }
                    /* Add default unparsing as last option. TODO: move into loop. */
                    caseStr += "\t\t//If no patterns match, break out of the switch and "
                               "go to default\n\t\tbreak;\n\t}\n"
                        + lineReset + "\n";
                }
            }
            /** If something was generated, need to generate default and close switch*/
            if (needSwitch)
            {
                caseStr += "\tdefault:;\n\t}\n";
            }

            data.emplace("RewritingCases", caseStr);

            /** Handle rules with empty viewlists or which explicitely match on
             * DefaultRView */
            string defaultViewCases;
            std::vector<FriendlyRewriteRule> matchingRules;
            /* Find the patterns that match the current operator and view */
            for (auto rule = this->rewriteRules.begin(); rule != this->rewriteRules.end(); ++rule)
            {
                if (rule->ForView("") && rule->MatchesOperator(*op))
                {
                    matchingRules.push_back(*rule);
                }
            }

            /* If no patterns match, handle through default unparse. */
            if (matchingRules.size() != 0)
            {
                for (auto match = matchingRules.begin(); match != matchingRules.end(); match++)
                {
                    ostringstream condition;
                    std::map<std::string, std::string> variables;
                    ostringstream clause;
                    /* generate condition */
                    PatternMatcher::GetPatternInformation(
                        match->GetPattern(), opMemberPrefix, opRewritePrefix, 0, condition,
                        variables
                    );
                    string conditionStr = condition.str();
                    if (conditionStr == "")
                    {
                        conditionStr = "true";
                    }

                    /* generate body */
                    GenerateRewriteBody(*match, clause, variables, "\t\t");
                    /** Generate return statement */
                    clause << lineReset << "\n\t\t\tif (phylum_cast<" << phy->GetName()
                           << ">(__result__) == phylum_cast<" << phy->GetName()
                           << ">(topmost_node))\n\t\t\t{"
                           << "\n\t\t\t\treturn "
                              "topmost_node;\n\t\t\t}\n\t\t\telse\n\t\t\t{"
                              "\n\t\t\t\treturn __result__->rewrite("
                           << this->namespaceName << "_current_view);\n\t\t\t}\n";

                    /* Add pattern as comment before the if statement */
                    ostringstream mm;
                    mm << match->GetPattern();
                    defaultViewCases += "\t/*" + mm.str() + "*/\n";
                    defaultViewCases += "#line " + to_string(match->GetLineNr()) + " \""
                        + match->GetFileName() + "\"\n";

                    /* If this is the first pattern in the list, start with nothing */
                    if (match == matchingRules.begin())
                    {
                        defaultViewCases += "\t";
                    }
                    /* If this is not the first pattern, add an else*/
                    else
                    {
                        defaultViewCases += "\telse ";
                    }
                    /* Add the if statement and body to the current case */
                    defaultViewCases += "if (" + conditionStr + ")\n\t{\n" + clause.str() + "\t}\n";
                }
            }

            data.emplace("RewriteEmptyViews", defaultViewCases);

            templ += templates::RewriteFunction_cct.render(data);
        }
    }
    return templ;
}

void RewriteClassGenerator::GenerateRewriteBody(
    FriendlyRewriteRule& pattern, ostringstream& clause,
    std::map<std::string, std::string> variables, const std::string& indent
)
{
    clause << indent << "// Body\n";

    for (auto pair : variables)
    {
        clause << "#line " << pattern.GetLineNr() << " \"" << pattern.GetFileName() << "\"\n"
               << indent << "auto " << pair.first << " = " << pair.second << ";\n";
    }
    clause << "#line " << pattern.GetLineNr() << " \"" << pattern.GetFileName() << "\"\n"
           << indent << "auto __result__ = ";
    auto terms = pattern.GetFriendlyRewriteClause().GetRewriteTerms();
    this->TraverseTerm(terms, clause);
    clause << ";\n";
    string variable = pattern.GetFriendlyRewriteClause().GetVariableReference();
    if (variable != "__result__")
    {
        clause << "#line " << pattern.GetLineNr() << " \"" << pattern.GetFileName() << "\"\n"
               << indent << "auto& " << variable << " = __result__;\n";
    }
    clause << pattern.GetFriendlyRewriteClause().GetCode() << "\n";
}

void RewriteClassGenerator::TraverseTerm(RewriteTerm& term, ostringstream& clause)
{
    if (term.GetStatus() == RewriteTermStatus::rterm_operator)
    {
        clause << term.GetContent() << "(";
        this->TraverseSubTerms(term, clause);
    }
    else if (term.GetStatus() == RewriteTermStatus::rterm_method_arrow)
    {
        clause << "(";
        this->TraverseTerm(term.GetSubterm(), clause);
        clause << ")->" << term.GetContent() << "(";
        this->TraverseSubTerms(term, clause);
    }
    else if (term.GetStatus() == RewriteTermStatus::rterm_method_dot)
    {
        clause << "(";
        this->TraverseTerm(term.GetSubterm(), clause);
        clause << ")." << term.GetContent() << "(";
        this->TraverseSubTerms(term, clause);
    }
    else if (term.GetStatus() == RewriteTermStatus::rterm_member_arrow)
    {
        clause << "(";
        this->TraverseTerm(term.GetSubterm(), clause);
        clause << ")->" << term.GetContent();
    }
    else if (term.GetStatus() == RewriteTermStatus::rterm_member_dot)
    {
        clause << "(";
        this->TraverseTerm(term.GetSubterm(), clause);
        clause << ")." << term.GetContent();
    }
    else if (term.GetStatus() == RewriteTermStatus::rterm_string_literal
             || term.GetStatus() == RewriteTermStatus::rterm_variable)
    {
        clause << term.GetContent();
    }
    else if (term.GetStatus() == RewriteTermStatus::rterm_int_literal)
    {
        clause << term.GetInt();
    }
}

void RewriteClassGenerator::TraverseSubTerms(RewriteTerm& term, ostringstream& clause)
{
    auto st = term.GetSubterms();
    for (auto t = st.begin(); t != st.end(); ++t)
    {
        this->TraverseTerm(*t, clause);
        if (next(t) != st.end())
        {
            clause << ", ";
        }
    }
    clause << ")";
}

// const void RewriteClassGenerator::GenerateRewritePatternCondition(OutmostPattern&
// pattern, ostringstream& condition,
//                                                                   ostringstream&
//                                                                   variables, string
//                                                                   carry, int argc)
// {
//     string indent = "\t\t\t\t\t";
//     string patternIf = "";

//     switch (pattern.GetStatus())
//     {
//         case outpattern_wildcardOperator:
//             // condition << " true && ";
//             break;
//         case outpattern_variable_assignment:
//             if (carry == "")
//             {
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = "
//                           << "l_arg" << to_string(argc) << ";\n";
//             }
//             else
//             {
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = " << carry
//                 <<
//                 ";\n";
//             }
//             GenerateRewritePatternCondition(pattern.GetSubPattern(), condition,
//             variables, "", argc); break;
//         case outpattern_subtermOperator:
//         {
//             int argc = 0;
//             for (auto p = pattern.GetSubPatterns().begin(); p !=
//             pattern.GetSubPatterns().end(); ++p)
//             {
//                 GenerateRewritePatternCondition(*p, condition, variables, "",
//                 argc); argc++;
//             }
//         }
//         break;
//         default:
//             cout << "wtf you doin'?" << endl;
//     }

//     // Wenn outermost pattern variable assignment:
//     // GenerateUnparsePatternCondition(kind)
//     // Wenn wildcardOperator: true
//     // ansonsten, wenn outermost Pattern subtermOperator:
//     // Für alle Kinder des outermost Patterns:
//     // GenerateUnparsePatternCondition(aktuelles kind)
// }

// const void RewriteClassGenerator::GenerateRewritePatternCondition(Pattern& pattern,
// ostringstream& condition, ostringstream& variables,
//                                                                   string carry, int
//                                                                   argc)
// {
//     switch (pattern.GetStatus())
//     {
//         case pattern_wildcard:
//             // condition << " true && ";
//             break;
//         case pattern_variable_name:
//             // condition << " true && ";
//             if (carry == "")
//             {
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = "
//                           << "l_arg" << to_string(argc) << ";\n";
//             }
//             else
//             {
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = " << carry
//                 <<
//                 "->" << opMemberPrefix << argc << ";\n";
//             }
//             break;
//         case pattern_int_literal:
//             condition << " member == " << pattern.GetIntLiteral() << " && ";
//             break;
//         case pattern_string_literal:
//             condition << " member == " << pattern.GetID() << " && ";
//             break;
//         case pattern_variable_assignment:
//             if (carry == "")
//             {
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = "
//                           << "l_arg" << to_string(argc) << ";\n";
//             }
//             else
//             {
//                 // carry = "((" + pattern.GetID() + "*)(" + carry + "->" +
//                 opMemberPrefix +
//                 // to_string(argc) + "))";
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = " << carry
//                 <<
//                 "->" << opMemberPrefix << argc << ";\n";
//             }
//             GenerateRewritePatternCondition(pattern.GetSubPattern(), condition,
//             variables, carry, argc); break;
//         case pattern_subtermOperator:
//         {
//             if (carry == "")
//             {
//                 condition << "l_arg" << argc << "->opType() == op_" <<
//                 pattern.GetID()
//                 << " && "; carry += "(std::static_pointer_cast<" + pattern.GetID()
//                 +
//                 "_impl>(l_arg" + to_string(argc) + "))";
//             }
//             else
//             {
//                 condition << carry << "->" << opMemberPrefix << argc << "->opType()
//                 == op_" << pattern.GetID() << " && "; carry =
//                 "(std::static_pointer_cast<"
//                 + pattern.GetID() + "_impl>(" + carry + "->" + opMemberPrefix +
//                 to_string(argc)
//                 +
//                         "))";
//             }
//             int argc = 0;
//             for (auto p = pattern.GetSubPatterns().begin(); p !=
//             pattern.GetSubPatterns().end(); ++p)
//             {
//                 GenerateRewritePatternCondition(*p, condition, variables, carry,
//                 argc); argc++;
//             }
//         }
//         break;
//         default:
//             cout << "wtf you doin'?" << endl;
//     }

//     // Betrachte Fälle:
//     // p_wildcard: true
//     // p_variable_name: true, füge Variablenzusweisung hinzu
//     // p_variable_assignment: doGenerateUnparseCondition() auf subPattern, füge
//     Variablenzuweisung hinzu
//     // p_string_literal: arg_i == literal
//     // p_int_literal: arg_i == literal
//     // p_subtermOperator: Für alle Kinder:
//     // GenerateUnparsePatternCondition(aktuelles kind)
// }

/****************
 * Redirections *
 ****************/

const std::string RewriteClassGenerator::GenerateRedirections()
{
    string red;
    for (auto c : this->codeRedirections)
    {
        red += c.GetCode();
    }
    return red;
}

}  // namespace generator
