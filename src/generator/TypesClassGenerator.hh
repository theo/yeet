/****************************************************************************************
  Term processor yeet. Declarations for code generation of type definitions

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file TypesClassGenerator.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header file for code generation of type definitions
 * @version 0.1
 * @date 2024-04-26
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "FriendlyRewriteRule.hh"
#include "FriendlyUnparseRule.hh"
#include "Generator.hh"
#include "tree/CodeRedirection.hh"
#include "tree/Operator.hh"
#include "tree/Phylum.hh"
#include "tree/RView.hh"
#include "tree/RewriteRule.hh"
#include "tree/UView.hh"
#include "tree/UnparseRule.hh"

namespace generator
{
/**
 * Class to generate a class file from the parsed information about the AST, structures.
 */
class TypesClassGenerator : public Generator
{
private:
    const std::string fileSuffix = "k.cc";
    /** Overloaded method from Generator */
    std::string Generate();
    const std::string GenerateOperatorImpl();
    const std::string GenerateListPhylumImpls();
    const std::string GenerateCreators();
    const std::string GenerateOperatorImpls();
    const std::string GenerateCase(tree::Phylum);
    const std::string GenerateOperatorChildren();
    const std::string GenerateCreateCases();
    const std::string GenerateCopyAttrCases();
    const std::string GenerateCopyChildrenList();

public:
    TypesClassGenerator();
    ~TypesClassGenerator();
    /** Create an instance of the generator class.
     * \param[in] filePrefix Name of the input file.
     * \param[in] logger Logger to use for status messages.
     * \param[in] drv The driver used for parsing and that stores the parsed information.
     */
    TypesClassGenerator(const std::string& filePrefix, parsing::Declarations& decl);
    /** Generates the type class output file */
};
}  // namespace generator
