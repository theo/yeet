/****************************************************************************************
  Term processor yeet. Definitions for code generation of unparsing declarations

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file UnparseHeaderGenerator.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Implementation of code generation of unparsing declarations
 * @version 0.1
 * @date 2024-04-26
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <string>

#include "runtime.hh"
#include "Template.hh"
#include "tree/RedirectionSymbol.hh"
#include "UnparseHeaderGenerator.hh"

using namespace std;
using namespace tree;

namespace generator
{
/* Construction */

UnparseHeaderGenerator::UnparseHeaderGenerator() {}
UnparseHeaderGenerator::~UnparseHeaderGenerator() {}
UnparseHeaderGenerator::UnparseHeaderGenerator(
    const std::string& filePrefix, parsing::Declarations& decl
)
    : Generator(filePrefix, decl)
{
    if (runtime::args.c_headers_given)
    {
        fileSuffix = "unpk.h";
    }
    else
    {
        fileSuffix = "unpk.hh";
    }
    this->filename = filePrefix + fileSuffix;
}

/* Generation */

string UnparseHeaderGenerator::Generate()
{
    vector<string> filenames;
    templates::Data data;
    data.emplace("Version", PACKAGE_STRING);
    data.emplace("UViewEnum", GenerateUViewEnum());
    data.emplace("RedirectedCode", this->GetRedirectionsFor("KC_UNPARSE_HEADER", filenames));
    data.emplace("Filenames", this->GetFileNamesFromList(filenames));
    data.emplace("Namespace", this->namespaceName);
    data.emplace("FilePrefix", this->filePrefix);
    data.emplace("HeaderSuffix", runtime::args.c_headers_given ? "h" : "hh");
    return templates::UnparseMain_hht.render(data);
}

const std::string UnparseHeaderGenerator::GenerateUViewEnum()
{
    templates::Data data;
    if (this->uViews.size() > 0)
    {
        data.emplace("UViews", ",\n");
    }
    for (auto v = this->uViews.begin(); v != this->uViews.end(); ++v)
    {
        data.append_to("UViews", "\t" + v->GetName());
        if (next(v) == this->uViews.end())
        {
            data.append_to("UViews", "\n");
        }
        else
        {
            data.append_to("UViews", ",\n");
        }
    }
    return templates::UViewEnum_hht.render(data);
}

}  // namespace generator
