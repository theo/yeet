/****************************************************************************************
  Term processor yeet. Definitions for code generation of type definitions

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file TypesClassGenerator.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Implementation of code generation for type definitions
 * @version 0.1
 * @date 2024-04-26
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>

#include "FriendlyTransformer.hh"
#include "runtime.hh"
#include "Template.hh"
#include "TypesClassGenerator.hh"

using namespace std;
using namespace tree;

namespace generator
{
/****************
 * Construction *
 ****************/

TypesClassGenerator::TypesClassGenerator(const std::string& filePrefix, parsing::Declarations& decl)
    : Generator(filePrefix, decl)
{
    this->filename = filePrefix + fileSuffix;
}

TypesClassGenerator::~TypesClassGenerator() {}
TypesClassGenerator::TypesClassGenerator() {}

/**************
 * Generation *
 **************/

string TypesClassGenerator::Generate()
{
    const std::string_view integer = templates::MkIntegerImpl_cct.render();
    const std::string_view real = templates::MkRealImpl_cct.render();
    const std::string_view casestring = templates::MkCasestringImpl_cct.render();
    vector<string> filenames;
    templates::Data data;
    data.emplace("Version", PACKAGE_STRING);
    data.emplace("Namespace", namespaceName);
    data.emplace("RedirectedCode", this->GetRedirectionsFor("KC_TYPES", filenames));
    data.emplace("Filenames", this->GetFileNamesFromList(filenames));
    data.emplace("HeaderSuffix", runtime::args.c_headers_given ? "h" : "hh");
    data.emplace("Constructors", this->GenerateOperatorImpl());
    data.emplace("ListPhylumImpls", this->GenerateListPhylumImpls());
    data.emplace("Creators", this->GenerateCreators());
    data.emplace("OperatorImpls", this->GenerateOperatorImpls());
    data.emplace("MkCasestringImpl", casestring);
    data.emplace("MkIntegerImpl", integer);
    data.emplace("MkRealImpl", real);
    data.emplace("FilePrefix", this->filePrefix);
    data.emplace("CreateChildList", this->GenerateOperatorChildren());
    data.emplace("CreateSwitchCases", this->GenerateCreateCases());
    data.emplace("CopyAttributeCases", this->GenerateCopyAttrCases());
    data.emplace("NumMaxChildren", to_string(this->statistics.GetMaxOperators()));
    data.emplace("CopyChildList", this->GenerateCopyChildrenList());

    return templates::TypesMain_cct.render(data);
}

/* List Phylum methods */

const std::string TypesClassGenerator::GenerateListPhylumImpls()
{
    string res;
    for (auto phy = phylumDeclarations.begin(); phy != phylumDeclarations.end(); ++phy)
    {
        // std::cout << phy->GetName() << ": " << phy->GetElementName() << ", " <<
        // phy->IsList() << std::endl;
        if (phy->IsList())
        {
            templates::Data data;
            data.emplace("Name", phy->GetName());
            data.emplace("Element", phy->GetElementName());
            res += templates::ListPhylumImpl_cct.render(data);
        }
    }
    return res;
}

const std::string TypesClassGenerator::GenerateOperatorImpls()
{
    string res;
    // cout << "here" << endl;
    for (auto phy = phylumDeclarations.begin(); phy != phylumDeclarations.end(); ++phy)
    {
        auto operators = phy->GetOperators();
        for (auto op = operators.begin(); op != operators.end(); ++op)
        {
            templates::Data data;
            data.emplace("Name", op->GetName());
            auto children = op->GetChildren();
            int argc = 0;
            string casebody;
            for (auto child = children.begin(); child != children.end(); ++child)
            {
                casebody += "case " + to_string(argc) + ": return this->" + opMemberPrefix
                    + to_string(argc) + ";\n";
                argc++;
            }
            data.emplace("Cases", casebody);
            res += templates::OperatorImpl_cct.render(data);
        }
    }
    return res;
}

/*************
 * Operators *
 *************/

const std::string TypesClassGenerator::GenerateOperatorImpl()
{
    std::string templ;
    for (auto phy = this->phylumDeclarations.begin(); phy != this->phylumDeclarations.end(); ++phy)
    {
        auto operators = phy->GetOperators();
        for (auto op = operators.begin(); op != operators.end(); ++op)
        {
            templates::Data data;
            data.emplace("Name", op->GetName());
            if (op->GetChildren().size() == 0)
            {
                templ += templates::ConstructorNoChild_cct.render(data);
            }
            else
            {
                auto children = op->GetChildren();
                string argTemplStr;
                string initTemplStr;
                int argc = 0;
                for (auto child = children.begin(); child != children.end(); ++child)
                {
                    string childname = opMemberPrefix + to_string(argc);
                    string declaration = "const " + *child + "& " + childname;
                    argTemplStr += declaration;
                    initTemplStr += childname + "(" + childname + ")";
                    if (next(child) != children.end())
                    {
                        argTemplStr += ", ";
                        initTemplStr += ", ";
                    }
                    argc++;
                }
                data.emplace("ConstructorArgs", argTemplStr);
                data.emplace("ChildInitialization", initTemplStr);
                templ += templates::Constructor_cct.render(data);
            }
            templ += templates::Destructor_cct.render(data);
        }
    }
    return templ;
}

const std::string TypesClassGenerator::GenerateOperatorChildren()
{
    string info = "";
    for (int i = 0; i < this->statistics.GetMaxOperators(); i++)
    {
        info += ", const AbstractPhylum& __phy" + to_string(i);
    }
    return info;
}

const std::string TypesClassGenerator::GenerateCopyChildrenList()
{
    string info = "";
    for (int i = 0; i < this->statistics.GetMaxOperators(); i++)
    {
        info += ", children[" + to_string(i) + "]";
    }
    return info;
}

const std::string TypesClassGenerator::GenerateCreateCases()
{
    string cases = "";
    auto phyla = this->phylumDeclarations;
    for (auto phylum = phyla.begin(); phylum != phyla.end(); ++phylum)
    {
        auto operators = phylum->GetOperators();
        for (auto op = operators.begin(); op != operators.end(); ++op)
        {
            string op_case = "\tcase op_" + op->GetName() + ":\n\t\treturn " + op->GetName() + "(";
            auto children = op->GetChildren();
            size_t i = 0;
            for (auto child = children.begin(); child != children.end(); ++child)
            {
                op_case += "phylum_cast<" + *child + ">(__phy" + to_string(i) + ")";
                i++;
                if (i < children.size())
                {
                    op_case += ", ";
                }
            }
            op_case += ");";
            if (!(next(phylum) == phyla.end() && next(op) == operators.end()))
            {
                op_case += "\n";
            }
            cases += op_case;
        }
    }
    return cases;
}

const std::string TypesClassGenerator::GenerateCopyAttrCases()
{
    string cases = "";
    auto phyla = this->phylumDeclarations;
    for (auto phylum = phyla.begin(); phylum != phyla.end(); ++phylum)
    {
        auto attributes = phylum->GetCode().GetAttributes();
        if (attributes.size() > 0)
        {
            string phy_case = "\n\tcase phy_" + phylum->GetName() + ":";
            if (attributes.size() == 1)
            {
                phy_case += "\n\t{\n\t\tphylum_cast<" + phylum->GetName() + ">(dest)->"
                    + attributes.at(0).GetName() + " = phylum_cast<c_" + phylum->GetName()
                    + ">(origin)->" + attributes.at(0).GetName() + ";\n\t\tbreak;\n\t}";
            }
            else
            {
                phy_case += "\n\t{\n\t\tc_" + phylum->GetName()
                    + " origin_concrete = phylum_cast<c_" + phylum->GetName() + ">(origin);";
                phy_case += "\n\t\t" + phylum->GetName() + " dest_concrete = phylum_cast<"
                    + phylum->GetName() + ">(dest);";
                for (auto attr = attributes.begin(); attr != attributes.end(); ++attr)
                {
                    phy_case += "\n\t\tdest_concrete->" + attr->GetName() + " = origin_concrete->"
                        + attr->GetName() + ";";
                }
                phy_case += "\n\t\tbreak;\n\t}";
            }
            cases += phy_case;
        }
    }
    return cases;
}

/**************
 * Generators *
 **************/

const std::string TypesClassGenerator::GenerateCreators()
{
    string creators = "";
    auto phyla = this->phylumDeclarations;
    for (auto phylum = phyla.begin(); phylum != phyla.end(); ++phylum)
    {
        auto operators = phylum->GetOperators();
        for (auto op = operators.begin(); op != operators.end(); ++op)
        {
            templates::Data data;
            data.emplace("Name", op->GetName());
            data.emplace("Type", op->GetName());
            auto children = op->GetChildren();
            string cargs = "";
            string iargs = "";
            int argc = 1;
            for (auto child = children.begin(); child != children.end(); ++child)
            {
                cargs += "const " + *child + "& a" + to_string(argc);
                iargs += "a" + to_string(argc);
                if (next(child) != children.end())
                {
                    cargs += ", ";
                    iargs += ", ";
                }
                argc++;
            }
            data.emplace("Arguments", cargs);
            data.emplace("Initialization", iargs);
            creators += templates::Creator_cct.render(data);
        }
    }
    return creators;
}

}  // namespace generator
