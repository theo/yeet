/****************************************************************************************
  Term processor yeet. Definitions for file generation friendly rewrite rules

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file FriendlyRewriteRule.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definitions for friendly rewrite rules
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "FriendlyRewriteRule.hh"

#include "../tree/Operator.hh"
#include "../tree/OutmostPattern.hh"
#include "../tree/RView.hh"

using namespace tree;

namespace generator
{
OutmostPattern& FriendlyRewriteRule::GetPattern() { return this->pattern; }

FriendlyRewriteRule::FriendlyRewriteRule(
    tree::RView view, tree::OutmostPattern pattern, FriendlyRewriteClause clause, int lineNr,
    std::string fileName
)
{
    this->view = view;
    this->pattern = pattern;
    this->clause = clause;
    this->lineNr = lineNr;
    this->fileName = fileName;
}

FriendlyRewriteClause& FriendlyRewriteRule::GetFriendlyRewriteClause() { return this->clause; }

bool FriendlyRewriteRule::ForView(RView view) { return (this->view.GetName() == view.GetName()); }

bool FriendlyRewriteRule::ForView(const std::string& name)
{
    return (this->view.GetName() == name);
}

bool FriendlyRewriteRule::MatchesOperator(Operator op)
{
    if (this->pattern.GetStatus() == tree::outpattern_wildcardOperator
        || this->pattern.GetStatus() == tree::outpattern_subtermOperator)
    {
        return op.GetName() == this->pattern.GetID();
    }
    else if (this->pattern.GetStatus() == tree::outpattern_variable_assignment)
    {
        return op.GetName() == this->pattern.GetSubPattern().GetID();
    }
    return false;
}

int FriendlyRewriteRule::GetLineNr() { return this->lineNr; }
std::string FriendlyRewriteRule::GetFileName() { return this->fileName; }
void FriendlyRewriteRule::SetLineNr(int lineNr) { this->lineNr = lineNr; }
void FriendlyRewriteRule::SetFileName(const std::string& fileName) { this->fileName = fileName; }
}  // namespace generator
