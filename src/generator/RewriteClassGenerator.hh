/****************************************************************************************
  Term processor yeet. Declarations for code generation of rewriting definitions

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file RewriteClassGenerator.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Declarations for code generation of rewriting definitions
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "tree/CodeRedirection.hh"
#include "tree/RView.hh"
#include "tree/RewriteRule.hh"
#include "FriendlyRewriteRule.hh"
#include "Generator.hh"
#include "PatternMatcher.hh"

namespace generator
{
/**
 * Class to generate a class file from the parsed information about the AST, rewriting.
 */
class RewriteClassGenerator : public Generator
{
private:
    const std::string fileSuffix = "rk.cc";
    /** Overloaded method from Generator */
    std::string Generate();
    const std::string GenerateHeaderName();
    const std::string GenerateRewriteImpl();
    const std::string GenerateRedirections();

    /**
     * Same as GenerateRewritePatternCondition(tree::Pattern&, std::ostringstream&
     * condition, std::ostringstream& variables,std::string carry, int argc), but for
     * OutmostPattern. This is a different method, because the cases for the patterns type
     * are slightly different in OutmostPattern. \param[in] pattern The pattern to convert
     * to c++ code. \param[out] condition This will be filled with an if-statement that
     * checks for the pattern on the ast. \param[out] variables This will be filled with
     * expressions to retrieve the patterns variables from the ast. \param[in] carry
     * Carries prefixes for the condition through the recursion. \param[in] argc Carries a
     * counter that defines which member of the current node (operator) in the AST we
     * inspect.
     */
    void GenerateRewritePatternCondition(
        tree::OutmostPattern& pattern, std::ostringstream& condition, std::ostringstream& variables,
        std::string carry, int argc
    );
    /**
     * Recursive method over a pattern to generate code for detecting a pattern and
     * retrieving the defined variables in the pattern. This is a different method than
     * the one for unparsing, since the variable names are different while rewriting
     * (because they now contain the rewritten AST). \param[in] pattern The pattern to
     * convert to c++ code. \param[out] condition This will be filled with an if-statement
     * that checks for the pattern on the ast. \param[out] variables This will be filled
     * with expressions to retrieve the patterns variables from the ast. \param[in] carry
     * Carries prefixes for the condition through the recursion. \param[in] argc Carries a
     * counter that defines which member of the current node (operator) in the AST we
     * inspect.
     */
    void GenerateRewritePatternCondition(
        tree::Pattern& pattern, std::ostringstream& condition, std::ostringstream& variables,
        std::string carry, int argc
    );
    /** Generate the body of a rewrite rule.
     * \param[in] pattern The pattern of the current rule.
     * \param[out] clause This will contain the c++ code of the body.
     */
    void GenerateRewriteBody(
        FriendlyRewriteRule& pattern, std::ostringstream& clause,
        std::map<std::string, std::string> variables, const std::string& indent
    );
    /** Recursive helper method for GenerateRewriteBody to traverse the term structure in
     * the body. \param[in] term The terms root. \param[out] clause This will contain the
     * generated c++ code.
     */
    void TraverseTerm(tree::RewriteTerm& term, std::ostringstream& clause);
    void TraverseSubTerms(tree::RewriteTerm& term, std::ostringstream& clause);

public:
    RewriteClassGenerator();
    ~RewriteClassGenerator();
    /** Create an instance of the generator class.
     * \param[in] filePrefix Name of the input file.
     * \param[in] logger Logger to use for status messages.
     * \param[in] drv The driver used for parsing and that stores the parsed information.
     */
    RewriteClassGenerator(const std::string& filePrefix, parsing::Declarations& decl);
    /** Generates the type class output file */
};
}  // namespace generator
