/****************************************************************************************
  Term processor yeet. Definitions for code generation of unparsing definitions

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file UnparseClassGenerator.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Implementation of code generation for unparsing definitions
 * @version 0.1
 * @date 2024-04-26
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <regex>

#include "PatternMatcher.hh"
#include "runtime.hh"
#include "Template.hh"
#include "UnparseClassGenerator.hh"

using namespace std;
using namespace tree;

namespace generator
{
/* Construction */

UnparseClassGenerator::UnparseClassGenerator() {}
UnparseClassGenerator::~UnparseClassGenerator() {}
UnparseClassGenerator::UnparseClassGenerator(
    const std::string& filePrefix, parsing::Declarations& decl
)
    : Generator(filePrefix, decl)
{
    this->filename = filePrefix + fileSuffix;
}

/* Generation */

string UnparseClassGenerator::Generate()
{
    vector<string> filenames;
    /* Fill the main template */
    templates::Data data;
    data.emplace("Version", PACKAGE_STRING);
    data.emplace("Namespace", namespaceName);
    data.emplace("RedirectedCode", this->GetRedirectionsFor("KC_UNPARSE", filenames));
    data.emplace("Filenames", this->GetFileNamesFromList(filenames));
    data.emplace("UnparseFunction", this->GenerateUnparseImpl());
    data.emplace("FilePrefix", this->filePrefix);
    data.emplace("HeaderSuffix", runtime::args.c_headers_given ? "h" : "hh");
    return templates::UnparseMain_cct.render(data);
}

std::string UnparseClassGenerator::GenerateUnparseImpl()
{
    std::string templ;
    /* Iterate over all (phyla and) operators*/
    for (auto phy = this->phylumDeclarations.begin(); phy != this->phylumDeclarations.end(); ++phy)
    {
        auto operators = phy->GetOperators();
        for (auto op = operators.begin(); op != operators.end(); ++op)
        {
            templates::Data data;
            data.emplace("Namespace", namespaceName);
            data.emplace("Name", op->GetName());

            /* For each view generate a case in the switch for unparsing */
            string caseStr;
            bool needSwitch = false;
            for (auto view = this->uViews.begin(); view != this->uViews.end(); ++view)
            {
                std::vector<FriendlyUnparseRule> matchingRules;
                /* Find the patterns that match the current operator and view */
                for (auto rule = this->unparseRules.begin(); rule != this->unparseRules.end();
                     ++rule)
                {
                    if (rule->ForView(*view) && rule->MatchesOperator(*op))
                    {
                        matchingRules.push_back(*rule);
                    }
                }

                /* If no patterns match, use default unparsing. */
                if (matchingRules.size() != 0)
                {
                    if (!needSwitch)
                    {
                        caseStr = "\tswitch (" + this->namespaceName + "_current_view)\n\t{\n";
                        needSwitch = true;
                    }

                    caseStr += "\tcase UView::" + view->GetName() + ":\n\t{\n";
                    for (auto match = matchingRules.begin(); match != matchingRules.end(); match++)
                    {
                        ostringstream condition;
                        std::map<std::string, std::string> variables;
                        ostringstream clause;
                        /* generate condition */
                        PatternMatcher::GetPatternInformation(
                            match->GetPattern(), opMemberPrefix, opMemberPrefix, 0, condition,
                            variables
                        );
                        // GenerateUnparsePatternCondition(match->GetPattern(), condition,
                        // variables, "", 0);
                        string conditionStr = condition.str();
                        if (conditionStr == "")
                        {
                            conditionStr = "true";
                        }
                        // else
                        //{
                        //    conditionStr = conditionStr.substr(0, conditionStr.length()
                        //    - 3);
                        //}

                        /* generate body */
                        GenerateUnparseBody(*match, clause, variables, "\t\t\t");
                        clause << lineReset << "\n\t\t\treturn;\n";

                        /* Add pattern as comment before the if statement */
                        ostringstream mm;
                        mm << match->GetPattern();
                        caseStr += "\t\t/*" + mm.str() + "*/\n";
                        caseStr += "#line " + to_string(match->GetLineNr()) + " \""
                            + match->GetFileName() + "\"\n";

                        /* If this is the first pattern in the list, start with nothing */
                        if (match == matchingRules.begin())
                        {
                            caseStr += "\t\t";
                        }
                        /* If this is not the first pattern, add an else*/
                        else
                        {
                            caseStr += "\t\telse ";
                        }
                        /* Add the if statement and body to the current case */
                        caseStr += "if ( " + conditionStr + ")\n\t\t{ // first clause\n"
                            + clause.str() + "\t\t}\n";
                    }
                    /* Add default unparsing as last option. TODO: move into loop. */
                    caseStr += "\t\t//If no patterns match, break out of the switch and "
                               "go to default\n\t\tbreak;\n\t}\n"
                        + lineReset + "\n";
                }
            }

            if (needSwitch)
            {
                caseStr += "\tdefault:;\n\t}\n";
            }

            data.emplace("UnparsingCases", caseStr);

            string defUnpStr;
            std::vector<FriendlyUnparseRule> matchingRules;
            /* Find the patterns that match the current operator and view */
            for (auto rule = this->unparseRules.begin(); rule != this->unparseRules.end(); ++rule)
            {
                if (rule->ForView("") && rule->MatchesOperator(*op))
                {
                    matchingRules.push_back(*rule);
                }
            }

            /* If no patterns match, use default unparsing. */
            if (matchingRules.size() != 0)
            {
                for (auto match = matchingRules.begin(); match != matchingRules.end(); match++)
                {
                    ostringstream condition;
                    std::map<std::string, std::string> variables;
                    ostringstream clause;
                    /* generate condition */
                    PatternMatcher::GetPatternInformation(
                        match->GetPattern(), opMemberPrefix, opMemberPrefix, 0, condition, variables
                    );
                    // GenerateUnparsePatternCondition(match->GetPattern(), condition,
                    // variables, "", 0);
                    string conditionStr = condition.str();
                    if (conditionStr == "")
                    {
                        conditionStr = "true";
                    }
                    // else
                    //{
                    //    conditionStr = conditionStr.substr(0, conditionStr.length()
                    //    - 3);
                    //}

                    /* generate body */
                    GenerateUnparseBody(*match, clause, variables, "\t\t");
                    /** Since default unparsing is handled differently to kc++,
                     * we need to return after doing anything
                     */
                    clause << lineReset << "\n\t\treturn;\n";

                    /* Add pattern as comment before the if statement */
                    ostringstream mm;
                    mm << match->GetPattern();
                    defUnpStr += "\t/*" + mm.str() + "*/\n";
                    defUnpStr += "#line " + to_string(match->GetLineNr()) + " \""
                        + match->GetFileName() + "\"\n";

                    /* If this is the first pattern in the list, start with nothing */
                    if (match == matchingRules.begin())
                    {
                        defUnpStr += "\t";
                    }
                    /* If this is not the first pattern, add an else*/
                    else
                    {
                        defUnpStr += "\telse ";
                    }
                    /* Add the if statement and body to the current case */
                    defUnpStr += "if ( " + conditionStr + ")\n\t{ // first clause\n" + clause.str()
                        + "\t}\n";
                }
                if (op->GetChildren().size() != 0)
                {
                    /* Add default unparsing as last option. TODO: move into loop. */
                    defUnpStr += "\telse\n\t{\n\t\tdefaultUnparse(" + this->namespaceName
                        + "_printer, " + this->namespaceName + "_current_view);\n\t}\n";
                }
            }
            else if (op->GetChildren().size() != 0)
            {
                defUnpStr = "\tdefaultUnparse(" + this->namespaceName + "_printer, "
                    + this->namespaceName + "_current_view);\n";
            }

            data.emplace("DefaultUnparsing", defUnpStr);

            templ += templates::UnparseFunction_cct.render(data);
        }
    }

    return templ;
}

void UnparseClassGenerator::GenerateUnparseBody(
    FriendlyUnparseRule& pattern, ostringstream& clause,
    std::map<std::string, std::string> variables, const std::string& indent
)
{
    clause << indent << "// Body\n";
    for (auto pair : variables)
    {
        clause << "#line " << pattern.GetLineNr() << " \"" << pattern.GetFileName() << "\"\n"
               << indent << "auto " << pair.first << " = " << pair.second << ";\n";
    }
    auto items = pattern.GetFriendlyUnparseClause().GetUnparseItems();
    for (auto item : items)
    {
        switch (item.GetStatus())
        {
        case tree::UnparseItemStatus::unparse_variable:
            clause << "#line " << pattern.GetLineNr() << " \"" << pattern.GetFileName() << "\"\n"
                   << indent << item.GetContent() << "->unparse(" << namespaceName << "_printer, "
                   << namespaceName << "_current_view);" << endl;
            break;
        case tree::UnparseItemStatus::unparse_string_literal:
            clause << indent << namespaceName << "_printer(\"" << item.GetContent()
                   << "\", " + namespaceName + "_current_view);" << endl;
            break;
        case tree::UnparseItemStatus::unparse_cbody:
            clause << "#line " << pattern.GetLineNr() << " \"" << pattern.GetFileName() << "\"\n"
                   << indent << item.GetContent() << endl;
            break;
        default:
            break;
        }
    }
    // clause << lineReset << endl; /* appended in GenerateUnparseImpl() */
    clause << endl;
}

}  // namespace generator

// const std::string UnparseClassGenerator::GetProvidedCondition(tree::OutmostPattern&
// pattern)
// {
//     std::string res = pattern.GetProvided();
//     std::string equalsStr = "";
//     std::ostringstream equals;
//     std::map<std::string, std::string> variables;
//     GetVariables(pattern, "", 0, variables, equals);
//     if (res != "")
//     {
//         std::cout << pattern << "\n";
//         for (auto pair : variables)
//         {
//             std::cout << pair.first << "\n";
//             const std::string regex = "(^|\\W)(" + pair.first + ")($|\\W)";
//             res = std::regex_replace(res, std::regex(regex), "$1" + pair.second +
//             "$3");
//         }
//         std::cout << res << std::endl;
//         if (res != "")
//         {
//             res = " && " + res;
//         }
//     }
//     equalsStr = equals.str();
//     if (equalsStr != "")
//     {
//         equalsStr = equalsStr.substr(0, equalsStr.length() - 3);
//         equalsStr = " && " + equalsStr;
//     }
//     return equalsStr + res;
// }

// const void UnparseClassGenerator::GetVariables(OutmostPattern& pattern, std::string
// carry, int argc,
//                                                std::map<std::string, std::string>&
//                                                variables, std::ostringstream& equals)
// {
//     string indent = "\t\t\t\t\t";
//     string patternIf = "";

//     switch (pattern.GetStatus())
//     {
//         case outpattern_variable_assignment:
//             if (carry == "")
//             {
//                 if (variables.find(pattern.GetID()) == variables.end())
//                 {
//                     variables.emplace(make_pair<string, string>(pattern.GetID(),
//                     "this->" + opMemberPrefix + to_string(argc)));
//                 }
//                 else
//                 {
//                     // add check for variable equality
//                     equals << variables.at(pattern.GetID()) << "->equals(this->" <<
//                     opMemberPrefix << argc << ") && ";
//                 }
//             }
//             GetVariables(pattern.GetSubPattern(), "", argc, variables, equals);
//             break;
//         case outpattern_subtermOperator:
//         {
//             int argc = 0;
//             for (auto p = pattern.GetSubPatterns().begin(); p !=
//             pattern.GetSubPatterns().end(); ++p)
//             {
//                 GetVariables(*p, "", argc, variables, equals);
//                 argc++;
//             }
//         }
//         break;
//         default:
//             break;
//     }
// }

// const void UnparseClassGenerator::GetVariables(Pattern& pattern, std::string carry, int
// argc,
//                                                std::map<std::string, std::string>&
//                                                variables, std::ostringstream& equals)
// {
//     switch (pattern.GetStatus())
//     {
//         case pattern_variable_name:
//             if (carry == "")
//             {
//                 if (variables.find(pattern.GetID()) == variables.end())
//                 {
//                     variables.emplace(make_pair<string, string>(pattern.GetID(),
//                     "this->" + opMemberPrefix + to_string(argc)));
//                 }
//                 else
//                 {
//                     // add check for variable equality
//                     equals << variables.at(pattern.GetID()) << "->equals(this->" <<
//                     opMemberPrefix << argc << ") && ";
//                 }
//             }
//             else
//             {
//                 if (variables.find(pattern.GetID()) == variables.end())
//                 {
//                     variables.emplace(make_pair<string, string>(pattern.GetID(), carry
//                     + "->" + opMemberPrefix + to_string(argc)));
//                 }
//                 else
//                 {
//                     // add check for variable equality
//                     equals << variables.at(pattern.GetID()) << "->equals(" << carry <<
//                     "->" << opMemberPrefix << argc << ") &&
//                     ";
//                 }
//             }
//             break;
//         case pattern_variable_assignment:
//             if (carry == "")
//             {
//                 if (variables.find(pattern.GetID()) == variables.end())
//                 {
//                     variables.emplace(make_pair<string, string>(pattern.GetID(),
//                     "this->" + opMemberPrefix + to_string(argc)));
//                 }
//                 else
//                 {
//                     // add check for variable equality
//                     equals << variables.at(pattern.GetID()) << "->equals(this->" <<
//                     opMemberPrefix << argc << ") && ";
//                 }
//             }
//             else
//             {
//                 // carry = "((" + pattern.GetSubPattern().GetID() + "_impl*)(" + carry
//                 + "->" + opMemberPrefix + to_string(argc)
//                 +
//                 // "))";
//                 if (variables.find(pattern.GetID()) == variables.end())
//                 {
//                     variables.emplace(make_pair<string, string>(pattern.GetID(), carry
//                     + "->" + opMemberPrefix + to_string(argc)));
//                 }
//                 else
//                 {
//                     // add check for variable equality
//                     equals << variables.at(pattern.GetID()) << "->equals(" << carry <<
//                     ") && ";
//                 }
//                 // variables.emplace(make_pair<string, string>(pattern.GetID(), "" +
//                 carry));
//                 // variables << "\t\t\t\t\tauto " << pattern.GetID() << " = " << carry
//                 << ";\n";
//             }
//             GetVariables(pattern.GetSubPattern(), carry, argc, variables, equals);
//             break;
//         case pattern_subtermOperator:
//         {
//             if (carry == "")
//             {
//                 carry += "(std::static_pointer_cast<" + pattern.GetID() +
//                 "_impl>(this->" + opMemberPrefix + to_string(argc) +
//                 "))";
//             }
//             else
//             {
//                 carry = "(std::static_pointer_cast<" + pattern.GetID() + "_impl>(" +
//                 carry + "->" + opMemberPrefix + to_string(argc) +
//                         "))";
//             }
//             int argc = 0;
//             for (auto p = pattern.GetSubPatterns().begin(); p !=
//             pattern.GetSubPatterns().end(); ++p)
//             {
//                 GetVariables(*p, carry, argc, variables, equals);
//                 argc++;
//             }
//         }
//         break;
//         default:
//             break;
//     }
// }

// const void UnparseClassGenerator::GenerateUnparsePatternCondition(OutmostPattern&
// pattern, ostringstream& condition,
//                                                                   ostringstream&
//                                                                   variables, string
//                                                                   carry, int argc)
// {
//     string indent = "\t\t\t\t\t";
//     string patternIf = "";

//     switch (pattern.GetStatus())
//     {
//         case outpattern_wildcardOperator:
//             // condition << " true && ";
//             break;
//         case outpattern_variable_assignment:
//             if (carry == "")
//             {
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = "
//                           << "this->" << opMemberPrefix << argc << ";\n";
//             }
//             else
//             {
//                 /*carry = "(std::static_pointer_cast<" +
//                 pattern.GetSubPattern().GetID() + "_impl>(" + carry + "->" +
//                 opMemberPrefix +
//                         to_string(argc) + "))";*/
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = " << carry <<
//                 ";\n";
//             }
//             GenerateUnparsePatternCondition(pattern.GetSubPattern(), condition,
//             variables, "", argc); break;
//         case outpattern_subtermOperator:
//         {
//             int argc = 0;
//             for (auto p = pattern.GetSubPatterns().begin(); p !=
//             pattern.GetSubPatterns().end(); ++p)
//             {
//                 GenerateUnparsePatternCondition(*p, condition, variables, "", argc);
//                 argc++;
//             }
//         }
//         break;
//         default:
//             cout << "wtf you doin'?" << endl;
//     }

//     // Wenn outermost pattern variable assignment:
//     // GenerateUnparsePatternCondition(kind)
//     // Wenn wildcardOperator: true
//     // ansonsten, wenn outermost Pattern subtermOperator:
//     // Für alle Kinder des outermost Patterns:
//     // GenerateUnparsePatternCondition(aktuelles kind)
// }

// const void UnparseClassGenerator::GenerateUnparsePatternCondition(Pattern& pattern,
// ostringstream& condition, ostringstream& variables,
//                                                                   string carry, int
//                                                                   argc)
// {
//     switch (pattern.GetStatus())
//     {
//         case pattern_wildcard:
//             // condition << " true && ";
//             break;
//         case pattern_variable_name:
//             // condition << " true && ";
//             if (carry == "")
//             {
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = "
//                           << "this->" << opMemberPrefix << argc << ";\n";
//             }
//             else
//             {
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = " << carry <<
//                 "->" << opMemberPrefix << argc << ";\n";
//             }
//             break;
//         case pattern_int_literal:
//             condition << " member == " << pattern.GetIntLiteral() << " && ";
//             break;
//         case pattern_string_literal:
//             condition << " member == " << pattern.GetID() << " && ";
//             break;
//         case pattern_variable_assignment:
//             if (carry == "")
//             {
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = "
//                           << "this->" << opMemberPrefix << argc << ";\n";
//             }
//             else
//             {
//                 // carry = "((" + pattern.GetSubPattern().GetID() + "_impl*)(" + carry
//                 + "->" + opMemberPrefix + to_string(argc)
//                 +
//                 // "))";
//                 variables << "\t\t\t\t\tauto " << pattern.GetID() << " = " << carry <<
//                 ";\n";
//             }
//             GenerateUnparsePatternCondition(pattern.GetSubPattern(), condition,
//             variables, carry, argc); break;
//         case pattern_subtermOperator:
//         {
//             if (carry == "")
//             {
//                 condition << " this->" << opMemberPrefix << argc << "->opType() == op_"
//                 << pattern.GetID() << " && "; carry += "(std::static_pointer_cast<" +
//                 pattern.GetID() + "_impl>(this->" + opMemberPrefix + to_string(argc) +
//                 "))";
//             }
//             else
//             {
//                 condition << carry << "->" << opMemberPrefix << argc << "->opType() ==
//                 op_" << pattern.GetID() << " && "; carry = "(std::static_pointer_cast<"
//                 + pattern.GetID() + "_impl>(" + carry + "->" + opMemberPrefix +
//                 to_string(argc) +
//                         "))";
//             }
//             int argc = 0;
//             for (auto p = pattern.GetSubPatterns().begin(); p !=
//             pattern.GetSubPatterns().end(); ++p)
//             {
//                 GenerateUnparsePatternCondition(*p, condition, variables, carry, argc);
//                 argc++;
//             }
//         }
//         break;
//         default:
//             cout << "wtf you doin'?" << endl;
//     }

//     // Betrachte Fälle:
//     // p_wildcard: true
//     // p_variable_name: true, füge Variablenzusweisung hinzu
//     // p_variable_assignment: doGenerateUnparseCondition() auf subPattern, füge
//     Variablenzuweisung hinzu
//     // p_string_literal: arg_i == literal
//     // p_int_literal: arg_i == literal
//     // p_subtermOperator: Für alle Kinder:
//     // GenerateUnparsePatternCondition(aktuelles kind)
// }
