/****************************************************************************************
  Term processor yeet. Definitions for templates for code generation

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file Template.hh
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definitions for templates for code generation
 * @version 0.1
 * @date 2024-04-26
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <algorithm>
#include <array>
#include <string>
#include <string_view>
#include <vector>

#include "generator/Data.hh"
#include "runtime.hh"

namespace generator
{
namespace templates
{

/**
 * @brief Class to hold processed information from a template string. Template strings
 * include variables that will be substituted in the code generation step. In generating a
 * template object, the compiler can read a constexpr string and split it along variables.
 * This allows the generation to just iterate through the substrings, building the final
 * string from substrings and variable substitutions. Further, we save how often each
 * variable occurs so that the length of the final string can be pre-calculated to create
 * a buffer beforehand
 *
 * @tparam num_variables amount of unique variables in the string
 * @tparam num_substrings amount of substrings surrounding variables in the string
 */
template <size_t num_variables, size_t num_substrings>
class Template
{
private:
    const std::string_view name;
    /**
     * @brief Variables are stored with their name and how often they appear in the
     * template. Combining this with the substitution strings and the size of the simple
     * substrings we can calculate the size of the rendered string beforehand and allocate
     * an appropriate buffer
     *
     */
    const std::array<std::pair<std::string_view, size_t>, num_variables> variables;
    /**
     * @brief Substrings of the template are saved as the const string part and the
     * variable name that follows. This makes rendering easier, since we just iterate this
     * array and switch between inputting the substring and the substituted string
     *
     */
    const std::array<std::pair<std::string_view, std::string_view>, num_substrings> substrings;
    /**
     * @brief Saves the size of the template string without the variables, allowing for
     * easy calculation of the size of the overall string after substitution
     *
     */
    const size_t size_of_substrings;

public:
    /**
     * @brief Normal constructor, taking the memberfields as parameters. Needs to work
     * constexpr since the templates are compile time strings and we want to take
     * advantage of that.
     *
     */
    constexpr Template(
        const std::string_view _name, const size_t _size_of_substrings,
        const std::array<std::pair<std::string_view, size_t>, num_variables>& _variables,
        const std::array<std::pair<std::string_view, std::string_view>, num_substrings>& _substrings
    )
        : name(_name), variables(_variables), substrings(_substrings),
          size_of_substrings(_size_of_substrings){};

    /**
     * @brief Rendering function without data for variables
     *
     * @return const std::string_view Just the first substring before the first variable
     */
    const std::string_view render() const { return std::get<0>(std::get<0>(substrings)); };
    /**
     * @brief Rendering function, building the string by substituting variables from
     * strings in data
     *
     * @param data Mapping of variables to strings
     * @return const std::string The fully rendered string
     */
    const std::string render(Data& data) const
    {
        size_t size_of_result = this->size_of_substrings;
        for (auto& var : variables)
        {
            if (data.find(var.first) == data.end())
            {
                runtime::log<runtime::level::warn>(
                    "Variable {} not found while rendering {}. Assuming \"\"", var.first, name
                );
                data.emplace(var.first, "");
            }
            size_of_result += var.second * data.at(var.first).size();
        }
        std::string res(size_of_result, '\0');
        auto begin = res.begin();
        for (auto& substring : substrings)
        {
            begin = fmt::format_to(begin, "{}{}", substring.first, data.at(substring.second));
        }
        return res;
    };
};

}  // namespace templates
}  // namespace generator

#include <Templates.inc>
