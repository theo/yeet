/****************************************************************************************
  Term processor yeet. Definitions for code extraction to match patterns

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file PatternMatcher.cc
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Declarations to match patterns in generated code
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "PatternMatcher.hh"

#include <regex>
#include <sstream>

using namespace std;
using namespace tree;

namespace generator
{
void PatternMatcher::GetPatternInformation(
    OutmostPattern& pattern, const std::string& opMemberPrefix, const std::string& opRewritePrefix,
    int argc, ostringstream& condition, std::map<std::string, std::string>& variables
)
{
    bool needsProvided = true;

    /* Check the outmost pattern type */
    switch (pattern.GetStatus())
    {
    case outpattern_wildcardOperator:
        /* If it is a wildcard outmostpattern, there is nothing to check for */
        break;
    case outpattern_variable_assignment:
        /* If it is a variable assignment, add the variable to the list and call
         * recursively for the subpattern the variable is assigned to. No occurence check
         * for the variable name is required, since it can not possibly appear before this
         * point.
         */
        needsProvided = false;
        if (opMemberPrefix == opRewritePrefix)
        {
            // we're unparsing, so the variable can be set to this, since "this" needs to
            // match the subPattern
            variables.emplace(make_pair(pattern.GetID(), "this"));
        }
        else
        {
            // we're rewriting, so the variable needs to equal the term with the rewritten
            // children
            auto subPattern = pattern.GetSubPattern();
            string res = "topmost_node";
            if (subPattern.GetStatus() == OutPatternStatus::outpattern_variable_assignment
                || subPattern.GetStatus() == OutPatternStatus::outpattern_undefined)
            {
                std::cout << "Chained assignment not allowed! (e.g. x=y=Op)" << std::endl;
            }
            variables.emplace(make_pair(pattern.GetID(), res));
        }
        GetPatternInformation(
            pattern.GetSubPattern(), opMemberPrefix, opRewritePrefix, argc, condition, variables
        );
        break;
    case outpattern_subtermOperator:
    {
        /* If the outmost pattern has subpatterns, iterate over the subpatterns and call
         * this function recursively on them.*/
        int argc = 0; /* The number of the current argument in the outmost pattern. */
        for (auto p = pattern.GetSubPatterns().begin(); p != pattern.GetSubPatterns().end(); ++p)
        {
            GetPatternInformation(
                *p, opMemberPrefix, opRewritePrefix, "", argc, condition, variables
            );
            argc++;
        }
        // brackets necessary, because we want to use an int.
    }
    break;
    default:
        /* Should not be reached. */
        cout << "Error: Internal Error: OutmostPattern type is undefined!" << pattern.GetStatus()
             << endl;
    }
    if (needsProvided)
    {
        GetProvidedInformation(pattern, condition, variables);
    }
}

void PatternMatcher::GetPatternInformation(
    Pattern& pattern, const std::string& opMemberPrefix, const std::string& opRewritePrefix,
    string carry, int argc, ostringstream& condition, std::map<std::string, std::string>& variables
)
{
    /* Check the pattern type */
    switch (pattern.GetStatus())
    {
    case pattern_wildcard:
        /* If it is a wildcard outmostpattern, there is nothing to check for */
        break;
    case pattern_variable_name:
        /* If it is a variable name, add the variable to the list. If there is a carry
         * over (we are beyond depth 0), use it to prefix the variables access path. There
         * is not recursive call necessary here, because we have reached a leave.
         */
        if (carry == "")
        {
            if (variables.find(pattern.GetID()) == variables.end())
            {
                if (opMemberPrefix == opRewritePrefix)
                {
                    // we're in unparsing and in the first level of the pattern, thus the
                    // variable is an argument of this
                    variables.emplace(
                        make_pair(pattern.GetID(), "this->" + opMemberPrefix + to_string(argc))
                    );
                }
                else
                {
                    // we're rewriting and in the first level of the pattern, thus the
                    // variable is an l_arg (opRewritePrefix)
                    variables.emplace(make_pair(pattern.GetID(), opRewritePrefix + to_string(argc))
                    );
                }
            }
            else
            {
                // add check for variable equality
                condition << variables.at(pattern.GetID()) << "->equals(this->" << opMemberPrefix
                          << argc << ") && ";
            }
        }
        else
        {
            if (variables.find(pattern.GetID()) == variables.end())
            {
                variables.emplace(
                    make_pair(pattern.GetID(), carry + "->" + opMemberPrefix + to_string(argc))
                );
            }
            else
            {
                // add check for variable equality
                condition << variables.at(pattern.GetID()) << "->equals(" << carry << "->"
                          << opMemberPrefix << argc << ") && ";
            }
        }
        break;
    case pattern_int_literal:
        /* If it is an int literal, add check for equality with the value */
        if (carry == "")
        {
            if (opMemberPrefix == opRewritePrefix)
            {
                // we're in unparsing and in the first level of the pattern, thus the int
                // literal is an argument of this
                condition << "std::static_pointer_cast<integer_impl>(this->" << opMemberPrefix
                          << argc << ")->value == " << pattern.GetIntLiteral() << " && ";
            }
            else
            {
                // we're rewriting and in the first level of the pattern, thus the int
                // literal is some l_arg or however opRewritePrefix is set
                condition << "std::static_pointer_cast<integer_impl>(" << opRewritePrefix << argc
                          << ")->value == " << pattern.GetIntLiteral() << " && ";
            }
        }
        else
        {
            condition << "std::static_pointer_cast<integer_impl>(" << carry << "->"
                      << opMemberPrefix << argc << ")->value == " << pattern.GetIntLiteral()
                      << " && ";
        }
        break;
    case pattern_string_literal:
        /* If it is a string literal, add check for equality with the value */
        if (carry == "")
        {
            if (opMemberPrefix == opRewritePrefix)
            {
                // we're in unparsing and in the first level of the pattern, thus the
                // string literal is an argument of this
                condition << "std::static_pointer_cast<casestring_impl>(this->" << opMemberPrefix
                          << argc << ")->value == \"" << pattern.GetID() << "\" && ";
            }
            else
            {
                // we're rewriting and in the first level of the pattern, thus the string
                // literal is some l_arg or however opRewritePrefix is set
                condition << "std::static_pointer_cast<casestring_impl>(" << opRewritePrefix << argc
                          << ")->value == \"" << pattern.GetID() << "\" && ";
            }
        }
        else
        {
            condition << "std::static_pointer_cast<casestring_impl>(" << carry << "->"
                      << opMemberPrefix << argc << ")->value == \"" << pattern.GetID() << "\" && ";
        }
        break;
    case pattern_variable_assignment:
        /* If it is a variable assignment, add the variable to the list and call
         * recursively for the subpattern the variable is assigned to. If there is a carry
         * over (we are beyond depth 0), use it to prefix the variables access path.
         */
        if (carry == "")
        {
            if (variables.find(pattern.GetID()) == variables.end())
            {
                if (opMemberPrefix == opRewritePrefix)
                {
                    // we're in unparsing and in the first level of the pattern, thus the
                    // variable is an argument of this
                    variables.emplace(
                        make_pair(pattern.GetID(), "this->" + opMemberPrefix + to_string(argc))
                    );
                }
                else
                {
                    // we're rewriting and in the first level of the pattern, thus the
                    // variable is an l_arg (opRewritePrefix)
                    variables.emplace(make_pair(pattern.GetID(), opRewritePrefix + to_string(argc))
                    );
                }
            }
            else
            {
                // add check for variable equality
                condition << variables.at(pattern.GetID()) << "->equals(this->" << opMemberPrefix
                          << argc << ") && ";
            }
        }
        else
        {
            // carry = "((" + pattern.GetSubPattern().GetID() + "_impl*)(" + carry + "->"
            // + opMemberPrefix + to_string(argc) +
            // "))";
            if (variables.find(pattern.GetID()) == variables.end())
            {
                variables.emplace(
                    make_pair(pattern.GetID(), carry + "->" + opMemberPrefix + to_string(argc))
                );
            }
            else
            {
                // add check for variable equality
                condition << variables.at(pattern.GetID()) << "->equals(" << carry << ") && ";
            }
            // variables.emplace(make_pair(pattern.GetID(), "" + carry));
            // variables << "\t\t\t\t\tauto " << pattern.GetID() << " = " << carry <<
            // ";\n";
        }
        GetPatternInformation(
            pattern.GetSubPattern(), opMemberPrefix, opRewritePrefix, carry, argc, condition,
            variables
        );
        break;
    case pattern_subtermOperator:
    {
        /* If the outmost pattern has subpatterns, add a condition checking for the
         * current Terms type and then iterate over the subpatterns and call this function
         * recursively on them. Also update the carry over path accordingly.
         */
        if (carry == "")
        {
            if (opMemberPrefix == opRewritePrefix)
            {
                // we're unparsing and in the first level of the pattern, thus our
                // subtermOperator is an argument of this
                condition << "this->" << opMemberPrefix << argc << "->opType() == op_"
                          << pattern.GetID() << " && ";
                carry += "(std::static_pointer_cast<" + pattern.GetID() + "_impl>(this->"
                    + opMemberPrefix + to_string(argc) + "))";
            }
            else
            {
                // we're rewriting and in the first level of the pattern, thus our
                // subtermOperator is an l_arg (opRewritePrefix)
                condition << opRewritePrefix << argc << "->opType() == op_" << pattern.GetID()
                          << " && ";
                carry += "(std::static_pointer_cast<" + pattern.GetID() + "_impl>("
                    + opRewritePrefix + to_string(argc) + "))";
            }
        }
        else
        {
            condition << carry << "->" << opMemberPrefix << argc << "->opType() == op_"
                      << pattern.GetID() << " && ";
            carry = "(std::static_pointer_cast<" + pattern.GetID() + "_impl>(" + carry + "->"
                + opMemberPrefix + to_string(argc) + "))";
        }
        int argc = 0;
        for (auto p = pattern.GetSubPatterns().begin(); p != pattern.GetSubPatterns().end(); ++p)
        {
            GetPatternInformation(
                *p, opMemberPrefix, opRewritePrefix, carry, argc, condition, variables
            );
            argc++;
        }
        break;
    }  // brackets necessary, because we want to use an int.
    default:
        /* Should never be reached. */
        cout << "Error: Internal Error: Pattern type is undefined!" << pattern.GetStatus() << endl;
    }
}

void PatternMatcher::GetProvidedInformation(
    tree::OutmostPattern& pattern, std::ostringstream& condition,
    std::map<std::string, std::string>& variables
)
{
    std::string res = pattern.GetProvided();
    if (res != "")
    {
        // std::cout << pattern << "\n";
        for (auto pair : variables)
        {
            // std::cout << pair.first << "\n";
            const std::string regex = "(^|\\W)(" + pair.first + ")($|\\W)";
            res = std::regex_replace(res, std::regex(regex), "$1" + pair.second + "$3");
        }
        // std::cout << res << std::endl;
    }
    else
    {
        res = "true";
    }
    condition << res;
}

}  // namespace generator
