/****************************************************************************************
  Term processor yeet. General definitions for code generation

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file Generator.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definitions for code generation
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <algorithm>
#include <filesystem>
#include <string>

#include "Generator.hh"
#include "tree/Phylum.hh"
#include "tree/RView.hh"
#include "tree/RewriteRule.hh"
#include "tree/UView.hh"
#include "tree/UnparseRule.hh"
#include "FriendlyTransformer.hh"

using namespace std;
using namespace tree;

namespace generator
{
/* Construction */

Generator::Generator() {}

Generator::~Generator() {}

Generator::Generator(const std::string& filePrefix, parsing::Declarations& decl)
{
    this->filePrefix = filePrefix;
    /* construct filename from prefix and suffix */
    /* Set all the parsed information */
    SetUViews(decl.unparse_views);
    SetRViews(decl.rewrite_views);
    // important: uviews and rviews have to be set before unparse and rewrite rules, as
    // the transformer needs them
    SetPhylumDeclarations(decl.phylum_declarations);
    SetUnparseRules(decl.unparse_rules);
    SetRewriteRules(decl.rewrite_rules);
    SetRedirections(decl.code_redirections);
    SetStatistics(decl.tree_statistics);
}

/* Setter */

void Generator::SetPhylumDeclarations(vector<Phylum>& thePhylumDeclarations)
{
    this->phylumDeclarations = thePhylumDeclarations;
}

void Generator::SetUnparseRules(std::vector<tree::UnparseRule>& theUnparseRules)
{
    FriendlyTransformer t;
    vector<FriendlyUnparseRule> frules = t.Transform(theUnparseRules);
    this->unparseRules = frules;
}

void Generator::SetRewriteRules(std::vector<tree::RewriteRule>& theRewriteRules)
{
    FriendlyTransformer t;
    vector<FriendlyRewriteRule> frules = t.Transform(theRewriteRules);
    this->rewriteRules = frules;
}

void Generator::SetUViews(std::vector<tree::UView>& theUViews) { this->uViews = theUViews; }

void Generator::SetRViews(std::vector<tree::RView>& theRViews) { this->rViews = theRViews; }

void Generator::SetRedirections(std::vector<tree::CodeRedirection>& theCodeRedirections)
{
    this->codeRedirections = theCodeRedirections;
}

void Generator::SetStatistics(tree::TreeStatistics& theStatistics)
{
    this->statistics = theStatistics;
}

void Generator::SetNamespaceName(const std::string& name) { this->namespaceName = name; }

/* Functions */

void Generator::GenerateFile()
{
    /** Generate new file */
    runtime::log("Trying to generate output file {}", this->filename);
    std::ofstream file("tmp_" + this->filename, ios::out | ios::trunc);
    if (!file.good())
    {
        runtime::log<runtime::level::fatal>(
            "Could not create temporary file tmp_{}", this->filename
        );
    }
    else
    {
        string temp = this->Generate();
        this->completeLineStatements(temp);
        this->fixTabCharacters(temp);
        file << temp;
        runtime::log("Successfully generated output.");
        file.flush();
        file.close();
        runtime::log("Sucessfully saved file {}", this->filename);
    }
    /** Compare new file with existing file, only replacing
     * (and therefore notifying changes) if they're different
     */
    std::ifstream out_file(this->filename, ios::ate | ios::binary);
    if (out_file.fail())
    {
        // Output file does not exist, so we can simply rename
        std::rename(("tmp_" + this->filename).c_str(), this->filename.c_str());
    }
    else
    {
        // Output file exists already, so we need to check for difference
        std::ifstream tmp_file("tmp_" + this->filename, ios::ate | ios::binary);
        if (tmp_file.fail())
        {
            runtime::log<runtime::level::fatal>(
                "Could not open temporary file tmp_{} for comparison", this->filename
            );
        }
        else
        {
            bool equal = out_file.tellg() == tmp_file.tellg();
            if (equal)
            {
                // Files are the same length, need to compare contents
                out_file.seekg(0, ios::beg);
                tmp_file.seekg(0, ios::beg);
                equal = std::equal(
                    std::istreambuf_iterator<char>(out_file.rdbuf()),
                    std::istreambuf_iterator<char>(),
                    std::istreambuf_iterator<char>(tmp_file.rdbuf())
                );
            }
            if (equal)
            {
                // can remove tmp file
                std::remove(("tmp_" + this->filename).c_str());
            }
            else
            {
                // Different -> use new file
                std::remove(this->filename.c_str());
                std::rename(("tmp_" + this->filename).c_str(), this->filename.c_str());
            }
        }
    }
}

void Generator::completeLineStatements(std::string& file)
{
    size_t i = 0;
    int lineNr = 1;
    while (i < file.length() - lineReset.length())
    {
        if (file.find(lineReset, i) == string::npos)
        {
            break;
        }
        else if (file.find("\n", i) == string::npos || file.find(lineReset, i) < file.find("\n", i))
        {
            i = file.find(lineReset, i);
            file.replace(
                i, lineReset.length(),
                "#line " + to_string(lineNr + 1) + " \"" + std::filesystem::current_path().string()
                    + "/" + this->filename + "\""
            );
        }
        else
        {
            lineNr++;
            i = file.find("\n", i) + 1;
        }
    }
}

void Generator::fixTabCharacters(std::string& file)
{
    size_t start_pos = 0;
    while ((start_pos = file.find("\t", start_pos)) != string::npos)
    {
        file.replace(start_pos, 1, "    ");
    }
}

const std::string Generator::GetFileNamesFromList(std::vector<string>& filesIncludedFrom)
{
    string result = "";
    for (auto file = filesIncludedFrom.begin(); file != filesIncludedFrom.end(); ++file)
    {
        result += *file;
        if (next(file) != filesIncludedFrom.end())
        {
            result += ",\n";
        }
    }
    return result;
}

const std::string Generator::GetRedirectionsFor(
    string symbol, std::vector<string>& filesIncludedFrom, const string& ofilename
)
{
    string result = "";
    /* Iterate all redirections */
    for (auto codeRed : codeRedirections)
    {
        /* does this redirection apply to the symbol? */
        bool applies = false;
        /* Iterate all places where the current redirection should be redirected */
        for (auto redSym : codeRed.GetRedirections())
        {
            /* Get file ending of supplied optional input file */
            string ending;
            string shortRed = "";
            if (filename != "")
            {
                ending = filename.substr(filename.find("."));
                /* Set CODE or HEADER depending on ending */
                if (ending == ".cc")
                {
                    shortRed = "CODE";
                }
                else
                {
                    shortRed = "HEADER";
                }
            }

            /* If symbol is CODE or HEADER, check the current filename (change the
             * extensions for matching) */
            if ((codeRed.GetFileName().substr(0, codeRed.GetFileName().length() - 2) + ending)
                    == ofilename
                && redSym == shortRed)
            {
                applies = true;
                break;
            }
            /* Otherwise, just check if symbol is in the redirections and not CODE or
             * HEADER, this redirection applies to symbol. Exit loop. */
            else if (redSym == symbol && redSym != "CODE" && redSym != "HEADER")
            {
                applies = true;
                break;
            }
        }
        /* If the current redirection applies, add it to the resulting string */
        if (applies)
        {
            result += "#line " + to_string(codeRed.GetLineNr()) + " \"" + codeRed.GetFileName()
                + "\"\n";
            result += codeRed.GetCode() + "\n";
            result += lineReset + "\n";
            /* If the file we redirect from is not in the list already, add it */
            if (find(filesIncludedFrom.begin(), filesIncludedFrom.end(), codeRed.GetFileName())
                == filesIncludedFrom.end())
            {
                filesIncludedFrom.push_back(codeRed.GetFileName());
            }
        }
    }
    return result;
}

}  // namespace generator
