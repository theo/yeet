/****************************************************************************************
  Term processor yeet. General declarations for code generation

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file Generator.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Declarations for code generation
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <fstream>
#include <string>

#include "FriendlyRewriteRule.hh"
#include "FriendlyUnparseRule.hh"
#include "runtime.hh"

namespace generator
{
/** Base class for all classes that generate cpp code from the parsed information. */
class Generator
{
private:
    /* Setters */

    /** Set the list of phylum declarations read from the source files
     *  \param[in] thePhylumDeclarations The phylum declarations from the source files.
     */
    void SetPhylumDeclarations(std::vector<tree::Phylum>& thePhylumDeclarations);
    /** Set the list of unparse rules read from the source files
     * \param[in] theUnparseRules The unparse rules from the source files.
     */
    void SetUnparseRules(std::vector<tree::UnparseRule>& theUnparseRules);
    /** Set the list of rewrite rules read from the source files
     * \param[in] theRewriteRules The rewrite rules from the source files.
     */
    void SetRewriteRules(std::vector<tree::RewriteRule>& theRewriteRules);
    /** Set the list of unparse views read from the source files
     * \param[in] theUViews The unparse views from the source files.
     */
    void SetUViews(std::vector<tree::UView>& theUViews);
    /** Set the list of rewrite views read from the source files
     * \param[in] theRViews The rewrite views from the source files.
     */
    void SetRViews(std::vector<tree::RView>& theRViews);
    /** Set the list of code redirections read from the source files
     * \param[in] theCodeRedirections The code redirections from the source files.
     */
    void SetRedirections(std::vector<tree::CodeRedirection>& theCodeRedirections);
    /** Set the statistics read from the source files
     * \param[in] theStatistics The code redirections from the source files.
     */
    void SetStatistics(tree::TreeStatistics& theStatistics);
    /** replace the placeholder with corresponding lineNumber
     * \param[in] The almost finished file
     */
    void completeLineStatements(std::string& file);
    /** replace tab character with specified amount of spaces
     * \param[in] The almost finished file
     */
    void fixTabCharacters(std::string& file);

protected:
    /* Constants and Helpers */

    /** Prefix for members of the generated operator classes. Names will be prefixX, where
     * X is a number. */
    const std::string opMemberPrefix = "__arg__";
    /** Prefix for rewritten Members. Names will be prefixX, where X is a number. */
    const std::string opRewritePrefix = "__r_arg__";
    /** Pattern to match for resetting #line-statements in the final step of. */
    const std::string lineReset = "__#line__reset__";
    /** Prefix of the generated file. */
    std::string filePrefix = "";
    /** Name of the (subclass-specific) file suffix, including .cc/.hh */
    std::string fileSuffix = "";
    /** Name of the output file. */
    std::string filename;
    /** Name of the namespace. */
    std::string namespaceName = "yeet";

    /* Parsed Info */

    /** List of phylum declarations in the source files */
    std::vector<tree::Phylum> phylumDeclarations;
    /** List of unparse rules in a generator friendly representation */
    std::vector<FriendlyUnparseRule> unparseRules;
    /** List of rewrite rules in a generator friendly representation */
    std::vector<FriendlyRewriteRule> rewriteRules;
    /** The defined unparseViews*/
    std::vector<tree::UView> uViews;
    /** The defined rewriteViews*/
    std::vector<tree::RView> rViews;
    /** List of code redirections in the source files */
    std::vector<tree::CodeRedirection> codeRedirections;
    /** Extra attributes found in parsing */
    tree::TreeStatistics statistics;

    /* Functions */

    /** Generates the output file. Must be implemented by all subclasses. */
    virtual std::string Generate() = 0;

    /** Returns a string containing the code redirections applicable to symbol.
     *  \param[in] symbol The symbol to filter the redirections.
     *  \param[out] filesIncludedFrom List of files the redirections that apply to the
     * given symbol originate from. \param[in] ofilename The filename to check for if CODE
     * or HEADER are found.
     */
    const std::string GetRedirectionsFor(
        std::string redirectionSymbol, std::vector<std::string>& filesIncludedFrom,
        const std::string& ofilename = ""
    );

    /** Helper method to get a file list from the filesIncludedFrom argument of
     * GetRedirectionsFor. \param[in] filesIncludedFrom The list of files from
     * GetRedirectionsFor.
     */
    const std::string GetFileNamesFromList(std::vector<std::string>& filesIncludedFrom);

public:
    Generator();
    /**
     * Create an instance of the generator class.
     * \param[in] filePrefix Name of the input file.
     * \param[in] logger Logger to use for status messages.
     * \param[in] driver The driver used to do the parsing.
     */
    Generator(const std::string& filePrefix, parsing::Declarations& decl);
    ~Generator();
    /** Generate an output file. Wrapper around the internal Generate function that
     * contains subclass specific code.
     */
    void GenerateFile();
    /** Set a custom name for the generated namespace.
     * \param[in] name The name to set.
     */
    void SetNamespaceName(const std::string& name);
};
}  // namespace generator
