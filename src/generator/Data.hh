/****************************************************************************************
  Term processor yeet. Definitions data to fill into templates. Wraps an std::map

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file Data.hh
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definitions to fill data into templates.
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2023 - present
 *
 */

#include <map>
#include <string>
#include <string_view>

#include <fmt/format.h>

namespace generator
{
namespace templates
{

/**
 * @brief Data class for rendering. Basically an std::map<std::string_view, std::string>
 * with a function to append to existing values. The map only has its own copy of the
 * value string, not the keys! Make sure that the data that describes the keys is valid as
 * long as the object is valid! This can be done via e.g. only using string literals as
 * keys or using the variable names from templates, since those are compile time strings!
 *
 */
class Data : public std::map<std::string_view, std::string>
{
public:
    /**
     * @brief Default Constructor. Automatically inserts the key-value ("","") for ease of
     * use when dealing with templates
     *
     */
    Data() : std::map<std::string_view, std::string>{{"", ""}} {}
    /**
     * @brief Append 'value' to 'key'
     *
     * @param key
     * @param value
     */
    void append_to(const std::string_view key, const std::string_view value)
    {
        if (find(key) == end())
        {
            emplace(key, value);
            return;
        }
        at(key) = fmt::format("{}{}", at(key), value);
    }
};

}  // namespace templates
}  // namespace generator
