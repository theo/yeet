/****************************************************************************************
  Term processor yeet. Declarations for file generation friendly unparse rules

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file FriendlyUnparseRule.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Declarations for friendly unparse rules
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <iostream>

#include "../tree/Operator.hh"
#include "../tree/OutmostPattern.hh"
#include "../tree/UView.hh"
#include "FriendlyUnparseClause.hh"

namespace generator
{

/**
 * Class for storing unparse rules in a better structure.
 */
class FriendlyUnparseRule
{
private:
    tree::UView view;
    tree::OutmostPattern pattern;
    FriendlyUnparseClause clause;
    /**
     * lineNr stands for the line in the .k file where the item is described
     */
    int lineNr;
    /**
     * fileName stands for the .k file in which the item is described
     */
    std::string fileName = "";
    /** Function to return the fileName
     * \return The fileName of the .k file in which the item is described
     */

public:
    FriendlyUnparseRule(
        tree::UView view, tree::OutmostPattern pattern, FriendlyUnparseClause clause, int lineNr,
        std::string fileName
    );
    bool MatchesOperator(tree::Operator op);
    bool ForView(tree::UView view);
    bool ForView(const std::string& name);
    FriendlyUnparseClause& GetFriendlyUnparseClause();
    tree::OutmostPattern& GetPattern();
    friend std::ostream& operator<<(std::ostream& os, const FriendlyUnparseRule& upr);
    /** Function to return the fileName string
     * \return The name of the corresponding .k file
     */
    std::string GetFileName();
    /** Function to return the lineNumber
     * \return The lineNumber where the item is described
     */
    int GetLineNr();
    /** Overloaded operator to print an unparseitem to an outstream
     * \param os The stream the item will be printed to
     * \param upr The item that will be printed
     * \return A reference to the resulting stream after printing
     */
    void SetLineNr(int lineNr);
    void SetFileName(const std::string& fileName);
};
}  // namespace generator
