/****************************************************************************************
  Term processor yeet. Definitions for runtime helpers

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file runtime.cc
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Definitions for runtime helpers
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <cstdlib>
#include <sstream>
#include <string_view>
#include <vector>

#include <cmdline.h>
#include <fmt/format.h>
#include <fmt/color.h>
#include <fmt/ostream.h>

#include <runtime.hh>
#include <tree/CodeRedirection.hh>
#include <tree/OutPatternStatus.hh>
#include <tree/Phylum.hh>
#include <tree/PhylumStatus.hh>
#include <tree/RewriteRule.hh>
#include <tree/RView.hh>
#include <tree/UnparseRule.hh>
#include <tree/UView.hh>

namespace runtime
{

constexpr std::string_view fatal_str = "FATAL";
constexpr std::string_view err_str = "ERROR";
constexpr std::string_view warn_str = "WARNING";
constexpr std::string_view info_str = "INFO";
constexpr std::string_view debug_str = "DEBUG";

using namespace std::string_view_literals;
auto level_to_text(level lvl) -> decltype(fmt::styled(""sv, {}))
{
    switch (lvl)
    {
    case level::fatal:
        return fmt::styled(fatal_str, fmt::emphasis::bold | fg(fmt::terminal_color::red));
    case level::err:
        return fmt::styled(err_str, fg(fmt::terminal_color::red));
    case level::warn:
        return fmt::styled(warn_str, fg(fmt::terminal_color::yellow));
    default:
    case level::info:
        return fmt::styled(info_str, fg(fmt::terminal_color::green));
    case level::debug:
        return fmt::styled(debug_str, fg(fmt::terminal_color::cyan));
    }
}

gengetopt_args_info args;

void evaluateParameters(int argc, char** argv)
{
    // initialize the parameters structure
    struct cmdline_parser_params* params = cmdline_parser_params_create();

    // call the cmdline parser
    if (cmdline_parser(argc, argv, &args) != 0)
    {
        log<level::err>("invalid command-line parameters");
        exit(EXIT_FAILURE);
    }

    free(params);
}
}  // namespace runtime

namespace parsing
{
void PrintPhyla(std::vector<tree::Phylum>& phylum_declarations)
{
    runtime::log<runtime::level::debug>("Phyla:");
    // For every Phylum
    for (auto& phy : phylum_declarations)
    {
        // Print their name and status
        runtime::log<runtime::level::debug>(
            "{} ({}):", phy.GetName(), fmt::underlying(phy.GetStatus())
        );
        // For every Operator of the Phylum
        for (auto& op : phy.GetOperators())
        {
            // Print its name
            std::ostringstream output;
            fmt::println(output, "\t{}(", op.GetName());
            // For every Argument of an Operator
            for (size_t i = 0; i < op.GetChildren().size(); i++)
            {
                // Print their name, includes ternary operator
                // to make fancy formatting
                fmt::println(
                    output, "{}{}", op.GetChildren().at(i),
                    (i != op.GetChildren().size() - 1) ? " " : ""
                );
            }
            fmt::println(output, ")");
            runtime::log<runtime::level::debug>("{}", output.view());
            // std::cout << "\n";
        }
        runtime::log<runtime::level::debug>("\t{{");
        for (auto& att : phy.GetCode().GetAttributes())
        {
            runtime::log<runtime::level::debug>(
                "\t{} {}{}", att.GetType(), att.GetName(),
                att.GetVal().empty() ? "" : (" =" + att.GetVal())
            );
        }
        runtime::log<runtime::level::debug>("\t}}");
    }
}

void PrintUnpRules(const std::vector<tree::UnparseRule>& unparse_rules)
{
    runtime::log<runtime::level::debug>("Unparse Rules:");
    for (auto& unp : unparse_rules)
    {
        std::ostringstream oss;
        oss << unp;
        runtime::log<runtime::level::debug>("{}", oss.str());
    }
}

void PrintRViewDec(std::vector<tree::RView> rewrite_views)
{
    std::ostringstream oss;
    oss << "RViews: ";
    for (auto rview = rewrite_views.begin(); rview != rewrite_views.end(); ++rview)
    {
        oss << rview->GetName() << (rview != rewrite_views.end() ? " " : "\n");
    }
    runtime::log<runtime::level::debug>("{}", oss.str());
}

void PrintUViewDec(std::vector<tree::UView> unparse_views)
{
    std::ostringstream oss;
    oss << "UViews: ";
    for (auto uview = unparse_views.begin(); uview != unparse_views.end(); ++uview)
    {
        oss << uview->GetName() << (uview != unparse_views.end() ? " " : "\n");
    }
    runtime::log<runtime::level::debug>("{}", oss.str());
}

void PrintRewRules([[maybe_unused]] const std::vector<tree::RewriteRule>& rewrite_rules)
{
    runtime::log<runtime::level::debug>("Rewrite Info not implemented.");
}

void PrintCodeRedirections(
    [[maybe_unused]] const std::vector<tree::CodeRedirection>& code_redirections
)
{
    runtime::log<runtime::level::debug>("Code Redirection Info not implemented.");
}

void PrintInfo(Declarations& decl)
{
    PrintPhyla(decl.phylum_declarations);
    PrintUViewDec(decl.unparse_views);
    PrintUnpRules(decl.unparse_rules);
    PrintRViewDec(decl.rewrite_views);
    PrintRewRules(decl.rewrite_rules);
    PrintCodeRedirections(decl.code_redirections);
}

bool CheckGlobalErrors(Declarations& decl)
{
    /* Check whether all phyla are well defined */
    for (auto& phy : decl.phylum_declarations)
    {
        /* Exceptions for predefined Phyla. */
        if (phy.GetName() != "integer" && phy.GetName() != "casestring" && phy.GetName() != "real")
        {
            /* If there is still a phylum that is used, but not declared
             * (Status::argument). */
            if (phy.GetStatus() != tree::PhylumStatus::phylum_declared)
            {
                runtime::log<runtime::level::fatal>(
                    "Undeclared Phylum {} used in {}:{}", phy.GetName(), phy.GetFileName(),
                    phy.GetLineNr()

                );
                return true;
            }
            /* If there is a phylum without an operator. */
            if (phy.GetOperators().empty())
            {
                runtime::log<runtime::level::err>(
                    "Phylum {} defined without operators in {}:{}", phy.GetName(),
                    phy.GetFileName(), phy.GetLineNr()
                );
                return false;
            }
        }
    }

    /* Check whether all unparse rules are well defined */
    for (auto& unp : decl.unparse_rules)
    {
        /* Check all outermost patterns */
        auto& opattern = unp.GetPattern();
        /* If it is a subterm operator (has arguments), find the number of arguments it is
         * supposed to have. */
        if (opattern.GetStatus() == tree::outpattern_subtermOperator)
        {
            size_t opargs{0};
            bool is_defined{false};
            for (auto& phy : decl.phylum_declarations)
            {
                for (auto& op : phy.GetOperators())
                {
                    if (op.GetName() == opattern.GetID())
                    {
                        is_defined = true;
                        opargs = op.GetChildren().size();
                        break;
                    }
                }
            }
            /* Check whether the operator used is defined. */
            if (!is_defined)
            {
                runtime::log<runtime::level::err>(
                    "Undeclared Operator {} in {}:{}", opattern.GetID(), unp.GetFileName(),
                    unp.GetLineNr()
                );
                return true;
            }
            /* Check whether it has too many arguments. */
            if (opattern.GetSubPatterns().size() > opargs)
            {
                runtime::log<runtime::level::err>(
                    "Too many subpatterns for operator {} in {}:{}", opattern.GetID(),
                    unp.GetFileName(), unp.GetLineNr()
                );
                return true;
            }
            /* Check whether it has not enough arguments. */
            if (opattern.GetSubPatterns().size() < opargs)
            {
                runtime::log<runtime::level::err>(
                    "Too few subpatterns for operator {} in {}:{}", opattern.GetID(),
                    unp.GetFileName(), unp.GetLineNr()
                );
                return true;
            }
        }
    }

    /* Check code redirections */
    for (auto red : decl.code_redirections)
    {
        for (auto code : red.GetRedirections())
        {
            if (!tree::CodeRedirection::IsValid(code))
            {
                runtime::log<runtime::level::err>(
                    "Invalid code redirection {} in {}:{}", code, red.GetFileName(), red.GetLineNr()
                );
                return true;
            }
        }
    }

    return false;
}
}  // namespace parsing
