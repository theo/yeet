/****************************************************************************************
  Term processor yeet. Definitions to describe outmost patterns in rules

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file OutmostPattern.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Implementation for parsed outmost patterns
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "OutmostPattern.hh"

namespace tree
{
OutmostPattern::OutmostPattern() { this->stat = OutPatternStatus::outpattern_undefined; }
OutmostPattern::OutmostPattern(const std::string& name, const std::string& provided)
{
    this->id = name;
    this->stat = OutPatternStatus::outpattern_wildcardOperator;
    this->provided = provided;
}
OutmostPattern::OutmostPattern(
    const std::string& name, std::vector<Pattern> subPatterns, const std::string& provided
)
{
    this->id = name;
    this->stat = OutPatternStatus::outpattern_subtermOperator;
    this->subPatterns = subPatterns;
    this->provided = provided;
}
OutmostPattern::OutmostPattern(const std::string& variable_id, OutmostPattern subPattern)
{
    this->id = variable_id;
    this->stat = OutPatternStatus::outpattern_variable_assignment;
    this->variable_ass_pattern.push_back(subPattern);
}
OutmostPattern::~OutmostPattern() {}
OutPatternStatus OutmostPattern::GetStatus() const { return this->stat; }
std::string OutmostPattern::GetID() const { return this->id; }
OutmostPattern& OutmostPattern::GetSubPattern() { return this->variable_ass_pattern.at(0); }
std::vector<Pattern>& OutmostPattern::GetSubPatterns() { return this->subPatterns; }
void OutmostPattern::SetWildcardLength(int length)
{
    this->wildcardOperatorArgumentLength = length;
}
int OutmostPattern::GetWildcardLength() const { return this->wildcardOperatorArgumentLength; }
std::string OutmostPattern::GetProvided() const { return this->provided; }
bool OutmostPattern::IsWildcard()
{
    if (this->stat == tree::outpattern_wildcardOperator)
    {
        return true;
    }
    else
    {
        return false;
    }

    // // Op
    // if (this->stat == tree::op_wildcardOperator)
    // {
    //     return true;
    // }

    // // exp1=...
    // if (this->stat == tree::op_variable_assignment)
    // {
    //     // exp1=Op
    //     if (this->subPattern.GetStatus() == p_variable_name)
    //     {
    //         return true;
    //     }
    //     // exp1=Op(...)
    //     if (this->subPattern.GetStatus() == p_subtermOperator)
    //     {
    //         for (auto p = this->subPattern.GetSubPatterns().begin(); p !=
    //         this->subPattern.GetSubPatterns().end(); ++p)
    //         {
    //             //... = list of wildcards or variables
    //             // TODO: vairables must not have subtrees
    //             if (p->GetStatus() != p_wildcard && p->GetStatus() !=
    //             p_variable_assignment)
    //             {
    //                 return false;
    //             }
    //         }
    //         return true;
    //     }
    // }

    // // Op(...)
    // if (this->stat == tree::op_subtermOperator)
    // {
    //     for (auto p = this->subPatterns.begin(); p != this->subPatterns.end(); ++p)
    //     {
    //         if (p->GetStatus() != p_wildcard && p->GetStatus() !=
    //         p_variable_assignment)
    //         {
    //             return false;
    //         }
    //     }
    //     return true;
    // }
    // return false;
}  // namespace tree
bool OutmostPattern::operator==(OutmostPattern& p2)
{
    if (this->GetProvided() != p2.GetProvided())
        return false;
    if (this->GetID() != p2.GetID())
        return false;
    if (this->GetStatus() != p2.GetStatus())
        return false;
    if (this->GetStatus() == OutPatternStatus::outpattern_variable_assignment)
    {
        if (!(this->GetSubPattern() == p2.GetSubPattern()))
            return false;
    }
    // need to check if every subpattern of the first pattern is also a
    // subpattern in the second pattern
    bool equals = false;
    for (auto& subp1 : this->GetSubPatterns())
    {
        for (auto& subp2 : p2.GetSubPatterns())
        {
            if (subp1 == subp2)
                equals = true;
        }
        if (equals == false)
            return false;
        else
            equals = false;
    }
    // for real equality we also need to check the other direction, if every
    // subpattern of the second pattern is also a subpattern in the first
    // pattern
    equals = false;
    for (auto& subp1 : p2.GetSubPatterns())
    {
        for (auto& subp2 : this->GetSubPatterns())
        {
            if (subp1 == subp2)
                equals = true;
        }
        if (equals == false)
            return false;
        else
            equals = false;
    }
    return true;
}

}  // namespace tree
