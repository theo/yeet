/****************************************************************************************
  Term processor yeet. Declarations for a parsed rewrite clause

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file RewriteClause.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header file for rewrite clauses
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <vector>

#include "RView.hh"
#include "RewriteTerm.hh"

namespace tree
{
/**
 * A class to read in a single rewrite clause
 */
class RewriteClause
{
private:
    /** A RewriteClause includes a bunch of views under which the clause will
     * be applied */
    std::vector<RView> views;
    /** A RewriteClause consists of a term which replaces the original phylum */
    RewriteTerm terms;
    /** A string containing the name of the variable which the resulting term will be
     * assigned to, allowing accessing it in added C++ code */
    std::string result_name = "__result__";
    /** A string containing the C++ code added by the user */
    std::string code = "";

public:
    /** Default constructor, bison wants this */
    RewriteClause();
    /** Constructor for a rewriteclause with a set of views and a term
     * \param item The replacing term
     * \param views The vector containing the relevant views
     */
    RewriteClause(std::vector<RView> views, RewriteTerm term);
    /** Constructor for a rewriteclause with a set of views, a term, an identifier for the
     * term and the C++ code \param item The replacing term \param views The vector
     * containing the relevant views \param result_name The name used to refer to the
     * resulting term \param code The user defined C++ code
     */
    RewriteClause(
        std::vector<RView> views, RewriteTerm term, const std::string& result_name,
        const std::string& code
    );
    /** Default deconstructor */
    ~RewriteClause();
    /** Function to return the replacing term
     * \return A reference to the term
     */
    RewriteTerm& GetRewriteTerms();

    /** Function to return the views of the clause
     * \return A reference to the vector containing the views
     */
    std::vector<RView>& GetViews();
    /** Function to return the name refering to the resulting term
     * \return A string containing the name
     */
    std::string GetResultName();
    /** Function to return the C++ code defined by the user
     * \return A string containing the code
     */
    std::string GetCode();
    bool operator==(RewriteClause& rc2);
};  // namespace tree
}  // namespace tree
