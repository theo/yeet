/****************************************************************************************
  Term processor yeet. Declarations for a parsed phylum

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file Phylum.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header file for pyhla
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <iostream>
#include <string>
#include <vector>

#include "CodeOption.hh"
#include "Operator.hh"
#include "PhylumStatus.hh"
#include "TreeInfo.hh"

namespace tree
{
class Operator;
/**
 * A class to read in a single Phylum declaration.
 */
class Phylum : public TreeInfo
{
private:
    /** List of operators that can produce this Phylum. */
    std::vector<Operator> operators;
    /** Name of this Phylum. */
    std::string name;
    /** The PhylumStatus of the phylum, if it's declared only as an Operatorargument
     * or standalone*/
    PhylumStatus stat;
    /** C++-Code specifying the attributes of this Phylum. */
    CodeOption code;
    /** Boolean to determine if the phylum is a list phylum so we know that we need to
     * generate list member functions */
    bool isList = false;
    /** The name of the element in case the phylum is a list phylum */
    std::string element = "";

public:
    /**
     * Constructor for a Phylum.
     * \param name The name of the Phylum.
     */
    Phylum(
        const std::string& name, PhylumStatus init, const std::string_view fileName, int lineNr,
        CodeOption code
    );
    /**
     * Destructor for a Phylum.
     */
    ~Phylum();
    /**
     * Sets the name of this Phylum.
     * \param name The name to set.
     */
    void SetName(const std::string& name);
    /**
     * Returns the name of this Phylum.
     */
    const std::string GetName() const;
    /**
     * Add an Operator that can produce this Phylum.
     * \param op The Operator to add.
     */
    void AddOperator(Operator& op);
    /**
     * Search for an operator in this Phylum by name.
     * \param name The name of the target.
     * \returns The operator with name if it exists, NULL otherwise.
     */
    const Operator* GetOperator(const std::string& name) const;
    /**
     * Checks whether the other Phylum has the same name.
     * \param other The other Phylum.
     */
    bool FlatEquals(const Phylum& other) const;
    /**
     * Returns a vector containing copies of the Operators of the Phylum
     */
    std::vector<Operator> GetOperators();
    /**
     * Sets the PhylumStatus of the Phylum
     * \param stat the new PhylumStatus
     */
    void SetStatus(PhylumStatus stat);
    /**
     * Returns the current PhylumStatus of the Phylum
     */
    PhylumStatus GetStatus();
    /**
     * Sets the custom declared code for this Phylum.
     * \param code The code to set.
     */
    void SetCode(CodeOption code);
    /**
     * Returns the custom code declared with the Phylum.
     */
    CodeOption GetCode() const;
    /**
     * Sets the Phylum as a list phylum
     * \param isList The value, defaults to true but can be set to false to remove list
     * property
     */
    void SetList(bool isList = true);
    /**
     * Returns if the Phylum is a list phylum
     */
    bool IsList() const;
    /**
     * Sets the element name of the list phylum
     * \param element The identifier for the element
     */
    void SetElement(const std::string& element);
    /**
     * Returns if the Phylum is a list phylum
     */
    const std::string GetElementName() const;
};
}  // namespace tree
