/****************************************************************************************
  Term processor yeet. Declarations for a term in a rewrite clause

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file RewriteTerm.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header file for terms in rewrite clauses
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <string>
#include <vector>

#include "RewriteTermStatus.hh"

namespace tree
{
/**
 * A class to read in a single rewrite term
 */
class RewriteTerm
{
private:
    RewriteTermStatus stat = RewriteTermStatus::rterm_undefined;
    std::string content = "";
    int int_literal = 0;
    /* You may ask yourself why this is a one-element vector.
     *  This is part of a pretty whacky solution to a problem where
     *  the proper solution is too tedious for me.
     *  The parser for a *.k file creates nodes in local scopes and
     *  copies them through vector push_backs or copies in general.
     *  The more elegant solution would be using pointers, therefore
     *  dynamically reserving space for the syntax tree and copying
     *  only pointers. This is a lot of work to properly manage and
     *  since only the generated code should be efficient, we can allow
     *  ourselves this solution.
     *  The only caveat of this is here, where a RewriteTerm can include
     *  another RewriteTerm in its declaration. (for member functions etc.)
     *  Since we can't save a copy of an object in an object of itself,
     *  we just use a container to guard against this.
     */
    std::vector<RewriteTerm> subTerm;
    std::vector<RewriteTerm> subTerms;

public:
    RewriteTerm();
    RewriteTerm(int int_literal);
    RewriteTerm(const std::string& content, RewriteTermStatus stat);
    RewriteTerm(const std::string& content, std::vector<RewriteTerm> subTerms);
    RewriteTerm(
        std::vector<RewriteTerm> subTerm, RewriteTermStatus stat, const std::string& identifier
    );
    RewriteTerm(
        std::vector<RewriteTerm> subTerm, const std::string& content,
        std::vector<RewriteTerm> subTerms, RewriteTermStatus stat
    );
    ~RewriteTerm();
    RewriteTermStatus GetStatus();
    int GetInt();
    std::string GetContent();
    std::vector<RewriteTerm> GetSubterms();
    RewriteTerm& GetSubterm();
    bool operator==(RewriteTerm& r2);
};
}  // namespace tree
