/****************************************************************************************
  Term processor yeet. Definitions for a parsed rewrite clauses

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file RewriteClause.cc
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Implementation file for rewrite clauses
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "RewriteClause.hh"

#include <vector>

namespace tree
{
RewriteClause::RewriteClause() {}
RewriteClause::RewriteClause(std::vector<RView> views, RewriteTerm term)
{
    this->views = views;
    if (views.size() == 0)
    {
        this->views.push_back(RView());
    }
    this->terms = term;
}
RewriteClause::RewriteClause(
    std::vector<RView> views, RewriteTerm term, const std::string& result_name,
    const std::string& code
)
{
    this->views = views;
    if (views.size() == 0)
    {
        this->views.push_back(RView());
    }
    this->terms = term;
    this->result_name = result_name;
    this->code = code;
}
RewriteClause::~RewriteClause() {}
/*void RewriteClause::AddRewriteTerm(RewriteTerm term)
{
    this->terms.push_back(term);
}*/
RewriteTerm& RewriteClause::GetRewriteTerms() { return this->terms; }
std::vector<RView>& RewriteClause::GetViews() { return this->views; }
std::string RewriteClause::GetResultName() { return this->result_name; }
std::string RewriteClause::GetCode() { return this->code; }
bool RewriteClause::operator==(RewriteClause& rc2)
{
    return this->GetRewriteTerms() == rc2.GetRewriteTerms();
}
}  // namespace tree
