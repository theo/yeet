/****************************************************************************************
  Term processor yeet. Declarations for an item in an unparse clause

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file UnparseItem.hh
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header file for an unparse item
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <string>

#include "UnparseItemStatus.hh"

namespace tree
{
/** A class describing a single UnparseItem, which may now simply be a string
 * literal, a variable which needs to further be unparsed or custom C++ code which will
 * be executed upon matching the pattern */
class UnparseItem
{
private:
    /** The state of the Item, either a string literal, a variable, or a cbody */
    UnparseItemStatus stat = UnparseItemStatus::unparse_string_literal;
    /** Content of the Item, either the content of the string literal or the
     * variable which needs to be refered for further unparsing. There's probably
     * a better way to implement this, but I don't know how the code generation
     * will work later, so I just used strings for both for now.
     * This also contains the pasted code in case of cbody,
     * current functionality allows us to just parse that as a string,
     * since Dollarvars aren't used yet, so all pasted code is standalone
     * C++ code
     */
    std::string content = "";

public:
    /** Default constructor, bison wants this */
    UnparseItem();
    /** Constructor for an unparseitem
     * \param content The content of the item, either an identifier for a
     * variable or a string literal \param stat The state of the item, used to
     * differentiate the semantics of the content string
     */
    UnparseItem(const std::string& content, UnparseItemStatus stat);
    /** Default deconstructor */
    ~UnparseItem();
    /** Function to return the state
     * \return The state
     */
    UnparseItemStatus GetStatus();
    /** Function to return the content string
     * \return The string
     */
    std::string GetContent();
    /** Overloaded operator to print an unparseitem to an outstream
     * \param os The stream the item will be printed to
     * \param upr The item that will be printed
     * \return A reference to the resulting stream after printing
     */
    friend std::ostream& operator<<(std::ostream& os, const UnparseItem& upi);
    bool operator==(UnparseItem& ui2);
};
}  // namespace tree
