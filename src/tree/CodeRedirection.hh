/****************************************************************************************
  Term processor yeet. Declarations for code that is copied verbatim from .k files

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file CodeRedirection.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header file for redirected code from .k files
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <string>
#include <vector>

#include "RedirectionSymbol.hh"
#include "TreeInfo.hh"

namespace tree
{
/** Class representing a code redirection. */
class CodeRedirection : public TreeInfo
{
public:
    CodeRedirection();
    CodeRedirection(
        std::string code, std::vector<std::string> redirections, const std::string_view fileName,
        int lineNr
    );
    ~CodeRedirection();
    /** Get the cpp code associated with this redirection.
     * \returns The codesnippet.
     */
    std::string GetCode();
    void SetCode(std::string code);
    /** Get all places the code should be redirected to. */
    std::vector<std::string> GetRedirections();
    /** Given a redirection of type KC_FUNCTIONS_..., get the associated filename.
     * \returns The filename.
     */
    std::string GetFilenameFromRedirection(std::string redirection);
    /** Check whether two redirection symbols are equal. For KC_FUNCTIONS, the filename is
     * ignored. \param[in] type The type to check for. \returns Whether redirection equals
     * other.
     */
    bool MatchesType(std::string type);
    /** Check whether the supplied redirection matches the type.
     * \param[in] code The code to check.
     * \param[in] type The type to check for.
     * \returns true, if code is of type type.
     */
    static bool MatchesType(std::string code, std::string type);
    /** Check whether the supplied redirection is valid (matches any of the possible
     * redirections). */
    static bool IsValid(std::string code);
    /** Set the file the code is redirected to.
     * \param[in] redirections The list of redirection codes. Can be one of the following
     * values: KC_TYPES                          k.cc implementation of all classes
     * generated from phylum and operator definitions KC_TYPES_HEADER k.hh class
     * declarations generated from phylum and operator definitions KC_REWRITE rk.cc
     * rewrite method implementations KC_REWRITE_HEADER                 rk.hh rewrite view
     * definitions KC_UNPARSE                        unpk.cc unparse method definitions
     * KC_UNPARSE_HEADER                 unpk.hh unparse view definitions
     * KC_FUNCTIONS_<filename>           function definitions from an input .k file to put
     * in a separate .cc file KC_FUNCTIONS_<filename>_HEADER    declarations from an input
     * .k file to put in a separate .hh file
     */
    void SetRedirections(std::vector<std::string> redirections);

private:
    /** All possible redirections. */
    const static std::string allRedirections[];
    /** The cpp code to redirect. */
    std::string code;
    /** File the code is redirected to. Can be one of the following values:
     *
     * KC_TYPES                          k.cc implementation of all classes generated from
     * phylum and operator definitions KC_TYPES_HEADER                   k.hh class
     * declarations generated from phylum and operator definitions KC_REWRITE rk.cc
     * rewrite method implementations KC_REWRITE_HEADER                 rk.hh rewrite view
     * definitions KC_UNPARSE                        unpk.cc unparse method definitions
     * KC_UNPARSE_HEADER                 unpk.hh unparse view definitions
     * KC_FUNCTIONS_<filename>           function definitions from an input .k file to put
     * in a separate .cc file KC_FUNCTIONS_<filename>_HEADER    declarations from an input
     * .k file to put in a separate .hh file
     */
    std::vector<std::string> redirections;
};
}  // namespace tree
