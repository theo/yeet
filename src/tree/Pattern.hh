/****************************************************************************************
  Term processor yeet. Declarations for parsed patterns in rules

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file Pattern.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header file for parsed patterns
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <iostream>
#include <string>
#include <vector>

#include "PatternStatus.hh"

namespace tree
{
/** A class describing a pattern which isn't an outermost pattern */
class Pattern
{
private:
    /** The state of the pattern */
    PatternStatus stat = PatternStatus::pattern_undefined;
    /** Part of the content of the pattern, meaning depends on status */
    std::string id = "";
    /** Part of the content of the pattern, meaning depends on status */
    std::vector<Pattern> subPatterns;
    /** Part of the content of the pattern, meaning depends on status */
    int int_literal = 0;

public:
    /** Default constructor, describes the '*' pattern */
    Pattern();
    /** constructor describing either a string literal pattern or an operator
     * which wildcards the amount of subterm, differentiated with the given
     * status
     *
     * \param name The content of the pattern, either the name of the
     * operator or the string literal
     * \param stat The state of the pattern, used
     * to differentiate between operator wildcard or string literal
     */
    Pattern(const std::string& name, PatternStatus stat);
    /** constructor describing an operator with a set amount of subterms
     *
     * \param name The name of the operator the pattern describes
     * \param subPatterns The vector of Subterms of the pattern
     */
    Pattern(const std::string& name, std::vector<Pattern> subPatterns);
    /* constructor describing a pattern which is assigned to another variable
     * Same as outermost pattern concerning the id of the pattern
     *
     * \param variable_id The identifier of the variable the patterns gets
     * assigned to \param subPattern The pattern which will be assigned to the
     * variable
     */
    Pattern(const std::string& variable_id, Pattern subPattern);
    /** constructor describing an integer literal pattern
     *
     * \param int_literal The integer describing the pattern
     */
    Pattern(int int_literal);
    /** Default deconstructor */
    ~Pattern();
    /** Returns the state of the pattern
     * \return The state
     */
    PatternStatus GetStatus() const;
    /** Returns the id string of the pattern
     * \return string
     */
    std::string GetID() const;
    /** Returns the Subpattern of an assignment type pattern
     * \return The Pattern
     */
    Pattern& GetSubPattern();
    /** Returns the Subterms of the pattern
     * \return Vector with patterns
     */
    std::vector<Pattern>& GetSubPatterns();
    /** Returns the integer literal of the pattern
     * \return int
     */
    int GetIntLiteral() const;
    /** Overloaded operator to compare two patterns on equality
     * \param p2 The pattern this will be compared to
     * \return bool
     */
    bool operator==(Pattern& p2);
    /** Overloaded operator to print a pattern to an outstream
     * \param os The stream the pattern will be printed to
     * \param p The pattern that will be printed
     * \return A reference to the resulting stream after printing
     */
    friend std::ostream& operator<<(std::ostream& os, const Pattern& p);
};
}  // namespace tree
