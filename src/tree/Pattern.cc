/****************************************************************************************
  Term processor yeet. Definitions for parsed patterns in rules

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file Pattern.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Implementation for parsed patterns
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "Pattern.hh"

namespace tree
{
Pattern::Pattern()
{
    this->id = "Wildcard";
    this->stat = PatternStatus::pattern_wildcard;
}

Pattern::Pattern(const std::string& name, PatternStatus stat)
{
    this->id = name;
    this->stat = stat;
}
Pattern::Pattern(const std::string& name, std::vector<Pattern> subPatterns)
{
    this->id = name;
    this->stat = PatternStatus::pattern_subtermOperator;
    this->subPatterns = subPatterns;
}
Pattern::Pattern(const std::string& variable_id, Pattern subPattern)
{
    this->id = variable_id;
    this->stat = PatternStatus::pattern_variable_assignment;
    this->subPatterns.push_back(subPattern);
}
Pattern::Pattern(int int_literal)
{
    this->int_literal = int_literal;
    this->stat = PatternStatus::pattern_int_literal;
}
Pattern::~Pattern() {}
PatternStatus Pattern::GetStatus() const { return this->stat; }
std::string Pattern::GetID() const { return this->id; }
Pattern& Pattern::GetSubPattern() { return this->subPatterns.front(); }
std::vector<Pattern>& Pattern::GetSubPatterns() { return this->subPatterns; }
int Pattern::GetIntLiteral() const { return this->int_literal; }
bool Pattern::operator==(Pattern& p2)
{
    if (this->GetID() != p2.GetID())
        return false;
    if (this->GetStatus() != p2.GetStatus())
        return false;
    // need to encapsule the subpattern comparison, if the vector is empty there
    // will be undefined behaviour, so we check if the patterns have a state
    // where this won't be the case the comparison is only really needed in one
    // state, so we use that for encapsulation
    if (this->GetStatus() == PatternStatus::pattern_variable_assignment)
    {
        if (!(this->GetSubPattern() == p2.GetSubPattern()))
            return false;
    }
    if (this->GetIntLiteral() != p2.GetIntLiteral())
        return false;
    // need to check if every subpattern of the first pattern is also a
    // subpattern in the second pattern
    bool equals = false;
    for (auto& subp1 : this->GetSubPatterns())
    {
        for (auto& subp2 : p2.GetSubPatterns())
        {
            if (subp1 == subp2)
                equals = true;
        }
        if (equals == false)
            return false;
        else
            equals = false;
    }
    // for real equality we also need to check the other direction, if every
    // subpattern of the second pattern is also a subpattern in the first
    // pattern
    equals = false;
    for (auto& subp1 : p2.GetSubPatterns())
    {
        for (auto& subp2 : this->GetSubPatterns())
        {
            if (subp1 == subp2)
                equals = true;
        }
        if (equals == false)
            return false;
        else
            equals = false;
    }
    return true;
}
}  // namespace tree
