/****************************************************************************************
  Term processor yeet. Declarations for parsed outmost patterns in rules

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file OutmostPattern.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header files for outmost patterns
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <iostream>
#include <string>
#include <vector>

#include "OutPatternStatus.hh"
#include "Pattern.hh"

namespace tree
{
/** A class including the currently possible outermost patterns */
class OutmostPattern
{
private:
    /** Status of the pattern */
    OutPatternStatus stat = OutPatternStatus::outpattern_undefined;
    /** Part of the content of the pattern, meaning depends on status */
    std::string id = "";
    /** Using vector to save the pattern in case of variable assignment, don't want to use
     * pointer, so we use a vector */
    std::vector<OutmostPattern> variable_ass_pattern = std::vector<OutmostPattern>();
    /** Part of the content of the pattern, meaning depends on status */
    std::vector<Pattern> subPatterns;
    /** TODO */
    int wildcardOperatorArgumentLength = 0;
    /** The string containing the additional pattern matching contraints, given via the
     * "provided" modifier */
    std::string provided = "";

public:
    /** Default constructer, bison wants this, never used */
    OutmostPattern();
    /** Constructer for a pattern consisting only of an operator, wildcarding
     * the amount of subterms \param name Name of the operator
     */
    OutmostPattern(const std::string& name, const std::string& provided);
    /** Constructer for a pattern consisting of an operator with a set amount of
     * subterms \param name Name of the operator \param subPatterns The Subterms
     * of the operator, given as a vector of patterns
     */
    OutmostPattern(
        const std::string& name, std::vector<Pattern> subPatterns, const std::string& provided
    );
    /** Constructer for a pattern consisting of a pattern being assigned to a
     * variable The id of the pattern object mentioned above will be used as the
     * identifier for the variable in this case \param variable_id The
     * identifier of the variable the pattern will be assigned to \param
     * subPattern The pattern that will be assigned to the variable
     */
    OutmostPattern(const std::string& variable_id, OutmostPattern variable_ass_pattern);
    /** Default deconstructor */
    ~OutmostPattern();
    /** Returns the state of the pattern
     * \return The state
     */
    OutPatternStatus GetStatus() const;
    /** Returns the id string of the pattern
     * \return string
     */
    std::string GetID() const;
    /** Returns the Subpattern of an assignment type pattern
     * \return The Pattern
     */
    OutmostPattern& GetSubPattern();
    /** Returns the Subterms of the pattern
     * \return Vector with patterns
     */
    std::vector<Pattern>& GetSubPatterns();
    /** todo */
    void SetWildcardLength(int length);
    /** todo */
    int GetWildcardLength() const;
    /** Returns the contraint given via the "provided" modifier
     * \return The string containing the constraint
     */
    std::string GetProvided() const;
    /** todo
     * Wenn
     */
    bool IsWildcard();
    /** Overloaded operator to compare two patterns on equality
     * \param p2 The pattern this will be compared to
     * \return bool
     */
    bool operator==(OutmostPattern& p2);
    /** Overloaded operator to print an outermost pattern to an outstream
     * \param os The stream the pattern will be printed to
     * \param p The pattern that will be printed
     * \return A reference to the resulting stream after printing
     */
    friend std::ostream& operator<<(std::ostream& os, const OutmostPattern& p);
};
}  // namespace tree
