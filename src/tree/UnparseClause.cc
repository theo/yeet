/****************************************************************************************
  Term processor yeet. Definitions for a parsed unparse clause

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file UnparseClause.cc
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Implementation file for unparse clauses
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "UnparseClause.hh"

namespace tree
{
UnparseClause::UnparseClause() {}
UnparseClause::UnparseClause(std::vector<UView> views, std::vector<UnparseItem> items)
{
    this->items = items;
    this->views = views;
    if (views.size() == 0)
    {
        this->views.push_back(UView());
    }
}
UnparseClause::~UnparseClause() {}
void UnparseClause::AddUnparseItem(UnparseItem item) { this->items.push_back(item); }
std::vector<UnparseItem>& UnparseClause::GetUnparseItems() { return this->items; }
std::vector<UView>& UnparseClause::GetViews() { return this->views; }
bool UnparseClause::operator==(UnparseClause& upc2)
{
    bool equals = false;
    for (auto& t1 : this->GetUnparseItems())
    {
        for (auto& t2 : upc2.GetUnparseItems())
        {
            if (t1 == t2)
            {
                equals = true;
                break;
            }
        }
        if (!equals)
        {
            return false;
        }
        equals = false;
    }
    equals = false;
    for (auto& t1 : upc2.GetUnparseItems())
    {
        for (auto& t2 : this->GetUnparseItems())
        {
            if (t1 == t2)
            {
                equals = true;
                break;
            }
        }
        if (!equals)
        {
            return false;
        }
        equals = false;
    }
    return true;
}
}  // namespace tree
