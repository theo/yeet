/****************************************************************************************
  Term processor yeet. Definitions for helpers to print patterns

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file PatternPrinting.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Pretty print patterns
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <ostream>

#include "OutPatternStatus.hh"
#include "OutmostPattern.hh"
#include "PatternStatus.hh"
#include "Pattern.hh"
#include "UnparseClause.hh"
#include "UnparseItem.hh"
#include "UnparseRule.hh"

namespace tree
{
// the following operators mostly have fancy formatting for readable output

std::ostream& operator<<(std::ostream& os, const tree::Pattern& p)
{
    switch (p.stat)
    {
    case tree::PatternStatus::pattern_int_literal:
        os << p.int_literal;
        break;
    case tree::PatternStatus::pattern_string_literal:
        os << "\"" << p.id << "\"";
        break;
    case tree::PatternStatus::pattern_subtermOperator:
        os << p.id << '(';
        for (size_t i = 0; i < p.subPatterns.size(); i++)
        {
            os << p.subPatterns.at(i) << ((i == p.subPatterns.size() - 1) ? "" : ", ");
        }
        os << ')';
        break;
    case tree::PatternStatus::pattern_variable_assignment:
        os << p.id << '=' << p.subPatterns.front();
        break;
    case tree::PatternStatus::pattern_wildcard:
        os << '*';
        break;
    case tree::PatternStatus::pattern_variable_name:
        os << p.id;
        break;
    default:
        os << '?';
    }
    return os;
}

std::ostream& operator<<(std::ostream& os, const tree::OutmostPattern& op)
{
    switch (op.stat)
    {
    case tree::OutPatternStatus::outpattern_wildcardOperator:
        os << op.id;
        break;
    case tree::OutPatternStatus::outpattern_subtermOperator:
        os << op.id << '(';
        for (size_t i = 0; i < op.subPatterns.size(); i++)
        {
            os << op.subPatterns.at(i) << ((i == op.subPatterns.size() - 1) ? "" : ", ");
        }
        os << ')';
        break;
    case tree::OutPatternStatus::outpattern_variable_assignment:
        os << op.id << '=' << op.variable_ass_pattern.at(0);
        break;
    default:
        os << '?';
    }
    if (op.provided != "")
        os << " provided(" << op.provided << ")";
    return os;
}

std::ostream& operator<<(std::ostream& os, const tree::UnparseItem& upi)
{
    switch (upi.stat)
    {
    case tree::UnparseItemStatus::unparse_string_literal:
        os << "\"" << upi.content << "\"";
        break;
    case tree::UnparseItemStatus::unparse_variable:
        os << upi.content;
        break;
    case tree::UnparseItemStatus::unparse_cbody:
        os << "{" << upi.content << "}";
        break;
    }
    return os;
}

std::ostream& operator<<(std::ostream& os, const tree::UnparseClause& upc)
{
    os << "[";
    for (size_t i = 0; i < upc.views.size(); i++)
    {
        os << upc.views.at(i).GetName() << ((i == upc.views.size() - 1) ? ":" : " ");
    }
    for (size_t i = 0; i < upc.items.size(); i++)
    {
        os << upc.items.at(i) << ((i == upc.items.size() - 1) ? "]" : " ");
    }
    return os;
}

std::ostream& operator<<(std::ostream& os, const tree::UnparseRule& upr)
{
    os << upr.pattern << "\n\t -> " << upr.unparseClause;
    return os;
}

}  // namespace tree
