/****************************************************************************************
  Term processor yeet. Declarations for a parsed unparse rule

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file UnparseRule.hh
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header file for unparse rules
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "OutmostPattern.hh"
#include "Rule.hh"
#include "UView.hh"
#include "UnparseClause.hh"

namespace tree
{
/** A class to read in a single unparse rule*/
class UnparseRule : public Rule
{
private:
    /** An UnparseRule consists of an UnparseClause, annotated later by
     * a view, not used yet */
    UnparseClause unparseClause;

public:
    /** Default constructor, bison wants this */
    UnparseRule();
    /** Constructor for an unparserule to an outermost pattern
     * and an unparseclause
     * \param pattern The outermost pattern
     * \param unparseClause The unparseclause
     */
    UnparseRule(
        OutmostPattern pattern, UnparseClause unparseClause, const std::string_view fileName,
        int lineNr
    );
    /** Default deconstructor */
    ~UnparseRule();
    /** Function to return the unparseclause
     * \return A reference to the unparseclause
     */
    UnparseClause& GetUnparseClause();
    /** Overloaded operator to compare two unparserules on equality
     * The rules are equal if they have the same outermost patterns, so a second
     * rule for the patterns extends the amount of unparseclauses
     * \param r2 The rule this will be compared to
     * \return bool
     */
    bool operator==(UnparseRule& r2);
    /** Overloaded operator to print an unparserule to an outstream
     * \param os The stream the rule will be printed to
     * \param upr The rule that will be printed
     * \return A reference to the resulting stream after printing
     */
    friend std::ostream& operator<<(std::ostream& os, const UnparseRule& upr);
};
}  // namespace tree
