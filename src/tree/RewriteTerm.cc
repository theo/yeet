/****************************************************************************************
  Term processor yeet. Definitions for a term of a rewrite clause

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file RewriteTerm.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Implementation file for a term in a rewrite clause
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "RewriteTerm.hh"

#include <string>

namespace tree
{
RewriteTerm::RewriteTerm() {}
RewriteTerm::RewriteTerm(int int_literal)
{
    this->int_literal = int_literal;
    this->stat = RewriteTermStatus::rterm_int_literal;
}
RewriteTerm::RewriteTerm(const std::string& content, RewriteTermStatus stat)
{
    this->content = content;
    this->stat = stat;
}
RewriteTerm::RewriteTerm(const std::string& content, std::vector<RewriteTerm> subterms)
{
    this->content = content;
    this->subTerms = subterms;
    this->stat = RewriteTermStatus::rterm_operator;
}
RewriteTerm::RewriteTerm(
    std::vector<RewriteTerm> subterm, RewriteTermStatus stat, const std::string& identifier
)
{
    this->subTerm = subterm;
    this->stat = stat;
    this->content = identifier;
}
RewriteTerm::RewriteTerm(
    std::vector<RewriteTerm> subTerm, const std::string& content, std::vector<RewriteTerm> subTerms,
    RewriteTermStatus stat
)
{
    this->subTerm = subTerm;
    this->content = content;
    this->subTerms = subTerms;
    this->stat = stat;
}
RewriteTerm::~RewriteTerm() {}

int RewriteTerm::GetInt() { return this->int_literal; }
std::vector<RewriteTerm> RewriteTerm::GetSubterms() { return this->subTerms; }
RewriteTerm& RewriteTerm::GetSubterm() { return this->subTerm.at(0); }
RewriteTermStatus RewriteTerm::GetStatus() { return this->stat; }
std::string RewriteTerm::GetContent() { return this->content; }

bool RewriteTerm::operator==(RewriteTerm& r2)
{
    if (this->GetStatus() != r2.GetStatus())
        return false;
    switch (this->GetStatus())
    {
    case rterm_int_literal:
        return this->GetInt() == r2.GetInt();
    case rterm_string_literal:
    case rterm_variable:
        return this->GetContent() == r2.GetContent();
    case rterm_operator:
        if (this->GetContent() != r2.GetContent())
            return false;
        break;
    default:
        if (!(this->GetSubterm() == r2.GetSubterm()))
            return false;
        if (this->GetContent() != r2.GetContent())
            return false;
        break;
    }
    bool equals = false;
    for (auto& t1 : this->GetSubterms())
    {
        for (auto& t2 : r2.GetSubterms())
        {
            if (t1 == t2)
            {
                equals = true;
                break;
            }
        }
        if (!equals)
        {
            return false;
        }
        equals = false;
    }
    equals = false;
    for (auto& t1 : r2.GetSubterms())
    {
        for (auto& t2 : this->GetSubterms())
        {
            if (t1 == t2)
            {
                equals = true;
                break;
            }
        }
        if (!equals)
        {
            return false;
        }
        equals = false;
    }
    return true;
}
}  // namespace tree
