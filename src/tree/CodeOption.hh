/****************************************************************************************
  Term processor yeet. Declarations for optional code blocks in .k files

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file CodeOption.hh
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header file for optional code blocks in .k files
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <iostream>
#include <string>
#include <vector>

#include "Attribute.hh"

namespace tree
{

/** A class to read in C Code blocks for Phylum declarations. */
class CodeOption
{
private:
    /** List of attributes declared in the codeblock */
    std::vector<Attribute> attributes;

public:
    /**
     * Constructor for a Code block.
     * \param attributes The vector containing the declared attributes.
     */
    CodeOption(std::vector<Attribute> attributes);
    CodeOption();
    /**
     * Destructor for a Code block.
     */
    ~CodeOption();

    std::vector<Attribute> GetAttributes();
};
}  // namespace tree
