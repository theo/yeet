/****************************************************************************************
  Term processor yeet. Definitions for code blocks in .k files that are copied verbatim

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file CodeRedirection.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Implementation file for redirected code from .k files
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "CodeRedirection.hh"

#include <string>
#include <vector>

using namespace std;

namespace tree
{
const std::string CodeRedirection::allRedirections[10] = {"CODE",         "HEADER",
                                                          "KC_TYPES",     "KC_TYPES_HEADER",
                                                          "KC_REWRITE",   "KC_REWRITE_HEADER",
                                                          "KC_UNPARSE",   "KC_UNPARSE_HEADER",
                                                          "KC_FUNCTIONS", "KC_FUNCTIONS_HEADER"};

CodeRedirection::CodeRedirection() {}
CodeRedirection::CodeRedirection(
    std::string code, std::vector<std::string> redirections, const std::string_view fileName,
    int lineNr
)
    : TreeInfo(fileName, lineNr)
{
    this->code = code;
    this->redirections = redirections;
}
CodeRedirection::~CodeRedirection() {}
std::string CodeRedirection::GetCode() { return this->code; }
void CodeRedirection::SetCode(std::string code) { this->code = code; }
std::vector<std::string> CodeRedirection::GetRedirections() { return this->redirections; }
void CodeRedirection::SetRedirections(std::vector<std::string> redirections)
{
    this->redirections = redirections;
}
std::string CodeRedirection::GetFilenameFromRedirection(std::string redirection)
{
    if (redirection.find("KC_FUNCTIONS_") == 0)
    {
        if (redirection.find("_HEADER") != std::string::npos)
        {
            return redirection.substr(13, redirection.length() - 7);
        }
        else
        {
            return redirection.substr(13);
        }
    }
    return "";
}
bool CodeRedirection::MatchesType(std::string type)
{
    for (auto redirection : this->redirections)
    {
        if (MatchesType(redirection, type))
        {
            return true;
        }
    }
    return false;
}

bool CodeRedirection::MatchesType(std::string code, std::string type)
{
    if (code.find("KC_FUNCTIONS") != string::npos
        && code.find("KC_FUNCTIONS_") == type.find("KC_FUNCTIONS_"))
    {
        if (code.find("_KC_HEADER") == type.find("_KC_HEADER"))
        {
            return true;
        }
    }
    else if (code == type)
    {
        return true;
    }
    return false;
}

bool CodeRedirection::IsValid(std::string code)
{
    for (size_t i = 0; i < (sizeof(allRedirections) / sizeof(*allRedirections)); ++i)
    {
        if (MatchesType(code, allRedirections[i]))
        {
            return true;
        }
    }
    return false;
}

}  // namespace tree
