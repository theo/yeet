/****************************************************************************************
  Term processor yeet. Declarations for a parsed unparse clause

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file UnparseClause.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header file for unparse clauses
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include "UView.hh"
#include "UnparseItem.hh"

namespace tree
{
/** A class describing a single UnparseClause */
class UnparseClause
{
private:
    /** An UnparseClause includes a bunch of views under which the clause will
     * be applied */
    std::vector<UView> views;
    /** An UnparseClause consists of many UnparseItems which will be unparsed */
    std::vector<UnparseItem> items;

public:
    /** Default constructor, bison wants this */
    UnparseClause();
    /** Constructor for an unparseclause with a set of unparseitems
     * \param items The vector containing the unparseitems
     * \param views The vector containing the relevant views
     */
    UnparseClause(std::vector<UView> views, std::vector<UnparseItem> items);
    /** Default deconstructor */
    ~UnparseClause();
    /** Function to add a new unparseitem
     * \param unparseItem The item to be added
     * \return void
     */
    void AddUnparseItem(UnparseItem unparseItem);
    /** Function to return the set of unparseitems
     * \return A reference to the vector containing the unparseitems
     */
    std::vector<UnparseItem>& GetUnparseItems();

    /** Function to return the views of the clause
     * \return A reference to the vector containing the views
     */
    std::vector<UView>& GetViews();

    /** Overloaded operator to print an unparseclause to an outstream
     * \param os The stream the clause will be printed to
     * \param upr The clause that will be printed
     * \return A reference to the resulting stream after printing
     */
    friend std::ostream& operator<<(std::ostream& os, const UnparseClause& upc);
    bool operator==(UnparseClause& upc2);
};

}  // namespace tree
