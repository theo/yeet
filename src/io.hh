/****************************************************************************************
  Term processor yeet. Declarations for helper class to handle file io

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file io.hh
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Header file for file io helper class
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2024 - present
 *
 */

#include <cstdio>
#include <memory>
#include <string>
#include <string_view>

#include <parser.hh>

namespace runtime
{

/**
 * @brief Class to wrap information for an input file to be parsed. Manages a FILE* to the
 * file and a bison location object to track where parsing currently is in the file.
 */
class Input
{
private:
    /**
     * @brief Class to implement a custom deleter for a FILE* managed through a smart
     * pointer. Allows the destructor of runtime::Input to be trivial.
     */
    class FileDeleter
    {
    private:
        // reference to input object for filename
        Input& input;

    public:
        explicit FileDeleter(Input& input_);
        // closes the file unless its stdin
        void operator()(FILE* file_);
    };

    // copy of the filename -> input object is owning
    const std::string filename;
    // file pointer
    std::unique_ptr<FILE, FileDeleter> file;
    // location object
    parsing::location loc;

public:
    // construct from given filename, default value indicates input from stdin
    explicit Input(const std::string_view filename_ = "-");

    // get the filename as a std::string_view
    auto get_filename() const noexcept -> std::string_view;
    // get a reference to the internal location object
    auto get_location() noexcept -> parsing::location&;

    operator FILE*() const;
    operator parsing::location&();
};

}  // namespace runtime
