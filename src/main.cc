/****************************************************************************************
  Term processor yeet. Main program entry

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

/**
 * @file main.cc
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Main program entry
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <cstdlib>
#include <regex>
#include <string>

#include <fmt/core.h>

#include <runtime.hh>
#include <generator/FunctionClassGenerator.hh>
#include <generator/FunctionHeaderGenerator.hh>
#include <generator/RewriteClassGenerator.hh>
#include <generator/RewriteHeaderGenerator.hh>
#include <generator/TypesClassGenerator.hh>
#include <generator/TypesHeaderGenerator.hh>
#include <generator/UnparseClassGenerator.hh>
#include <generator/UnparseHeaderGenerator.hh>
#include <generator/yystypeHeaderGenerator.hh>

/** Main entry point of the tool. */
int main(int argc, char* argv[])
{
    fmt::println(PACKAGE_STRING);

    runtime::evaluateParameters(argc, argv);

    if (runtime::args.inputs_num == 0)
    {
        runtime::log<runtime::level::err>("No source files given.");
        return EXIT_FAILURE;
    }

    parsing::Declarations declarations;

    for (unsigned int i = 0; i < runtime::args.inputs_num; i++)
    {
        if (parsing::Parse(runtime::args.inputs[i], declarations) == 0)
        {
            runtime::log("Parsed {}", runtime::args.inputs[i]);
        }
        else
        {
            runtime::log<runtime::level::err>("Error while parsing {}", runtime::args.inputs[i]);
            return EXIT_FAILURE;
        }
    }
    if (parsing::CheckGlobalErrors(declarations))
    {
        runtime::log<runtime::level::fatal>("A fatal error was found. Exit.");
        return EXIT_FAILURE;
    }
    parsing::PrintInfo(declarations);
    if (!std::regex_match(
            runtime::args.namespace_arg, std::regex("^([[:alpha:]]|_)([[:alnum:]]|_)*$")
        ))
    {
        runtime::log<runtime::level::err>(
            "Your custom namespace name is not a valid c++ identifier."
        );
        return EXIT_FAILURE;
    }

    /* Generate the types header file. */
    generator::TypesHeaderGenerator khGen(runtime::args.file_prefix_arg, declarations);
    khGen.SetNamespaceName(runtime::args.namespace_arg);
    khGen.GenerateFile();
    /* Generate the types class file. */
    generator::TypesClassGenerator kcGen(runtime::args.file_prefix_arg, declarations);
    kcGen.SetNamespaceName(runtime::args.namespace_arg);
    kcGen.GenerateFile();
    /* Generate the unparse header file. */
    generator::UnparseHeaderGenerator unphGen(runtime::args.file_prefix_arg, declarations);
    unphGen.SetNamespaceName(runtime::args.namespace_arg);
    unphGen.GenerateFile();
    /* Generate the unparse class file. */
    generator::UnparseClassGenerator unpcGen(runtime::args.file_prefix_arg, declarations);
    unpcGen.SetNamespaceName(runtime::args.namespace_arg);
    unpcGen.GenerateFile();
    /* Generate the unparse header file. */
    generator::RewriteHeaderGenerator rhGen(runtime::args.file_prefix_arg, declarations);
    rhGen.SetNamespaceName(runtime::args.namespace_arg);
    rhGen.GenerateFile();
    /* Generate the unparse class file. */
    generator::RewriteClassGenerator rcGen(runtime::args.file_prefix_arg, declarations);
    rcGen.SetNamespaceName(runtime::args.namespace_arg);
    rcGen.GenerateFile();
    /* Generate header and class files for each input file. */
    for (unsigned int i = 0; i < runtime::args.inputs_num; i++)
    {
        /* Generate class file. */
        generator::FunctionClassGenerator fcGen(
            runtime::args.file_prefix_arg, declarations, runtime::args.inputs[i]
        );
        fcGen.SetNamespaceName(runtime::args.namespace_arg);
        fcGen.GenerateFile();
        /* Generate header file. */
        generator::FunctionHeaderGenerator fhGen(
            runtime::args.file_prefix_arg, declarations, runtime::args.inputs[i]
        );
        fhGen.SetNamespaceName(runtime::args.namespace_arg);
        fhGen.GenerateFile();
    }
    if (runtime::args.yystype_given)
    {
        /** Generate yystype header */
        generator::yystypeHeaderGenerator yyGen(runtime::args.file_prefix_arg, declarations);
        yyGen.SetNamespaceName(runtime::args.namespace_arg);
        yyGen.GenerateFile();
    }
    return 0;
}
