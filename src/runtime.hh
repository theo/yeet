/****************************************************************************************
  Term processor yeet. Declarations for runtime helpers

  Copyright (c) 2020 - present, Chair for Theoretical Computer Science at the
  University of Rostock, lead by Karsten Wolf, as well as yeet contributors

  The MIT License (MIT)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -- NOTE: Third party dependency used by this software --
  This software depends on the fmt lib (MIT License),
  and users must comply to its license: https://github.com/fmtlib/fmt/blob/master/LICENSE
 ****************************************************************************************/

#pragma once
/**
 * @file runtime.hh
 * @author Nick Kotsakidis (nick.kotsakidis@uni-rostock.de)
 * @author Justin Kreikemeyer (justin.kreikemeyer@uni-rostock.de)
 * @author Ole Szellas (ole.szellas@uni-rostock.de)
 * @author Lukas Zech (lukas.zech@uni-rostock.de)
 * @brief Declarations for runtime helpers
 * @version 0.1
 * @date 2024-04-25
 *
 * @copyright Copyright (c) 2020 - present
 *
 */

#include <array>
#include <string>
#include <string_view>

#include <cmdline.h>
#include <fmt/ostream.h>
#include <fmt/color.h>

#include <tree/CodeRedirection.hh>
#include <tree/Phylum.hh>
#include <tree/RView.hh>
#include <tree/RewriteRule.hh>
#include <tree/Rule.hh>
#include <tree/UView.hh>
#include <tree/UnparseRule.hh>
#include <tree/TreeStatistics.hh>

namespace runtime
{
namespace error
{

enum class ErrorType : int
{
    /* CLI Errors */
    NO_SOURCE_FILE = 0, /* No source file provided */

    /* Parsing Errors */
    PARSING_ERROR = 1,               /* Undefined error while parsing */
    PHY_MISSING_OP = 2,              /* no operator defined for phylum */
    OP_NOT_IN_PHY = 3,               /* outermost operator o not in expected phylum p */
    PHY_DECL_NOT_FOUND = 4,          /* could not find declaration of phylum p */
    OP_DECL_NOT_FOUND = 5,           /* could not find declaration of operator o */
    TOO_FEW_SUBPATTERNS_FOR_OP = 6,  /* too few subpatterns for operator */
    TOO_MANY_SUBPATTERNS_FOR_OP = 7, /* too many subpatterns for operator */
    INVALID_REDIRECTION = 8,         /* Code Redirection is invalid */
    // OUTMOST_OP_NOT_EXPECTED_IN_PHY, /* Operator not in expected phylum */
    // UNEXPECTED_INT_SUBTERM,         /* unexpected int subterm */
    // UNEXPECTED_STRING_SUBTERM,      /* unexpected string subterm */
    // ILLEGAL_DOLLARVAR,              /* illegal dollar variable */

    /* Code generation errors */
    COULD_NOT_CREATE_FILE = 9, /* Could not create or write to output file */
};

using enum ErrorType;

constexpr std::array<std::string_view, 10> default_error_message{
    /** CLI Errors */
    "No source file(s) provided.",  // NO_SORUCE_FILE
    /** Parsing Errors */
    "Error while parsing. Aborting.",             // PARSING_ERROR
    "no operator defined for phylum",             // PYH_MISSING_OP
    "outermost operator not in expected phylum",  // OP_NOT_IN_PHY
    "Could not find declaration of phylum",       // PHY_DECL_NOT_FOUND
    "Could not find declaration of operator",     // OP_DECL_NOT_FOUND
    "Not enough subpatterns for operator",        // TOO_FEW_SUBPATTERNS_FOR_OP
    "Too many subpatterns for operator",          // TOO_MANY_SUBPATTERNS_FOR_OP
    "Redirection is invalid: ",                   // INVALID_REDIRECTION
    // "Operator not in expected phylum", // OUTMOST_OP_NOT_EXPECTED_IN_PHY
    // "unexpected int subterm", // UNEXPECTED_INT_SUBTERM
    // "unexpected string subterm", // UNEXPECTED_STRING_SUBTERM
    // "illegal dollar variable", // ILLEGAL_DOLLARVAR
    /** Code Generation Errors */
    "Could not create or write to output file",  // COULD_NOT_CREATE_FILE
};
}  // namespace error

extern gengetopt_args_info args;
void evaluateParameters(int argc, char** argv);

enum class level : int
{
    debug = 0,
    info = 1,
    warn = 2,
    err = 3,
    fatal = 4,
};

using namespace std::string_view_literals;
auto level_to_text(level) -> decltype(fmt::styled(""sv, {}));

/**
 * @brief Log message as information with formatting.
 *
 * @tparam Args
 * @param format_str Format string
 * @param fargs Arguments to format into the string
 */
template <level loglevel = level::info, typename... Args>
void log(fmt::format_string<Args...> format_str, Args&&... fargs)
{
    if (static_cast<std::underlying_type_t<level>>(loglevel) >= runtime::args.loglevel_arg)
    {
        fmt::println(
            std::cerr, "[{}]: {}", level_to_text(loglevel),
            fmt::format(format_str, std::forward<Args>(fargs)...)
        );
    }
}
}  // namespace runtime

namespace parsing
{
struct Declarations
{
    /** Holds all Phylum definitions. */
    std::vector<tree::Phylum> phylum_declarations;
    /** Holds all Rewrite rules. */
    std::vector<tree::RewriteRule> rewrite_rules;
    /** Holds all Unparse rules. */
    std::vector<tree::UnparseRule> unparse_rules;
    /** Holds all UView declarations */
    std::vector<tree::UView> unparse_views;
    /** Holds all RView declarations */
    std::vector<tree::RView> rewrite_views;
    /** Holds all code redirections */
    std::vector<tree::CodeRedirection> code_redirections;
    /** Holds statistics about the parsed tree syntax */
    tree::TreeStatistics tree_statistics;
};
/** Prints Information about the parsed files. */
void PrintInfo(Declarations&);
/** Run the parser on file f. Return 1 on success. */
int Parse(const std::string_view, Declarations&);
/** Check global errors after all files have been parsed.
 * \returns true if a fatal error was found.
 */
bool CheckGlobalErrors(Declarations&);
}  // namespace parsing
